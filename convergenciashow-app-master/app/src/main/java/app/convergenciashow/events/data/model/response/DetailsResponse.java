package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class DetailsResponse extends BaseResponse {
    private ArrayList<CommentsResponse> comments;

    public ArrayList<CommentsResponse> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentsResponse> comments) {
        this.comments = comments;
    }
}
