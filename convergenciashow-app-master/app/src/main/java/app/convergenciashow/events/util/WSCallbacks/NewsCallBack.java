package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.domain.New;

public interface NewsCallBack {

    void onSuccess(@NonNull ArrayList<New> value);

    void onError(@NonNull Throwable throwable);
}
