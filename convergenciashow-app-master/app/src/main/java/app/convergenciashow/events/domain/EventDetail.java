package app.convergenciashow.events.domain;

public class EventDetail {

    private Long id;
    private String name;
    private String address;
    private String description;
    private String dateInit;
    private String dateEnd;
    private Double lat;
    private Double lon;
    private Integer imageVersion;
    private boolean willUserBeInEvent;
    private boolean globalMapAvailable;
    private Integer globalMapVersion;

    public boolean isGlobalMapAvailable() {
        return globalMapAvailable;
    }

    public void setGlobalMapAvailable(boolean globalMapAvailable) {
        this.globalMapAvailable = globalMapAvailable;
    }

    public Integer getGlobalMapVersion() {
        return globalMapVersion;
    }

    public void setGlobalMapVersion(Integer globalMapVersion) {
        this.globalMapVersion = globalMapVersion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateInit() {
        return dateInit;
    }

    public void setDateInit(String dateInit) {
        this.dateInit = dateInit;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(Integer imageVersion) {
        this.imageVersion = imageVersion;
    }

    public boolean isWillUserBeInEvent() {
        return willUserBeInEvent;
    }

    public void setWillUserBeInEvent(boolean willUserBeInEvent) {
        this.willUserBeInEvent = willUserBeInEvent;
    }
}
