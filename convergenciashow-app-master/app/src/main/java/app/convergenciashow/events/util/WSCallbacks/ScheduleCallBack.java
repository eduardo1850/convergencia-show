package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.data.model.response.BaseResponse;

public interface ScheduleCallBack {

    void onSuccess(@NonNull ArrayList<Object> value);

    void onError(@NonNull BaseResponse baseResponse);
}
