package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class PlanoResponse extends BaseResponse {

    private ArrayList<StandsResponse> stands;

    private Boolean mapAvailable;

    private Integer mapVersion;

    public ArrayList<StandsResponse> getStands() {
        return stands;
    }

    public void setStands(ArrayList<StandsResponse> stands) {
        this.stands = stands;
    }

    public Boolean getMapAvailable() {
        return mapAvailable;
    }

    public void setMapAvailable(Boolean mapAvailable) {
        this.mapAvailable = mapAvailable;
    }

    public Integer getMapVersion() {
        return mapVersion;
    }

    public void setMapVersion(Integer mapVersion) {
        this.mapVersion = mapVersion;
    }
}
