package app.convergenciashow.events.domain;

public class Expo {

    private Long id;
    private	String title;
    private	String date;
    private Long idRoom;
    private String speakers;
    private String roomTitle;
    private boolean isMarkedForPersonalAgenda;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSpeakers() {
        return speakers;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }

    public Long getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(Long idRoom) {
        this.idRoom = idRoom;
    }

    public String getRoomTitle() {
        return roomTitle;
    }

    public void setRoomTitle(String roomTitle) {
        this.roomTitle = roomTitle;
    }

    public boolean isMarkedForPersonalAgenda() {
        return isMarkedForPersonalAgenda;
    }

    public void setMarkedForPersonalAgenda(boolean markedForPersonalAgenda) {
        isMarkedForPersonalAgenda = markedForPersonalAgenda;
    }
}
