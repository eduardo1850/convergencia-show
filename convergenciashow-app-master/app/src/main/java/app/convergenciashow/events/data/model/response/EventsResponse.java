package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class EventsResponse extends BaseResponse {

    private ArrayList<EventResponse> events;

    public ArrayList<EventResponse> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<EventResponse> events) {
        this.events = events;
    }
}
