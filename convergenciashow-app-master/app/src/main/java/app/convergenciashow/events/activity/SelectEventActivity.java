package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.EventAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.domain.Event;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.EventsCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SelectEventActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llselectevent)
    LinearLayout llselectevent;
    @BindView(R.id.selectevent_tv)
    TextView selectevent_tv;
    @BindView(R.id.selecteventRv)
    RecyclerView selecteventRv;
    @BindView(R.id.selectevent_bt_ok)
    Button selecteventbtok;

    private static final int rows = 7;
    private Long loadedElements = 0L;
    private EventRepository eventRepository;
    private AuthRepository authRepository;
    private EventAdapter eventAdapter;
    private boolean loading = false;
    private int rvLastVisibleItem;
    private AlertBuilderView progressBuilder, alertBuilderView;
    private ArrayList<Event> events;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_event);
        unbinder = ButterKnife.bind(this);

        showErrorLayout(false);
        events = new ArrayList<>();
        eventRepository = new EventRepository(this);
        authRepository = new AuthRepository(getApplicationContext(), this);
        selecteventbtok.setOnClickListener(this);
        eventAdapter = new EventAdapter(events, this);
        progressBuilder = new AlertBuilderView(this, Operations.AlertBuilder_progress, "");
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());

        selecteventRv.setLayoutManager(manager);
        selecteventRv.swapAdapter(eventAdapter, true);
        selecteventRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) selecteventRv.getLayoutManager();
                rvLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (selecteventRv.getChildCount() > 0) {
                    if (!loading && rvLastVisibleItem == selecteventRv.getChildCount() - 1) {
                        loading = true;
                        loadElements();
                    }
                }
            }
        });
        loadElements();
    }

    private void loadElements() {
        progressBuilder.show();
        eventRepository.getEvents(authRepository.getAuth(), loadedElements, rows, new EventsCallback() {
            @Override
            public void onSuccess(@NonNull ArrayList<Event> response) {
                progressBuilder.dismiss();
                if (response.size() > 0) {
                    events.addAll(response);
                    eventAdapter.update();
                    loadedElements = loadedElements + rows;
                    loading = false;
                }
                //else {showErrorLayout(true);}
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                progressBuilder.dismiss();
                if (baseResponse != null) {
                    progressBuilder.dismiss();
                    if (baseResponse != null)
                        RedirectToLogin(baseResponse.getCodeDescription());
                    else
                        RedirectToLogin(getString(R.string.generic_error));
                } else {
                    RedirectToLogin(getString(R.string.generic_error));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getString(R.string.pickup_event), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectevent_bt_ok:
                Utils.invokeActivity(SelectEventActivity.this, LoginActivity.class, true);
                break;
        }
    }

    private void showErrorLayout(boolean thereAreError) {
        if (thereAreError) {
            selecteventRv.setVisibility(View.GONE);
            llselectevent.setVisibility(View.VISIBLE);
        } else {
            selecteventRv.setVisibility(View.VISIBLE);
            llselectevent.setVisibility(View.GONE);
        }
    }

    private void RedirectToLogin(String errorMessage) {
        alertBuilderView = new AlertBuilderView(SelectEventActivity.this, Operations.AlertBuilder_error, errorMessage);
        alertBuilderView.show();
        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Utils.invokeActivity(SelectEventActivity.this, LoginActivity.class, true);
            }
        });
    }
}
