package app.convergenciashow.events.data.model.request;

import java.util.ArrayList;

public class AnswerQuestionRequest {

    private Long idQuestion;
    private ArrayList<Long> options;

    public AnswerQuestionRequest() {
    }

    public AnswerQuestionRequest(Long idQuestion, ArrayList<Long> options) {
        this.idQuestion = idQuestion;
        this.options = options;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public ArrayList<Long> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Long> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "AnswerQuestionRequest{" +
                "idQuestion=" + idQuestion +
                ", options=" + options +
                '}';
    }
}
