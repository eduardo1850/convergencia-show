package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.ActiveQuestionResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;

public interface ActiveQuestionCallback {

    void onSuccess(@NonNull ActiveQuestionResponse activeQuestionResponse);

    void onError(@NonNull BaseResponse baseResponse);

}
