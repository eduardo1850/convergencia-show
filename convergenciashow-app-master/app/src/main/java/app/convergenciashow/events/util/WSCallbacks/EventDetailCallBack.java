package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.EventDetail;

public interface EventDetailCallBack {

    void onSuccess(@NonNull EventDetail value);

    void onError(@NonNull BaseResponse baseResponse);
}
