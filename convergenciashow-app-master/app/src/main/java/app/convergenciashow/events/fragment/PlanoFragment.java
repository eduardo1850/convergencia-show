package app.convergenciashow.events.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;

import app.convergenciashow.events.R;
import app.convergenciashow.events.domain.Plano;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PlanoFragment extends BaseReturnFragment {

    private static final String PLANO = "Plano";
    private Plano plano;
    private PDFView planoImage;
    private LinearLayout llplan;
    private ProgressBar progressBar;
    private Activity activity;

    public PlanoFragment() {
    }

    public static PlanoFragment newInstance(Plano param1) {
        PlanoFragment fragment = new PlanoFragment();
        Bundle args = new Bundle();
        args.putParcelable(PLANO, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(getString(R.string.back));
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plano = getArguments().getParcelable(PLANO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_plano, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = getActivity();
        try {
            planoImage = activity.findViewById(R.id.planoImage);
            llplan = activity.findViewById(R.id.llplan);
            progressBar = activity.findViewById(R.id.planoPB);
            new RetrievePDFStream().execute(Utils.getPdfUrl(AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT), plano.getMapVersion()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class RetrievePDFStream extends AsyncTask<String, Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL urlx = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) urlx.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    setError(false);
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                } else {
                    setError(true);
                    return null;
                }
            } catch (IOException e) {
                setError(true);
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            planoImage.fromStream(inputStream)
                    .onLoad(new OnLoadCompleteListener() {
                        @Override
                        public void loadComplete(int nbPages) {
                            progressBar.setVisibility(View.GONE);
                            planoImage.setVisibility(View.VISIBLE);

                        }
                    })
                    .onError(new OnErrorListener() {
                        public void onError(Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            setError(true);
                        }
                    }).load();
        }
    }

    private void setError(final boolean thereAreError) {
        if (isAdded()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thereAreError) {
                        llplan.setVisibility(View.VISIBLE);
                        planoImage.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    } else {
                        llplan.setVisibility(View.GONE);
                        planoImage.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

}
