package app.convergenciashow.events.data.model.response;

public class ConvergenciaResponse extends BaseResponse{

    private String description;
    private String title;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
