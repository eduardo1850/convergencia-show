package app.convergenciashow.events.data.model.response;

import java.util.List;

public class DocumentsResponse {

    private List<DocumentResponse> documents;

    public List<DocumentResponse> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentResponse> documents) {
        this.documents = documents;
    }
}
