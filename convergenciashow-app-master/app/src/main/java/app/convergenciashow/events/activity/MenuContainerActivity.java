package app.convergenciashow.events.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.listeners.IPickResult;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.BaseActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.custom.MyBottomView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.fragment.AsistentesFragment;
import app.convergenciashow.events.fragment.ConversationsFragment;
import app.convergenciashow.events.fragment.ForoFragment;
import app.convergenciashow.events.fragment.GalleryFragment;
import app.convergenciashow.events.fragment.MenuEventFragment;
import app.convergenciashow.events.fragment.NewsFragment;
import app.convergenciashow.events.fragment.NotificationsFragment;
import app.convergenciashow.events.fragment.PollsFragment;
import app.convergenciashow.events.fragment.ProfileFragment;
import app.convergenciashow.events.fragment.SpeakersFragment;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.BottomNavigationViewHelper;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import static app.convergenciashow.events.util.Constants.FRAGMENT_ASSISTANTS;
import static app.convergenciashow.events.util.Constants.FRAGMENT_CHAT;
import static app.convergenciashow.events.util.Constants.FRAGMENT_DOCUMENTS;
import static app.convergenciashow.events.util.Constants.FRAGMENT_EVENT;
import static app.convergenciashow.events.util.Constants.FRAGMENT_FORUM;
import static app.convergenciashow.events.util.Constants.FRAGMENT_GALLERY;
import static app.convergenciashow.events.util.Constants.FRAGMENT_NEWS;
import static app.convergenciashow.events.util.Constants.FRAGMENT_POLLS;
import static app.convergenciashow.events.util.Constants.FRAGMENT_PROFILE;
import static app.convergenciashow.events.util.Constants.FRAGMENT_PUSH;
import static app.convergenciashow.events.util.Constants.FRAGMENT_SPEAKERS;

public class MenuContainerActivity extends BaseActivity implements IPickResult, View.OnClickListener {

    private MyBottomView navigationView;
    private AuthRepository repository;
    private AlertBuilderView alertBuilderView, progressBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu_container);
        super.onCreate(savedInstanceState);
        progressBuilder = new AlertBuilderView(MenuContainerActivity.this, Operations.AlertBuilder_progress, "");
    }

    @Override
    protected void setComponent() {
        repository = new AuthRepository(getApplicationContext(), this);
        navigationView = findViewById(R.id.navigationView);
        BottomNavigationViewHelper.disableShiftMode(navigationView);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                 Fragment selectedFragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_evento:
                        selectedFragment = MenuEventFragment.newInstance();
                        openFragment(selectedFragment);
                        setFragmentToShow(FRAGMENT_EVENT);
                        break;
                    case R.id.action_noticias:
                        selectedFragment = NewsFragment.newInstance();
                        openFragment(selectedFragment);
                        setFragmentToShow(FRAGMENT_NEWS);
                        break;
                    case R.id.action_yo:
                        selectedFragment = ProfileFragment.newInstance();
                        openFragment(selectedFragment);
                        /*if (repository.checkAuth()) {
                            selectedFragment = ProfileFragment.newInstance();
                            openFragment(selectedFragment);
                        } else {
                            invokeActivity(MenuContainerActivity.this, ProfileActivity.class, false);
                        }*/
                        break;
                    case R.id.action_notificacones:
                        selectedFragment = NotificationsFragment.newInstance();
                        openFragment(selectedFragment);
                        setFragmentToShow(FRAGMENT_PUSH);
                        break;
                    case R.id.action_chat:
                        selectedFragment = ConversationsFragment.newInstance();
                        openFragment(selectedFragment);
                        setFragmentToShow(FRAGMENT_CHAT);
                        break;
                }
                return true;
            }
        });
        navigationView.setSelectedItemId(R.id.action_evento);
    }

    private void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commitAllowingStateLoss();
    }

    public void setItemSelected(int item) {
        int action = 0;
        Fragment fragment = null;
        if (item == FRAGMENT_EVENT) {
            action = R.id.action_evento;
            fragment = MenuEventFragment.newInstance();
        } else if (item == FRAGMENT_NEWS) {
            action = R.id.action_noticias;
            fragment = NewsFragment.newInstance();
        } else if (item == FRAGMENT_PROFILE) {
            action = R.id.action_yo;
            fragment = ProfileFragment.newInstance();
        } else if (item == FRAGMENT_PUSH) {
            action = R.id.action_notificacones;
            fragment = NotificationsFragment.newInstance();
        } else if (item == FRAGMENT_CHAT) {
            action = R.id.action_notificacones;
            fragment = ConversationsFragment.newInstance();
        } else if (item == FRAGMENT_FORUM) {
            action = R.id.action_evento;
            fragment = ForoFragment.newInstance();
        } else if (item == FRAGMENT_GALLERY) {
            action = R.id.action_evento;
            fragment = GalleryFragment.newInstance();
        } else if (item == FRAGMENT_SPEAKERS) {
            action = R.id.action_evento;
            fragment = SpeakersFragment.newInstance();
        } else if (item == FRAGMENT_ASSISTANTS) {
            action = R.id.action_evento;
            fragment = AsistentesFragment.newInstance();
        } else if (item == FRAGMENT_POLLS) {
            action = R.id.action_evento;
            fragment = PollsFragment.newInstance();
        }

        navigationView.setSelectedItemId(action);

        if (fragment != null) {
            openFragment(fragment);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_ASSISTANTS) {
            setItemSelected(FRAGMENT_ASSISTANTS);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_SPEAKERS) {
            setItemSelected(FRAGMENT_SPEAKERS);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_GALLERY) {
            setItemSelected(FRAGMENT_GALLERY);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_DOCUMENTS) {
            setItemSelected(FRAGMENT_DOCUMENTS);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_FORUM) {
            setItemSelected(FRAGMENT_FORUM);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else if (AppPreferences.getInstance(this).getIntData(Constants.STACK_TRACE) == FRAGMENT_POLLS) {
            setItemSelected(FRAGMENT_POLLS);
            AppPreferences.getInstance(this).remove(Constants.STACK_TRACE);
        } else {
            setItemSelected(getFragmentToShow());
        }
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            navigationView.setSelectedItemId(FRAGMENT_PROFILE);
            openFragment(ProfileFragment.newInstanceArg(pickResult.getBitmap()));
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.alert_btn_ok:
                alertBuilderView.dismiss();
                progressBuilder.show();
                AuthUser authUser = repository.getAuth();
                repository.logOut(authUser, new GenericCallBack() {
                    @Override
                    public void onSuccess(@NonNull BaseResponse value) {
                        progressBuilder.dismiss();
                        AppPreferences.getInstance(MenuContainerActivity.this).remove(Constants.ID_EVENT);
                        Utils.invokeActivity(MenuContainerActivity.this, LoginActivity.class, true);
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null)
                            new AlertBuilderView(MenuContainerActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                        else
                            new AlertBuilderView(MenuContainerActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                    }
                });
                break;
            case R.id.alert_btn_nok:
                alertBuilderView.dismiss();
                break;
        }
    }

    public void gotoConversation(Long idUserToConverse, String nameUserToConverse, String imageUserToConverse) {
        eraseFragments();
        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra(Constants.IDUSERTOCONVERSE, idUserToConverse);
        intent.putExtra(Constants.NAMEUSERTOCONVERSE, nameUserToConverse);
        intent.putExtra(Constants.IMAGEUSERTOCONVERSE, imageUserToConverse);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1504:
                int permissionAcepted = 0;
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            permissionAcepted++;
                        }
                    }
                    if (permissionAcepted == grantResults.length)
                        setFragmentToShow(FRAGMENT_FORUM);
                }
                break;
            default:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, SimpleScannerActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getString(R.string.camera_permission), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

}
