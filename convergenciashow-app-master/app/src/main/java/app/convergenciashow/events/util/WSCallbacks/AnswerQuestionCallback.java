package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.AnswerQuestionResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;

public interface AnswerQuestionCallback {

    void onSuccess(@NonNull AnswerQuestionResponse answerQuestionResponse);

    void onError(@NonNull BaseResponse baseResponse);

}
