package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.SendAMessageResponse;

public interface SendAMessageCallback {

    void onSuccess(@NonNull SendAMessageResponse response);

    void onError(@NonNull BaseResponse baseResponse);

}
