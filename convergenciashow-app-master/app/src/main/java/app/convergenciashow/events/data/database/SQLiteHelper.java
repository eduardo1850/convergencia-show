package app.convergenciashow.events.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import app.convergenciashow.events.domain.AuthUser;

import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.CAN_BE_REACHED;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.COMPANY_NAME;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.CREATE_AUTH_QUERY;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.DELETE_AUTH_QUERY;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.EMAIL;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.HAS_PICTURE;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.ID_SESSION;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.ID_USER;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.IMAGE_VERSION;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.IS_VERIFIED;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.ROLE;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.SELECT_AUTH_QUERY;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.TABLE_NAME;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.USERNAME;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.USER_DESCRIPTION;
import static app.convergenciashow.events.data.database.AuthContractor.AuthEntry.USER_LASTNAME;

public class SQLiteHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Events.db";
    private SQLiteDatabase db;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(CREATE_AUTH_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void insertAuth(AuthUser user) {
        db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ID_USER, user.getIdUser());
        cv.put(ID_SESSION, user.getIdSession());
        cv.put(USERNAME, user.getUserName());
        cv.put(USER_LASTNAME, user.getUserLastName());
        cv.put(IMAGE_VERSION, user.getImageVersion());
        cv.put(USER_DESCRIPTION, user.getUserDescription());
        cv.put(EMAIL, user.getEmail());
        cv.put(ROLE, user.getRole());
        cv.put(COMPANY_NAME, user.getCompanyName());
        cv.put(HAS_PICTURE, user.isAlreadyHasPicture() ? 1 : 0);
        cv.put(IS_VERIFIED, user.isVerified() ? 1 : 0);
        cv.put(CAN_BE_REACHED, user.isCanUserBeReached() ? 1 : 0);
        db.insert(TABLE_NAME, null, cv);
    }

    public void deleteAuth() {
        db = getWritableDatabase();
        db.execSQL(DELETE_AUTH_QUERY);
    }

    public AuthUser getAuth() {
        db = getWritableDatabase();
        Cursor cursor = db.rawQuery(SELECT_AUTH_QUERY, null);
        AuthUser authUser = new AuthUser();
        if (cursor.moveToFirst()) {
            authUser.setIdUser(cursor.getLong(cursor.getColumnIndex(ID_USER)));
            authUser.setIdSession(cursor.getString(cursor.getColumnIndex(ID_SESSION)));
            authUser.setUserName(cursor.getString(cursor.getColumnIndex(USERNAME)));
            authUser.setUserLastName(cursor.getString(cursor.getColumnIndex(USER_LASTNAME)));
            authUser.setImageVersion(cursor.getInt(cursor.getColumnIndex(IMAGE_VERSION)));
            authUser.setUserDescription(cursor.getString(cursor.getColumnIndex(USER_DESCRIPTION)));
            authUser.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            authUser.setRole(cursor.getString(cursor.getColumnIndex(ROLE)));
            authUser.setCompanyName(cursor.getString(cursor.getColumnIndex(COMPANY_NAME)));
            authUser.setAlreadyHasPicture(cursor.getInt(cursor.getColumnIndex(HAS_PICTURE)) == 1 ? true : false);
            authUser.setVerified(cursor.getInt(cursor.getColumnIndex(IS_VERIFIED)) == 1 ? true : false);
            authUser.setCanUserBeReached(cursor.getInt(cursor.getColumnIndex(CAN_BE_REACHED)) == 1 ? true : false);
            return authUser;
        } else {
            return null;
        }
    }


    public void updateCanBeReachedField(int value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("canUserBeReached", value);
        updateRecord(TABLE_NAME, contentValues, null, null);
    }

    public int updateRecord(String table, ContentValues values, String whereClause, String... whereClauseArgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        int alteredRows = db.update(table, values, whereClause, whereClauseArgs);
        return alteredRows;
    }


}
