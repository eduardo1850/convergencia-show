package app.convergenciashow.events.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ChatBaseActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.AnswerQuestionRequest;
import app.convergenciashow.events.data.model.response.ActiveQuestionResponse;
import app.convergenciashow.events.data.model.response.AnswerQuestionResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.OptionsResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AnswerQuestionCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static app.convergenciashow.events.util.Constants.FRAGMENT_POLLS;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class QuestionActivity extends ChatBaseActivity {

    @BindView(R.id.llquestions)
    LinearLayout llquestions;
    @BindView(R.id.question_title)
    TextView question_title;
    @BindView(R.id.ll_options)
    LinearLayout lloptions;
    @BindView(R.id.question_cardview)
    CardView questioncardview;
    @BindView(R.id.question_button)
    LinearLayout questionbutton;
    @BindView(R.id.question_button_text)
    TextView questionbuttontext;

    private Unbinder unbinder;
    private Long answer, idPoll;
    private ArrayList<Long> answers;
    private ActiveQuestionResponse actQuestion;
    private AuthRepository authRepository;
    private EventRepository repository;
    private AlertBuilderView progressBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_question);
        unbinder = ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        setTitle("");
        answers = new ArrayList<>();
        authRepository = new AuthRepository(getApplicationContext(), this);
        repository = new EventRepository(this);
        progressBuilder = new AlertBuilderView(QuestionActivity.this, Operations.AlertBuilder_progress, "");
        AppPreferences.getInstance(this).saveIntData(Constants.STACK_TRACE, FRAGMENT_POLLS);
        if (getIntent() != null) {
            actQuestion = (ActiveQuestionResponse) getIntent().getSerializableExtra(Constants.QUESTION_ACTIVITY);
            idPoll = getIntent().getLongExtra(Constants.ID_POLL, 0L);
            loadInformation(idPoll, actQuestion);
        } else {
            setEmptyQuestions(true);
        }
    }

    public void loadInformation(Long _idPoll, final ActiveQuestionResponse actQ) {
        idPoll = _idPoll;
        if (progressBuilder != null && progressBuilder.isShowing())
            progressBuilder.dismiss();

        if (actQ == null) {
            setEmptyQuestions(true);
        } else {
            if (actQ.getOptionsSelected() != null) {
                loadAnswers(actQ);
            } else {
                actQuestion = actQ;
                setEmptyQuestions(false);
                question_title.setText(actQ.getTitle());
                if (actQ.isMultiSelection())
                    createCheckBoxs();
                else
                    createRadioButton();
            }
        }

        questionbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!actQ.isMultiSelection()) {
                    if (answer != null)
                        answers.add(answer);
                }
                if (answers.size() == 0) {
                    Toast.makeText(QuestionActivity.this, getString(R.string.question_validation_text), Toast.LENGTH_LONG).show();
                } else {
                    AnswerQuestionRequest request = new AnswerQuestionRequest(actQ.getIdQuestion(), answers);
                    repository.answerQuestion(authRepository.getAuth(), request, new AnswerQuestionCallback() {
                        @Override
                        public void onSuccess(@NonNull AnswerQuestionResponse answerQuestionResponse) {
                            setQuestionAnswers(answerQuestionResponse, actQ.isMultiSelection());
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            onQuestionError(baseResponse);
                        }
                    });
                }
            }
        });

    }

    private void setQuestionAnswers(AnswerQuestionResponse answerQuestionResponse, boolean isSingle) {
        lloptions.removeAllViews();
        //if (!isSingle)
        serPercentages(answerQuestionResponse);
        for (OptionsResponse opt : answerQuestionResponse.getOptions()) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.layout_progress_bar, null);
            ProgressBar progress = view.findViewById(R.id.progress);
            TextView title = view.findViewById(R.id.progress_title);
            TextView number = view.findViewById(R.id.progress_number);
            progress.setProgress((int) opt.getPercentage().intValue());
            title.setText(opt.getTitle());
            if (!isSingle)
                number.setText(opt.getPercentage() + "%");
            else
                number.setText("(" + opt.getAnswers() + ")");
            lloptions.addView(view);
        }
        questionbutton.setBackgroundColor(getResources().getColor(R.color.text_header));
        questionbuttontext.setText(getString(R.string.question_thanks));
    }

    private void serPercentages(AnswerQuestionResponse answerQuestionResponse) {
        try {
            Integer totalAnswers = answerQuestionResponse.getAnswers();
            for (OptionsResponse answers : answerQuestionResponse.getOptions()) {
                for (int i = 0; i < actQuestion.getOptions().size(); i++) {
                    OptionsResponse questions = actQuestion.getOptions().get(i);
                    if (answers.getId().equals(questions.getId())) {
                        double porc = (answers.getAnswers() * 100) / totalAnswers;
                        answerQuestionResponse.getOptions().get(i).setPercentage(porc);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName().toString(), e.getMessage());
        }
    }

    // MultiSelection
    private void createCheckBoxs() {
        lloptions.removeAllViews();
        for (int i = 0; i < actQuestion.getOptions().size(); i++) {
            final Long id = actQuestion.getOptions().get(i).getId();
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.questions_checkbox, null);
            TextView titulo = view.findViewById(R.id.question_item_title);
            titulo.setText(actQuestion.getOptions().get(i).getTitle());
            CheckBox checkbox = (CheckBox) view.findViewById(R.id.question_item_checkbox);
            checkbox.setId(i);
            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        answers.add(id);
                    } else {
                        for (int j = 0; j < answers.size(); j++) {
                            if (answers.get(j) == id)
                                answers.remove(j);
                        }
                    }
                }
            });


            lloptions.addView(view);
        }
    }

    // SingleSelection
    private void createRadioButton() {
        lloptions.removeAllViews();
        for (int i = 0; i < actQuestion.getOptions().size(); i++) {
            final Long id = actQuestion.getOptions().get(i).getId();
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.questions_radiobutton, null);
            TextView titulo = view.findViewById(R.id.question_item_title);
            titulo.setText(actQuestion.getOptions().get(i).getTitle());
            CheckBox radio = (CheckBox) view.findViewById(R.id.question_item_radiobutton);
            radio.setId(i);
            final int radioID = radio.getId();
            radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    answer = id;
                    final int childCount = lloptions.getChildCount();
                    for (int j = 0; j < childCount; j++) {
                        ViewGroup element = (ViewGroup) lloptions.getChildAt(j);
                        findAllcheckboxes(element, radioID);
                    }
                }
            });
            lloptions.addView(view);
        }
    }

    //  SingleSelection
    private void findAllcheckboxes(ViewGroup viewGroup, int radioID) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllcheckboxes((ViewGroup) view, radioID);
            else if (view instanceof CheckBox) {
                CheckBox radio = (CheckBox) view;
                if (radioID == radio.getId() && radio.isChecked())
                    radio.setChecked(true);
                else
                    radio.setChecked(false);
            }
        }
    }

    private void onQuestionError(BaseResponse baseResponse) {
        if (baseResponse != null) {
            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                AlertBuilderView alertBuilderView = new AlertBuilderView(this, Operations.AlertBuilder_error, getString(R.string.session_error));
                alertBuilderView.show();
                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        authRepository.logOut();
                        Utils.invokeActivity(QuestionActivity.this, LoginActivity.class, true);
                    }
                });
            } else {
                new AlertBuilderView(this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
            }
        } else {
            new AlertBuilderView(this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        if (progressBuilder != null)
            progressBuilder.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
        if (progressBuilder != null)
            progressBuilder.dismiss();
    }

    /*public void invokeActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_reload_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        TextView title = findViewById(R.id.action_bar_return_string);
        AppCompatImageView refresh = findViewById(R.id.action_bar_return_reload);
        title.setText(getString(R.string.back));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBuilder.show();
                questionbutton.setBackgroundColor(getResources().getColor(R.color.orange));
                questionbuttontext.setText(getString(R.string.send_question_text));
                Utils.getActiveQuestion(idPoll, repository, authRepository, QuestionActivity.this, true);
            }
        });
    }*/

    private void setEmptyQuestions(boolean set) {
        llquestions.setVisibility((set) ? View.VISIBLE : View.GONE);
        question_title.setVisibility((set) ? View.GONE : View.VISIBLE);
        questioncardview.setVisibility((set) ? View.GONE : View.VISIBLE);
        questionbutton.setVisibility((set) ? View.GONE : View.VISIBLE);
    }


    private void loadAnswers(ActiveQuestionResponse actQuestion) {
        AnswerQuestionResponse answerQuestionResponse = new AnswerQuestionResponse();
        ArrayList<OptionsResponse> optionsResponse = new ArrayList<>();
        for (OptionsResponse options : actQuestion.getOptions()) {
            OptionsResponse o = new OptionsResponse();
            o.setAnswers(options.getAnswers());
            o.setId(options.getId());
            o.setTitle(options.getTitle());
            optionsResponse.add(o);
        }
        answerQuestionResponse.setAnswers(actQuestion.getAnswers());
        answerQuestionResponse.setOptions(optionsResponse);
        setQuestionAnswers(answerQuestionResponse, actQuestion.isMultiSelection());
    }
}
