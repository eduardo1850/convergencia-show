package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.domain.Lines;

import java.util.ArrayList;

public interface CommentsCallBack {

        void onSuccess(@NonNull ArrayList<Lines> value);

        void onError(@NonNull Throwable throwable);
}
