package app.convergenciashow.events.fragment;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LaCasaFragment extends Fragment {

    @BindView(R.id.wv_lacasa)
    WebView webView;

    private String Document_url;
    private Unbinder unbinder;
    private AlertBuilderView progressBuilder, alertBuilderView;

    public LaCasaFragment() {
    }

    public static LaCasaFragment newInstance() {
        LaCasaFragment fragment = new LaCasaFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invokeActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lacasa, container, false);
        unbinder = ButterKnife.bind(this, view);
        invokeActionBar();

        progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new Callback());
        webView.loadUrl(Constants.LA_CASA);

        if (Build.VERSION.SDK_INT >= 19)
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        else
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBuilder.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBuilder.dismiss();
            }
        });

        return view;
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

    private void invokeActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar_fragment, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        AppCompatImageView title = activity.findViewById(R.id.action_bar_return_string);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(MenuEventFragment.newInstance());
            }
        });
    }

    protected void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

}
