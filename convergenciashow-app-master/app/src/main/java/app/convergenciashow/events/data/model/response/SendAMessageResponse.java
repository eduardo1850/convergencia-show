package app.convergenciashow.events.data.model.response;

public class SendAMessageResponse extends BaseResponse {

    private Long idElement;

    public Long getIdElement() {
        return idElement;
    }

    public void setIdElement(Long idElement) {
        this.idElement = idElement;
    }
}
