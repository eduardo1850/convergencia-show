package app.convergenciashow.events.domain;

public class GallerySections {

    private int elements;
    private String date;

    public GallerySections(int elements, String date) {
        this.elements = elements;
        this.date = date;
    }

    public int getElements() {
        return elements;
    }

    public void setElements(int elements) {
        this.elements = elements;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
