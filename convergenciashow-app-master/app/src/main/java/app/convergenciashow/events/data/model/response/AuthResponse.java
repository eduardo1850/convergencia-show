package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class AuthResponse extends BaseResponse {

    private Long idUser;
    private String idSession;
    private String userName;
    private String userLastName;
    private Integer imageVersion;
    private String userDescription;
    private String email;
    private String role;
    private Long defaultEvent;
    private String companyName;
    private boolean alreadyHasPicture;
    private boolean verified;
    private ArrayList<AuthEventResponse> events;
    private boolean canUserBeReached;

    public Long getDefaultEvent() {
        return defaultEvent;
    }

    public void setDefaultEvent(Long defaultEvent) {
        this.defaultEvent = defaultEvent;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public Integer getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(Integer imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public boolean isAlreadyHasPicture() {
        return alreadyHasPicture;
    }

    public void setAlreadyHasPicture(boolean alreadyHasPicture) {
        this.alreadyHasPicture = alreadyHasPicture;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public ArrayList<AuthEventResponse> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<AuthEventResponse> events) {
        this.events = events;
    }

    public boolean isCanUserBeReached() {
        return canUserBeReached;
    }

    public void setCanUserBeReached(boolean canUserBeReached) {
        this.canUserBeReached = canUserBeReached;
    }
}
