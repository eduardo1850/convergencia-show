package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.request.AuthRequest;
import app.convergenciashow.events.data.model.request.AuthRequestLinkedIn;
import app.convergenciashow.events.data.model.request.GotoEventRequest;
import app.convergenciashow.events.data.model.request.NewRegisterRequest;
import app.convergenciashow.events.data.model.request.RecoverPassRequest;
import app.convergenciashow.events.data.model.request.UpdateSessionDeviceTokenRequest;
import app.convergenciashow.events.data.model.request.CanBeReachedByMessageRequest;
import app.convergenciashow.events.data.model.response.AuthResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.GetDetailsOfSpeakerResponse;
import app.convergenciashow.events.data.model.response.GetDetailsOfUserResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface AuthService {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("auth/login-with-event-key")
    Call<AuthResponse> getAuth(@Body AuthRequest request);

    @POST("auth/login-with-credentials")
    Call<AuthResponse> getLogin(@Body AuthRequest request);

    @POST("auth/login-with-linkedin")
    Call<AuthResponse> getLoginLinkedIn(@Body AuthRequestLinkedIn request);

    @PUT("auth/update-session-device-token")
    Call<AuthResponse> updateSessionDeviceToken(@Header("auth") String headerAut, @Body UpdateSessionDeviceTokenRequest request);

    @DELETE("auth/logout")
    Call<BaseResponse> getLoguot(@Header("auth") String headerAut);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("auth/recover-password")
    Call<BaseResponse> getPasswordRecovery(@Body RecoverPassRequest request);

    @Multipart
    @POST("users/update-profile-data")
    Call<BaseResponse> update(@Header("auth") String headerAut, @Part MultipartBody.Part image, @Part("user") RequestBody user);

    @Headers({"Content-Type: application/json"})
    @POST("users/create-profile")
    Call<AuthResponse> getNewRegister(@Body NewRegisterRequest request);

    @Headers({"Content-Type: application/json"})
    @POST("users/mark-in-event")
    Call<BaseResponse> markInEvent(@Header("auth") String headerAut, @Body GotoEventRequest request);

    @Headers({"Content-Type: application/json"})
    @POST("users/can-be-reached-by-message")
    Call<BaseResponse> canBeReachedByMessage(
            @Header("auth") String headerAut,
            @Body CanBeReachedByMessageRequest request);

    @GET("users/get-details-of-user-in-event")
    Call<GetDetailsOfUserResponse> getDetailsOfUser(
            @Query("idEvent") Long idEvent,
            @Query("idUser") Long idUser
    );

    @GET("users/get-speaker-details")
    Call<GetDetailsOfSpeakerResponse> getDetailsOfSpeaker(
            @Header("auth-admin") String headerAut,
            @Query("idSpeaker") Long idSpeaker
    );

}
