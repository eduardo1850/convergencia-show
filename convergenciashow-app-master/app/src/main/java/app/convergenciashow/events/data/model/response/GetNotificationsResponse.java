package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class GetNotificationsResponse extends BaseResponse {

    private ArrayList<NotificationResponse> notifications;

    public ArrayList<NotificationResponse> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<NotificationResponse> notifications) {
        this.notifications = notifications;
    }
}
