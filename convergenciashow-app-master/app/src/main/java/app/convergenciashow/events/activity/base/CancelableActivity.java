package app.convergenciashow.events.activity.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import app.convergenciashow.events.R;

public class CancelableActivity extends AppCompatActivity {

    protected String titleBar = "";
    protected static final String SO = "A";
    private static final String FRAGMENT_TO_SHOW = "fragmmentToShow";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initRepository();
        setComponent();
        invokeActionBar(titleBar);
    }

    protected void invokeActivity(Activity activity, Class clazz, boolean shouldFinish) {
        Intent intent = new Intent(activity, clazz);
        startActivity(intent);
        if(shouldFinish){
            activity.finish();
        }
    }

    private void invokeActionBar(String titleBar) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_cancelable_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        ImageView title = findViewById(R.id.close_btn);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

   protected void setComponent(){
        //Implement in activity
   }

   protected void initRepository() {
        //Implement in activity
   }

   public void setFragmentToShow(int fragmentToShow) {
       SharedPreferences.Editor editor = getPreferences().edit();
       editor.putInt(FRAGMENT_TO_SHOW, fragmentToShow);
       editor.commit();
   }

   public int getFragmentToShow(){
        return getPreferences().getInt(FRAGMENT_TO_SHOW, 1);
   }

   private SharedPreferences getPreferences() {
        return getPreferences(Context.MODE_PRIVATE);
   }
}
