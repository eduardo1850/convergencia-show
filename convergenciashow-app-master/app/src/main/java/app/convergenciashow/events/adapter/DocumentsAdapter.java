package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.DocumentDescriptionActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.domain.Document;
import app.convergenciashow.events.fragment.ContentFragment;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.ViewHolder> {

    private ContentFragment fragment;
    private Activity activity;
    private Context context;
    private List<Document> documents;
    private Unbinder unbinder;
    private AlertBuilderView progressBuilder;

    public DocumentsAdapter(List<Document> document, ContentFragment fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
        this.context = fragment.getContext();
        this.documents = document;
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
        progressBuilder.show();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.document_item, viewGroup, false);
        DocumentsAdapter.ViewHolder vh = new DocumentsAdapter.ViewHolder(v);
        progressBuilder.dismiss();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentsAdapter.ViewHolder viewHolder, int i) {

        final Document d = documents.get(i);
        viewHolder.documents_tv.setText(d.getTitle());

        viewHolder.ll_document_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, DocumentDescriptionActivity.class);
                intent.putExtra(Constants.DOCUMENTURL, Utils.getDocumentUrl(d.getId(), d.getDocumentVersion()));
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_document_item)
        View ll_document_item;
        @BindView(R.id.documents_tv)
        TextView documents_tv;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

}
