package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.SpeakersResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ExpositoresService {

    @GET("users/mobile/get-speakers-in-event")
    Call<SpeakersResponse> list(
            @Header("auth") String headerAuth,
            @Query("idEvent") String idEvent);
}