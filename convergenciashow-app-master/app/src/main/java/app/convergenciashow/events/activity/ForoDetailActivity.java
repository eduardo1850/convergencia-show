package app.convergenciashow.events.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.CancelableActivity;
import app.convergenciashow.events.adapter.ForoDetailAdapter;
import app.convergenciashow.events.data.model.response.PutCommentResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ForoRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Lines;
import app.convergenciashow.events.domain.Post;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.CommentsCallBack;
import app.convergenciashow.events.util.WSCallbacks.PutCommentCallBack;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergenciashow.events.util.Constants.JPG;

public class ForoDetailActivity extends CancelableActivity {

    @BindView(R.id.foropost_iv_user)
    CircleImageView foropost_iv_user;
    @BindView(R.id.foropost_tv_date)
    TextView foropost_tv_date;
    @BindView(R.id.foropost_tv_name)
    TextView foropost_tv_name;
    @BindView(R.id.foropost_ll_likes)
    LinearLayout foropost_ll_likes;
    @BindView(R.id.foropost_iv_likes)
    ImageView foropost_iv_likes;
    @BindView(R.id.foropost_tv_likes)
    TextView foropost_tv_likes;
    @BindView(R.id.foropost_iv_postimage)
    ImageView foropost_iv_postimage;
    @BindView(R.id.detailsRv)
    RecyclerView detailsRv;
    @BindView(R.id.rl_do_a_comment)
    RelativeLayout rl_do_a_comment;
    @BindView(R.id.commentTxt)
    EditText commentsTxt;
    @BindView(R.id.btnEnviar)
    TextView enviarButton;

    private Unbinder unbinder;
    private ForoDetailAdapter adapter;
    private Lines lines;
    private ForoRepository repository;
    private AuthRepository authRepository;
    private Long loadedElements = 0L;
    private boolean loading;
    private static final int rows = 100;
    private int numberofcomments = 0;
    Long id = 0L;
    String name = "";
    AuthUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_foro_detail_main);
        unbinder = ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        repository = new ForoRepository(this);
        authRepository = new AuthRepository(getApplicationContext(),this);
        user = authRepository.getAuth();
        loadElements();
    }

    public void showBoxComment() {
        rl_do_a_comment.setVisibility(View.VISIBLE);
        //enviarButton.setVisibility(View.VISIBLE);
        //commentsTxt.setVisibility(View.VISIBLE);
    }

    public void hideBoxComment() {
        rl_do_a_comment.setVisibility(View.GONE);
    }

    private String getProfileImageUrl(Long profileId) {
        return Constants.IMAGE_PROFILE_PATH + profileId + JPG;
    }

    @Override
    protected void setComponent() {

        if (getIntent().getExtras() != null) {
            Post item = (Post) getIntent().getSerializableExtra("postObject");
            numberofcomments = item.getMessages();

            Glide.with(this).load(getProfileImageUrl(item.getIdOwner()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            foropost_iv_user.setImageResource(R.drawable.default_user);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(foropost_iv_user);

            foropost_tv_date.setText(Utils.getParseForumDate(item.getDate()));
            foropost_tv_name.setText(item.getOwnerName());
            foropost_tv_likes.setText(item.getUpvotes() + "");
            Utils.changeLikeStatus(item.isVoted(), foropost_iv_likes);

            if (item.isHasPicture())
                Glide.with(this).load(Utils.getForumImageUrl(this, item.getId(), item.getImageVersion())).into(foropost_iv_postimage);
        }

        lines = new Lines();
        Intent intent = getIntent();
        id = intent.getLongExtra("id", 0L);
        name = intent.getStringExtra("postName");

        lines.setLine1(name);
        lines.setLine2("");
        lines.setLine3("Comentar");

        adapter = new ForoDetailAdapter(lines, numberofcomments, this);
        detailsRv.setAdapter(adapter);
        detailsRv.setLayoutManager(new LinearLayoutManager(this));

        enviarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repository.putComment(user, id, commentsTxt.getText().toString(), new PutCommentCallBack() {
                    @Override
                    public void onSuccess(@NonNull PutCommentResponse value) {
                        hideBoxComment();
                        reset();
                        loadElements();
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {

                    }
                });
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    private void reset() {
        loadedElements = 0L;
    }

    public void loadElements() {
        repository.getComments(user, id, loadedElements, rows, new CommentsCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<Lines> value) {
                adapter.update(value);
                loadedElements = loadedElements + rows;
                loading = false;
            }

            @Override
            public void onError(@NonNull Throwable throwable) {

            }
        });
    }


}
