package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class GetConversationsResponse extends BaseResponse {

    private ArrayList<ConversationResponse> conversations;

    public ArrayList<ConversationResponse> getConversations() {
        return conversations;
    }

    public void setConversations(ArrayList<ConversationResponse> conversations) {
        this.conversations = conversations;
    }
}
