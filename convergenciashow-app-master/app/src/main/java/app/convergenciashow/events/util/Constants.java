package app.convergenciashow.events.util;

public class Constants {

    private static boolean isProd = true;

    public static String BASE_URL = isProd
            ? "http://clieadmservices.goweb.tv:8080/CONVERGENCIASHOWAppV1/"
            : "http://clieadmservices.goweb.tv:8080/CONVERGENCIASHOWAppV1/";

    public static String IMAGE_PROFILE_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/profile_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/profile_images/";

    public static String IMAGE_SPEAKERS_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/speaker_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/speaker_images/";

    public static String EVENT_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/event_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/event_images/";

    public static String NEWS_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/news_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/news_images/";

    public static String AUTHOR_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/manager_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/manager_images/";

    public static String FORUM_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/forum_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/forum_images/";

    public static String PDF_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/event_maps/"
            : "https://www.goweb.tv/convergencia_show_events/app/event_maps/";

    public static String GALLERY_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/gallery_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/gallery_images/";

    public static String DOCUMENTS_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/documents/"
            : "https://www.goweb.tv/convergencia_show_events/app/documents/";

    public static String SPONSORS_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/sponsor_images/"
            : "https://www.goweb.tv/convergencia_show_events/app/sponsor_images/";

    public static String MAP_URL_PATH = isProd
            ? "https://www.goweb.tv/convergencia_show_events/app/event_maps/global-"
            : "https://www.goweb.tv/convergencia_show_events/app/event_maps/global-";
    
    public static String FetchProfileDataURI = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,public-profile-url,picture-url,email-address,picture-urls::(original))";
    public static String PDFGoogleReader = "http://docs.google.com/gview?embedded=true&url=";
    public static String LA_CASA = "https://www.cemefi.org/lacasa";
    public static String TWITTER = "https://mobile.twitter.com/convergenciamx?lang=es";
    public static String TERMS_AND_CONDITIONS = "https://www.cemefi.org/avisos/AVISO_PRIVACIDAD.pdf";
    public static String PDF = ".pdf";
    public static String JPG = ".jpg";
    public static String VER = "?v=";

    //Push Notification
    public static String RECEIVING_MESSAGE = "RECEIVING_MESSAGE";
    public static String MESSAGES_DATE_FORMAT = "yyyy-MM-dd HH:mm:SS.s";
    public static String SCHEDULES_DATE_FORMAT = "yyyy-MM-dd";

    // NavigationBottom
    public static int FRAGMENT_EVENT = 1;
    public static int FRAGMENT_NEWS = 2;
    public static int FRAGMENT_PROFILE = 3;
    public static int FRAGMENT_PUSH = 4;
    public static int FRAGMENT_CHAT = 5;
    public static int FRAGMENT_SCHEDULE = 6;
    public static int FRAGMENT_PERSONAL_SCHEDULE = 7;
    public static int FRAGMENT_GALLERY = 8;
    public static int FRAGMENT_DOCUMENTS = 9;
    public static int FRAGMENT_FORUM = 10;
    public static int FRAGMENT_SPEAKERS = 11;
    public static int FRAGMENT_ASSISTANTS = 12;
    public static int FRAGMENT_POLLS = 13;
    public static int PERMISSIONS_CODE = 1504;
    public static int ZBAR_CAMERA_PERMISSION = 1;

    // Polls
    public static String POLLS_ERROR_CODE = "1702";
    public static String POLLS_NO_MORE_QUESTIONS = "1704";
    public static String QUESTION_ACTIVITY = "QuestionActivity";
    public static String GLOBAL_QUESTION_ACTIVITY = "GlobalQuestionActivity";
    public static String ID_POLL = "IdPoll";

    // AppPreferences keys
    public static String ID_EVENT = "iDEvent";
    public static String EVENT_MAP_AVAILABLE = "isMapAvailable";
    public static String EVENT_MAP_VERSION = "mapVersion";
    public static final String FRAGMENT_TO_SHOW = "fragmmentToShow";
    public static final String STACK_TRACE = "stack_trace";
    public static String INTERNET_CODE = "1234";

    public static String ANDROID_APP = "A";
    public static String IDUSERTOCONVERSE = "iDUserToConverse";
    public static String IDEVENT = "idEvent";
    public static String NAMEUSERTOCONVERSE = "nameUserToConverse";
    public static String IMAGEUSERTOCONVERSE = "imageUserToConverse";
    public static String PICTUREURL = "pictureURL";
    public static String PICTUREDECRIPTION = "pictureDescription";
    public static String DOCUMENTURL = "documentURL";
    public static String LinkedIn_Package = "com.linkedin.android";

}
