package app.convergenciashow.events.data.database;

public class AuthContractor {

    public static abstract class AuthEntry {

        public static final String TABLE_NAME = "auth";

        public static final String ID_USER = "idUser";
        public static final String ID_SESSION = "idSession";
        public static final String USERNAME = "userName";
        public static final String USER_LASTNAME = "userLastName";
        public static final String IMAGE_VERSION = "imageVersion";
        public static final String USER_DESCRIPTION = "userDescription";
        public static final String EMAIL = "email";
        public static final String ROLE = "role";
        public static final String COMPANY_NAME = "companyName";
        public static final String HAS_PICTURE = "alreadyHasPicture";
        public static final String IS_VERIFIED = "verified";
        public static final String CAN_BE_REACHED = "canUserBeReached";

        public static String CREATE_AUTH_QUERY = "CREATE TABLE " + TABLE_NAME + " ( "
                + ID_USER + " INTEGER PRIMARY KEY, "
                + ID_SESSION + " TEXT, "
                + USERNAME + " TEXT, "
                + USER_LASTNAME + " TEXT, "
                + IMAGE_VERSION + " INTEGER, "
                + USER_DESCRIPTION + " TEXT, "
                + EMAIL + " TEXT, "
                + ROLE + " TEXT, "
                + COMPANY_NAME + " TEXT, "
                + HAS_PICTURE + " INTEGER, "
                + IS_VERIFIED + " INTEGER, "
                + CAN_BE_REACHED + " INTEGER "
                + " ) ";

        public static String DELETE_AUTH_QUERY = "DELETE FROM " + TABLE_NAME;

        public static String SELECT_AUTH_QUERY = "SELECT * FROM " + TABLE_NAME;

        /*public static String SELECT_AUTH_QUERY = "SELECT "
                + ID_USER + ", " + ID_SESSION + ", "
                + USERNAME + ", " + USER_LASTNAME + ", "
                + IMAGE_VERSION + ", " + USER_DESCRIPTION + ", "
                + EMAIL + ", " + ROLE + ", " + COMPANY_NAME + ", "
                + HAS_PICTURE + " FROM " + TABLE_NAME;*/
    }


}