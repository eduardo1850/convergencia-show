package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.MarkInEventRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ScheduleRepository;
import app.convergenciashow.events.domain.Expo;
import app.convergenciashow.events.fragment.EventScheduleFragment;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.util.ArrayList;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class EventScheduleAdapter extends RecyclerView.Adapter<EventScheduleAdapter.ViewHolder> {

    private ArrayList<Object> items;
    private Context context;
    private final AuthRepository authRepository;
    private final ScheduleRepository repository;
    private Activity activity;
    private AlertBuilderView progressBuilder;

    public EventScheduleAdapter(ArrayList<Object> items, EventScheduleFragment fragment, AuthRepository authRepository) {
        this.items = items;
        this.context = fragment.getContext();
        this.activity = fragment.getActivity();
        this.authRepository = authRepository;
        this.repository = new ScheduleRepository(fragment.getContext());
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
    }

    public void update(ArrayList<Object> expoItems) {
        items.addAll(expoItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EventScheduleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        ViewHolder vh;
        // create a ne<w view
        switch (viewType) {
            case 2:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_schedule_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.date_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final EventScheduleAdapter.ViewHolder viewHolder, final int i) {
        final Object n = items.get(i);
        String date;
        String hour;

        if (n instanceof String) {
            viewHolder.dateCalendar.setText(Utils.setDateSchedule(((String) n), activity));
        } else {
            hour = ((Expo) n).getDate().substring(11, 16);
            viewHolder.titleCalendar.setText(((Expo) n).getTitle());
            //viewHolder.hourCalendar.setText(hour + " - " + ((Expo) n).getRoomTitle());
            viewHolder.hourCalendar.setText(Utils.getDescription(n));

            viewHolder.speakersCalendar.setText(((Expo) n).getSpeakers());

            if (((Expo) n).isMarkedForPersonalAgenda())
                Glide.with(context).load(R.drawable.calendar_yes).into(viewHolder.image);
            else
                Glide.with(context).load(R.drawable.calendar_no).into(viewHolder.image);

            viewHolder.idExpo = ((Expo) n).getId();
            viewHolder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!((Expo) n).isMarkedForPersonalAgenda()) {
                        progressBuilder.show();
                        MarkInEventRequest request = new MarkInEventRequest(viewHolder.idExpo, true);
                        repository.markForPersonalSchedule(authRepository.getAuth(), request, new GenericCallBack() {
                            @Override
                            public void onSuccess(@NonNull BaseResponse value) {
                                ((Expo) items.get(i)).setMarkedForPersonalAgenda(true);
                                notifyDataSetChanged();
                                progressBuilder.dismiss();
                            }

                            @Override
                            public void onError(@NonNull BaseResponse baseResponse) {
                                progressBuilder.dismiss();
                                if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                    AlertBuilderView alertBuilderView = new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.session_error));
                                    alertBuilderView.show();
                                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            authRepository.logOut();
                                            Utils.invokeActivity(activity, LoginActivity.class, true);
                                        }
                                    });
                                } else {
                                    Glide.with(context).load(R.drawable.calendar_no).into(viewHolder.image);
                                }
                            }
                        });
                    }
                }
            });

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof String) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleCalendar;
        TextView hourCalendar;
        TextView speakersCalendar;
        TextView dateCalendar;
        ImageView image;
        Long idExpo;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);


            if (viewType == 2) {
                titleCalendar = itemView.findViewById(R.id.titleCalendar);
                hourCalendar = itemView.findViewById(R.id.hourCalendar);
                speakersCalendar = itemView.findViewById(R.id.speakersCalendar);
                image = itemView.findViewById(R.id.imageCalendar);
            } else if (viewType == 1) {
                dateCalendar = itemView.findViewById(R.id.itemDate);
            }

        }
    }


}
