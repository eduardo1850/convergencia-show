package app.convergenciashow.events.data.model.response;

public class MessagesResponse {

    private Long idMessage;
    private Long idUser;
    private String message;
    private String addedDate;

    public MessagesResponse(Long idMessage, Long idUser, String message, String addedDate) {
        this.idMessage = idMessage;
        this.idUser = idUser;
        this.message = message;
        this.addedDate = addedDate;
    }

    public Long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(Long idMessage) {
        this.idMessage = idMessage;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }
}
