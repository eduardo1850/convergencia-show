package app.convergenciashow.events.data.service;

import app.convergenciashow.events.util.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ServiceManger {

    public static NewsService getNewsService() {
       return getInstance().create(NewsService.class);
    }

    public static StreamService getStreamingService() {
        return getInstance().create(StreamService.class);
    }

    public static ForoService getForoService() {
        return getInstance().create(ForoService.class);
    }

    public static AsistentesService getAsistentesService() {
        return getInstance().create(AsistentesService.class);
    }

    public static ExpositoresService getExpositoresService() {
        return getInstance().create(ExpositoresService.class);
    }

    public static PlanoService getPlanoService() {
        return getInstance().create(PlanoService.class);
    }

    public static EventDetailService getEventDetailService() {
        return getInstance().create(EventDetailService.class);
    }

    public static ScheduleService getScheduleService() {
        return getInstance().create(ScheduleService.class);
    }

    public static AuthService getAuthService() {
        return getInstance().create(AuthService.class);
    }

    public static getNotificationsService getNotificationsService() {
        return getInstance().create(getNotificationsService.class);
    }

    public static ConversationsService getConversationsService() {
        return getInstance().create(ConversationsService.class);
    }

    private static Retrofit getInstance() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(logging);

       return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }
}
