package app.convergenciashow.events.data.model.response;

import java.util.List;

public class GalleryResponse extends BaseResponse {

    private List<PicturesResponse> pictures;

    public List<PicturesResponse> getPictures() {
        return pictures;
    }

    public void setPictures(List<PicturesResponse> pictures) {
        this.pictures = pictures;
    }

}
