package app.convergenciashow.events.domain;

public class AuthEvent{
    private Long id;
    private Integer roleInEvent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRoleInEvent() {
        return roleInEvent;
    }

    public void setRoleInEvent(Integer roleInEvent) {
        this.roleInEvent = roleInEvent;
    }
}
