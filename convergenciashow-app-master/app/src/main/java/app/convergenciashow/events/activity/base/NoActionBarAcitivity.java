package app.convergenciashow.events.activity.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

public class NoActionBarAcitivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
    }
}
