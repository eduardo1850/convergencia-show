package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.PutCommentResponse;

public interface PutCommentCallBack {

    void onSuccess(@NonNull PutCommentResponse value);

    void onError(@NonNull Throwable throwable);
}
