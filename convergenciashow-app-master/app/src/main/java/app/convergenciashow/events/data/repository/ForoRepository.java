package app.convergenciashow.events.data.repository;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.Gson;

import app.convergenciashow.events.data.model.request.CommentRequest;
import app.convergenciashow.events.data.model.request.LikeRequest;
import app.convergenciashow.events.data.model.request.PostRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.CommentsResponse;
import app.convergenciashow.events.data.model.response.DetailsResponse;
import app.convergenciashow.events.data.model.response.ForoResponse;
import app.convergenciashow.events.data.model.response.PostResponse;
import app.convergenciashow.events.data.model.response.PutCommentResponse;
import app.convergenciashow.events.data.model.response.PutPostResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Lines;
import app.convergenciashow.events.domain.Post;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.CommentsCallBack;
import app.convergenciashow.events.util.WSCallbacks.ForoCallBack;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;
import app.convergenciashow.events.util.WSCallbacks.PutCommentCallBack;
import app.convergenciashow.events.util.WSCallbacks.PutPostCallBack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getForoService;

public class ForoRepository {

    private Context ctx;

    public ForoRepository(Context ctx) {
        this.ctx = ctx;
    }

    public void getPost(AuthUser user, String IdEvent, Long loadedElements, Integer numberOfRows, final ForoCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<ForoResponse> callResponse = getForoService().getPost(userHeader, loadedElements, numberOfRows, IdEvent);
        callResponse.enqueue(new Callback<ForoResponse>() {
            @Override
            public void onResponse(Call<ForoResponse> call, Response<ForoResponse> response) {
                cb.onSuccess(transform(response.body()));
            }

            @Override
            public void onFailure(Call<ForoResponse> call, Throwable t) {
                cb.onError(t);
            }
        });
    }

    public void putPost(AuthUser user, PostRequest request, Bitmap image, final PutPostCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        RequestBody post = null;
        MultipartBody.Part body = null;
        if (request.getDescription() != null && !request.getDescription().equals("")) {
            Gson gson = new Gson();
            post = RequestBody.create(MediaType.parse("application/json"), gson.toJson(request));
        }
        if (image != null) {
            File file = convertImageToFile(image);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        }


        Call<PutPostResponse> callResponse = getForoService().putPost(userHeader, body, post);
        callResponse.enqueue(new Callback<PutPostResponse>() {
            @Override
            public void onResponse(Call<PutPostResponse> call, Response<PutPostResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    cb.onError(Utils.parse409(response.errorBody(), ctx));
                }
            }

            @Override
            public void onFailure(Call<PutPostResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getComments(AuthUser user, Long idPost, Long loadedElements, Integer numberOfRows, final CommentsCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<DetailsResponse> callResponse = getForoService().getComments(userHeader, loadedElements, numberOfRows, idPost);
        callResponse.enqueue(new Callback<DetailsResponse>() {
            @Override
            public void onResponse(Call<DetailsResponse> call, Response<DetailsResponse> response) {
                cb.onSuccess(transform(response.body()));
            }

            @Override
            public void onFailure(Call<DetailsResponse> call, Throwable t) {
                cb.onError(t);
            }
        });
    }

    public void putLike(AuthUser user, LikeRequest request, final GenericCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<BaseResponse> callResponse = getForoService().putLike(userHeader, request);
        callResponse.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    cb.onError(Utils.parse409(response.errorBody(), ctx));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });

    }


    private File convertImageToFile(Bitmap bitmap) {
        File f = new File(ctx.getCacheDir(), "imageForo");
        try {
            f.createNewFile();


            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();


            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void putComment(AuthUser user, Long idPost, String description, final PutCommentCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();

        CommentRequest request = new CommentRequest(idPost, description);
        Call<PutCommentResponse> callResponse = getForoService().putComment(userHeader, request);
        callResponse.enqueue(new Callback<PutCommentResponse>() {
            @Override
            public void onResponse(Call<PutCommentResponse> call, Response<PutCommentResponse> response) {
                cb.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<PutCommentResponse> call, Throwable t) {
                cb.onError(t);
            }
        });
    }

    private ArrayList<Post> transform(ForoResponse response) {
        ArrayList<PostResponse> postResponses = response.getPosts();
        ArrayList<Post> posts = new ArrayList<>();
        for (PostResponse pr : postResponses) {
            Post p = new Post();
            p.setDate(pr.getDate());
            p.setDescription(pr.getDescription());
            p.setHasPicture(pr.isHasPicture());
            p.setId(pr.getId());
            p.setIdOwner(pr.getIdOwner());
            p.setImageVersion(pr.getImageVersion());
            p.setMessages(pr.getMessages());
            p.setOwnerName(pr.getOwnerName());
            p.setTypeOfOwner(pr.getTypeOfOwner());
            p.setUpvotes(pr.getUpvotes());
            p.setVoted(pr.isVoted());
            posts.add(p);
        }

        return posts;
    }

    private ArrayList<Lines> transform(DetailsResponse response) {
        ArrayList<CommentsResponse> commentsResponse = response.getComments();
        ArrayList<Lines> lines = new ArrayList<>();
        for (CommentsResponse cr : commentsResponse) {
            Lines l = new Lines();
            l.setLine1(cr.getOwnerName());
            l.setLine2(cr.getDate());
            l.setLine3(cr.getDescription());
            l.setComments(0);
            lines.add(l);
        }

        return lines;
    }

}
