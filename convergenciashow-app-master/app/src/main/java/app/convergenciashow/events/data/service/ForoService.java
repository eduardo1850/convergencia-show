package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.request.CommentRequest;
import app.convergenciashow.events.data.model.request.LikeRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.DetailsResponse;
import app.convergenciashow.events.data.model.response.ForoResponse;
import app.convergenciashow.events.data.model.response.PutCommentResponse;
import app.convergenciashow.events.data.model.response.PutPostResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ForoService {

    @GET("forum/mobile/get-posts")
    Call<ForoResponse> getPost(
            @Header("auth") String headerAut,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows,
            @Query("idEvent") String idEvent);

    @Multipart
    @POST("forum/mobile/add-post")
    Call<PutPostResponse> putPost(
            @Header("auth") String headerAut,
            @Part MultipartBody.Part image,
            @Part("post") RequestBody name);

    @GET("forum/mobile/get-comments")
    Call<DetailsResponse> getComments(
            @Header("auth") String headerAut,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows,
            @Query("idPost") Long idPost);

    @POST("forum/mobile/add-comment")
    Call<PutCommentResponse> putComment(
            @Header("auth") String headerAut, @Body CommentRequest request);

    @POST("forum/mobile/vote-post")
    Call<BaseResponse> putLike(
            @Header("auth") String headerAut, @Body LikeRequest request);


}


