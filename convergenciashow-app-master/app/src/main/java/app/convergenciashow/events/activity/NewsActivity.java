package app.convergenciashow.events.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ReturnBaseActivity;
import app.convergenciashow.events.domain.New;
import app.convergenciashow.events.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NewsActivity extends ReturnBaseActivity {

    @BindView(R.id.authorImage)
    ImageView authorImage;
    @BindView(R.id.authorName)
    TextView authorName;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.newsImage)
    ImageView newsImage;
    @BindView(R.id.newsContent)
    TextView newsContent;
    @BindView(R.id.newTitle)
    TextView newTitle;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_news);
        unbinder = ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        if (getIntent() != null) {
            New n = (New) getIntent().getSerializableExtra("new_data");
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.picture);
            requestOptions.error(R.drawable.placeholderpicture);

            authorName.setText(n.getOwnerName());
            newTitle.setText(n.getTitle());
            date.setText(Utils.timeIsRunningOut(n.getDate(), this));
            newsContent.setText(n.getDescription());
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions)
                    .load(Utils.getAuthorImageUrl(n.getIdOwner(), 0)).into(authorImage);
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions)
                    .load(Utils.getNewsImageUrl(n.getId(), n.getImageVersion())).into(newsImage);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

}
