package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;

import app.convergenciashow.events.R;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static app.convergenciashow.events.util.Constants.DOCUMENTURL;
import static app.convergenciashow.events.util.Constants.FRAGMENT_DOCUMENTS;


public class DocumentDescriptionActivity extends AppCompatActivity {

    private String Document_url;
    private Unbinder unbinder;
    private AlertBuilderView progressBuilder, alertBuilderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_description);
        unbinder = ButterKnife.bind(this);
        invokeActionBar();
        progressBuilder = new AlertBuilderView(this, Operations.AlertBuilder_progress, "");
        if (getIntent().getExtras() != null) {
            Document_url = getIntent().getExtras().getString(DOCUMENTURL);
            if (!Document_url.isEmpty()) {
                try {
                    WebView webView = new WebView(this);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView.setWebViewClient(new Callback());
                    webView.loadUrl(Constants.PDFGoogleReader + Document_url);
                    setContentView(webView);

                    if (Build.VERSION.SDK_INT >= 19)
                        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    else
                        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon) {
                            super.onPageStarted(view, url, favicon);
                            progressBuilder.show();
                        }

                        @Override
                        public void onPageFinished(WebView view, String url) {
                            super.onPageFinished(view, url);
                            progressBuilder.dismiss();
                        }
                    });
                } catch (Exception e) {
                    alertDocumentError();
                }
            } else {
                alertDocumentError();
            }
        } else {
            alertDocumentError();
        }
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

    private void invokeActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_cancelable_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        ImageView title = findViewById(R.id.close_btn);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishIt();
            }
        });
    }

    private void finishIt() {
        AppPreferences.getInstance(DocumentDescriptionActivity.this).saveIntData(app.convergenciashow.events.util.Constants.STACK_TRACE, FRAGMENT_DOCUMENTS);
        unbinder.unbind();
        finish();
    }

    private void alertDocumentError() {
        progressBuilder.dismiss();
        alertBuilderView = new AlertBuilderView(this, Operations.AlertBuilder_error, getString(R.string.document_error));
        alertBuilderView.show();
        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finishIt();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

}
