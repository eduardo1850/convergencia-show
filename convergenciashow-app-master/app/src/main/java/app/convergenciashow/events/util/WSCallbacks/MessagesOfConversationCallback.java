package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.MessagesResponse;

public interface MessagesOfConversationCallback {

    void onSuccess(@NonNull ArrayList<MessagesResponse> response);

    void onError(@NonNull BaseResponse baseResponse);

}
