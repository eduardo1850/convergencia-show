package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.data.model.response.MessagesResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder> {

    private ArrayList<MessagesResponse> messagesList;
    private Activity activity;
    private AuthRepository authUser;
    private Long whoAMI;

    public ConversationAdapter(ArrayList<MessagesResponse> messages, Activity activity) {
        this.messagesList = messages;
        this.activity = activity;
        this.authUser = new AuthRepository(activity, activity);
        whoAMI = authUser.getAuth().getIdUser();
    }

    @NonNull
    @Override
    public ConversationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_message_item, viewGroup, false);
        ConversationAdapter.ViewHolder viewHolder = new ConversationAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final MessagesResponse messagesResponse = messagesList.get(i);
        viewHolder.ll_messageitem_left.setVisibility(View.GONE);
        viewHolder.ll_messageitem_rigth.setVisibility(View.GONE);
        if (messagesResponse.getIdUser().equals(whoAMI)) {
            viewHolder.ll_messageitem_rigth.setVisibility(View.VISIBLE);
            viewHolder.item_text_rigth.setText(messagesResponse.getMessage());
            viewHolder.item_date_rigth.setText(Utils.timeIsRunningOut(messagesResponse.getAddedDate(), activity));
        } else {
            viewHolder.ll_messageitem_left.setVisibility(View.VISIBLE);
            viewHolder.item_text_left.setText(messagesResponse.getMessage());
            viewHolder.item_date_left.setText(Utils.timeIsRunningOut(messagesResponse.getAddedDate(), activity));
        }
    }

    @Override
    public int getItemCount() {
        if (messagesList != null)
            return messagesList.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_ll_messageitem_left)
        LinearLayout ll_messageitem_left;
        @BindView(R.id.message_item_text_left)
        TextView item_text_left;
        @BindView(R.id.message_item_date_left)
        TextView item_date_left;

        @BindView(R.id.message_ll_messageitem_rigth)
        LinearLayout ll_messageitem_rigth;
        @BindView(R.id.message_item_text_rigth)
        TextView item_text_rigth;
        @BindView(R.id.message_item_date_rigth)
        TextView item_date_rigth;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /*private void setLayoutGratvity(LinearLayout layout, int gravity) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        params.weight = 1.0f;
        params.gravity = gravity;
        layout.setLayoutParams(params);
    }*/

    public void update() {
        //activity.runOnUiThread(new Runnable() {
        //@Override
        //public void run() {
        notifyDataSetChanged();
        //}
        //});
    }
}
