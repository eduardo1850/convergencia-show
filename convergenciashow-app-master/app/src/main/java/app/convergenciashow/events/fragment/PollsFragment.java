package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.PollsAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.Poll;
import app.convergenciashow.events.data.model.response.PollsResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.PollsCallback;
import app.convergenciashow.events.util.WSCallbacks.PollsFragmentCallback;

import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class PollsFragment extends ReturnBaseFragment implements View.OnClickListener {

    private RecyclerView asistentesRecyclerView;
    private LinearLayout llpolls;
    private AuthRepository authRepository;
    private EventRepository repository;
    private PollsAdapter adapter;
    private EditText pollsSearch;
    private Button globalPollButton;
    private Button speakerPollButton;
    private int pollType; //Define the poll type to get.1=globals,2=speaker

    public static PollsFragment newInstance() {
        PollsFragment fragment = new PollsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setReturnString("< " + getString(R.string.backto_event));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        authRepository = new AuthRepository(getContext(), getActivity());
        repository = new EventRepository(getContext());
        return inflater.inflate(R.layout.fragment_polls, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setComponent() {
        adapter = new PollsAdapter(new ArrayList<Poll>(), this, new PollsFragmentCallback() {
            @Override
            public void getActiveQuestionCB(Long idPoll) {
                //Log.d("lorem", "" + pollType);
                if (pollType == 1) {
                    if (isAdded())
                        Utils.getNextQuestion(idPoll, repository, authRepository, getActivity(), false);
                } else {
                    if (isAdded())
                        Utils.getActiveQuestion(idPoll, repository, authRepository, getActivity(), false);
                }
            }
        });

        llpolls = getActivity().findViewById(R.id.llpolls);

        globalPollButton = (Button) getActivity().findViewById(R.id.poll_global_button);
        globalPollButton.setOnClickListener(this);
        speakerPollButton = getActivity().findViewById(R.id.poll_speaker_button);
        speakerPollButton.setOnClickListener(this);

        pollsSearch = getActivity().findViewById(R.id.pollsSearch);
        pollsSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        asistentesRecyclerView = getActivity().findViewById(R.id.asistentesRv);
        asistentesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        asistentesRecyclerView.setAdapter(adapter);
        globalPollButton.performClick();
    }

    public void loadElements() {
        adapter.clear();
        String idEvent = AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT) + "";
        //Log.d("lorem", "" + pollType);
        repository.getPolls(authRepository.getAuth(), pollType, idEvent, "", new PollsCallback() {
            @Override
            public void onSuccess(@NonNull PollsResponse polls) {
                if (polls.getPolls().size() > 0) {
                    llpolls.setVisibility(GONE);
                    asistentesRecyclerView.setVisibility(VISIBLE);
                    adapter.update(polls.getPolls());
                } else {
                    llpolls.setVisibility(VISIBLE);
                    asistentesRecyclerView.setVisibility(GONE);
                }
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                onPollsError(baseResponse);
            }
        });
    }

    private void onPollsError(BaseResponse baseResponse) {
        setErrorLayout(true);
        if (baseResponse != null) {
            //if (baseResponse.getCodeNumber().equals(Constants.POLLS_ERROR_CODE)) {
            //    if (isAdded())
            //        Utils.invokeActivity(getActivity(), QuestionActivity.class, false);
            //} else
            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                if (isAdded()) {
                    AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            authRepository.logOut();
                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                        }
                    });
                }
            } else {
                if (isAdded())
                    new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
            }
        } else {
            if (isAdded())
                new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
        }
    }

    private void setErrorLayout(boolean error) {
        llpolls.setVisibility(error ? VISIBLE : GONE);
        asistentesRecyclerView.setVisibility(error ? GONE : View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        resetButtons();

        if (view.equals(getActivity().findViewById(R.id.poll_global_button))) {
            pollType = 1;
        } else if (view.equals(getActivity().findViewById(R.id.poll_speaker_button))) {
            pollType = 2;
        }
        ((Button) view).setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        ((Button) view).setTextColor(getResources().getColor(R.color.white));
        loadElements();
    }

    public void resetButtons() {
        globalPollButton.setPressed(false);
        speakerPollButton.setPressed(false);
        globalPollButton.setBackgroundColor(Color.TRANSPARENT);
        globalPollButton.setTextColor(getResources().getColor(R.color.gray_title));
        speakerPollButton.setBackgroundColor(Color.TRANSPARENT);
        speakerPollButton.setTextColor(getResources().getColor(R.color.gray_title));
    }
}
