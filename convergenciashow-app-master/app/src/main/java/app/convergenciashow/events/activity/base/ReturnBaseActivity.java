package app.convergenciashow.events.activity.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import app.convergenciashow.events.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ReturnBaseActivity extends AppCompatActivity {

    protected static final String SO = "A";
    private static final String FRAGMENT_TO_SHOW = "fragmmentToShow";
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initRepository();
        setComponent();
        invokeActionBar(getString(R.string.back));
    }

    protected void invokeActivity(Activity activity, Class clazz, boolean shouldFinish) {
        Intent intent = new Intent(activity, clazz);
        startActivity(intent);
        if (shouldFinish) {
            activity.finish();
        }
    }

    private void invokeActionBar(String titleBar) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        AppCompatImageView title = findViewById(R.id.action_bar_return_string);
        //title.setText(titleBar);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void setComponent() {
        //Implement in activity
    }

    protected void initRepository() {
        //Implement in activity
    }

    public void setFragmentToShow(int fragmentToShow) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(FRAGMENT_TO_SHOW, fragmentToShow);
        editor.commit();
    }

    public void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

    public int getFragmentToShow() {
        return getPreferences().getInt(FRAGMENT_TO_SHOW, 1);
    }

    private SharedPreferences getPreferences() {
        return getPreferences(Context.MODE_PRIVATE);
    }

   /* @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }*/
}
