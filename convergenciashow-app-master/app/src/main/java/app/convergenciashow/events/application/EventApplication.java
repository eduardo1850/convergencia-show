package app.convergenciashow.events.application;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.stetho.Stetho;

import app.convergenciashow.events.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class EventApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Facebook Stetho
        Stetho.initializeWithDefaults(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/HelveticaNeueLTCom-Md.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
