package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.PollsResponse;

public interface PollsCallback {

    void onSuccess(@NonNull PollsResponse polls);

    void onError(@NonNull BaseResponse baseResponse);

}
