package app.convergenciashow.events.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.barteksc.pdfviewer.PDFView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapFragment extends ReturnBaseFragment {

    private PDFView mapImage;
    private LinearLayout llmap;

    public MapFragment() {
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setReturnString(getString(R.string.back));
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Activity activity = getActivity();

        mapImage = activity.findViewById(R.id.mapImage);
        llmap = activity.findViewById(R.id.llmap);

        if (AppPreferences.getInstance(getActivity()).getBooleanData(Constants.EVENT_MAP_AVAILABLE)) {
            try {
                new RetrievePDFStream().execute(
                        Utils.getMapUrl(
                                AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT),
                                AppPreferences.getInstance(getActivity()).getIntData(Constants.EVENT_MAP_VERSION)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            setError(true);
        }
    }

    class RetrievePDFStream extends AsyncTask<String, Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL urlx = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) urlx.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    setError(false);
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                } else {
                    setError(true);
                    return null;
                }
            } catch (IOException e) {
                setError(true);
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            mapImage.fromStream(inputStream).load();
        }
    }

    private void setError(final boolean thereAreError) {
        if (isAdded()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (thereAreError) {
                        llmap.setVisibility(View.VISIBLE);
                        mapImage.setVisibility(View.GONE);
                    } else {
                        llmap.setVisibility(View.GONE);
                        mapImage.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

}
