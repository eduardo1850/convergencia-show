package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.ConversationsAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ConversationResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ConversationRepository;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.ConversationsCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ConversationsFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.chat_rv)
    RecyclerView chat_rv;

    private Long loadedElements = 0L;
    private ConversationRepository repository;
    private static final int rows = 7;
    private AlertBuilderView progressBuilder;
    private Unbinder unbinder;
    private int rvLastVisibleItem;
    private boolean loading = false;
    private ConversationsAdapter adapter;

    public static ConversationsFragment newInstance() {
        ConversationsFragment fragment = new ConversationsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(getString(R.string.conversaciones));
        super.onCreate(savedInstanceState);
        //progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    protected void setComponent() {
        adapter = new ConversationsAdapter(new ArrayList<ConversationResponse>(), getContext());
        repository = new ConversationRepository(getContext());
        chat_rv.setLayoutManager(new LinearLayoutManager(getContext()));
        chat_rv.swapAdapter(adapter, true);
        chat_rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) chat_rv.getLayoutManager();
                rvLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (chat_rv.getChildCount() > 0) {
                    if (!loading && rvLastVisibleItem == chat_rv.getChildCount() - 1) {
                        loading = true;
                        loadConversations();
                    }
                }
            }
        });
        loadConversations();
    }

    public void loadConversations() {
        //progressBuilder.show();
        final AuthRepository authRepository;
        try {
            if (isAdded()) {
                authRepository = new AuthRepository(getContext(), getActivity());
                repository.getConversations(authRepository.getAuth(), loadedElements, rows, new ConversationsCallback() {
                    @Override
                    public void onSuccess(@NonNull ArrayList<ConversationResponse> response) {
                        //progressBuilder.dismiss();
                        adapter.update(response);
                        loadedElements = loadedElements + rows;
                        loading = false;
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        //progressBuilder.dismiss();
                        if (baseResponse != null) {
                            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                if (isAdded()) {
                                    AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                                    alertBuilderView.show();
                                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            authRepository.logOut();
                                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                                        }
                                    });
                                }
                            } else {
                                if (isAdded())
                                    new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            }
                        } else {
                            if (isAdded())
                                new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                        }
                    }
                });
            }
        } catch (Exception e) {
            new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
        }

    }

}
