package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.PutPostResponse;

public interface PutPostCallBack {

    void onSuccess(@NonNull PutPostResponse value);

    void onError(@NonNull BaseResponse baseResponse);
}
