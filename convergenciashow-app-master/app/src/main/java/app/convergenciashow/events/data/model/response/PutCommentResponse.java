package app.convergenciashow.events.data.model.response;

public class PutCommentResponse extends BaseResponse {
    private Long idElement;

    public Long getIdElement() {
        return idElement;
    }

    public void setIdElementM(Long idElement) {
        this.idElement = idElement;
    }
}
