package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class PollsResponse extends BaseResponse {

    private ArrayList<Poll> polls;

    public ArrayList<Poll> getPolls() {
        return polls;
    }

    public void setPolls(ArrayList<Poll> polls) {
        this.polls = polls;
    }
}
