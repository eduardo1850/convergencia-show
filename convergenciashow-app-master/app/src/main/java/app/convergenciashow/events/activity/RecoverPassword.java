package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.RecoverPassRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecoverPassword extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.resetpass_til_email)
    TextInputLayout til_email;
    @BindView(R.id.resetpass_et_email)
    EditText et_email;
    @BindView(R.id.resetpass_btn_reset)
    Button btn_reset;
    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        setTitle("");

        btn_reset.setOnClickListener(this);
        authRepository = new AuthRepository(getApplicationContext(), this);
        progressBuilder = new AlertBuilderView(RecoverPassword.this, Operations.AlertBuilder_progress, "");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.resetpass_btn_reset:
                if (validate()) {
                    progressBuilder.show();
                    RecoverPassRequest recoverPassRequest = new RecoverPassRequest(et_email.getText().toString());
                    authRepository.recoverPassword(recoverPassRequest, new GenericCallBack() {
                        AlertBuilderView alertBuilderView;

                        @Override
                        public void onSuccess(@NonNull BaseResponse baseResponse) {
                            progressBuilder.dismiss();
                            alertBuilderView = new AlertBuilderView(RecoverPassword.this, Operations.AlertBuilder_success, getString(R.string.resetpass_successoperation));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    Utils.invokeActivity(RecoverPassword.this, LoginActivity.class, true);
                                }
                            });
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            progressBuilder.dismiss();
                            if (baseResponse != null) {
                                alertBuilderView = new AlertBuilderView(RecoverPassword.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription());
                                alertBuilderView.show();
                            } else {
                                alertBuilderView = new AlertBuilderView(RecoverPassword.this, Operations.AlertBuilder_error, getString(R.string.generic_error));
                                alertBuilderView.show();
                                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        Utils.invokeActivity(RecoverPassword.this, LoginActivity.class, true);
                                    }
                                });
                            }
                        }
                    });
                }
                break;
        }
    }

    private boolean validate() {
        if (Utils.isEmpty(et_email)) {
            til_email.setError(getString(R.string.email_required));
            return false;
        } else if (!Utils.isEmailValid(et_email.getText().toString())) {
            til_email.setError(getString(R.string.email_invalid));
            return false;
        }
        return true;
    }
}
