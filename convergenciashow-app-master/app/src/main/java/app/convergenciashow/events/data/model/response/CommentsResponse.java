package app.convergenciashow.events.data.model.response;

public class CommentsResponse {
    private Long id;

    private String description;

    private String date;

    private Long idOwner;

    private int typeOfOwner;

    private String ownerName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(Long idOwner) {
        this.idOwner = idOwner;
    }

    public int getTypeOfOwner() {
        return typeOfOwner;
    }

    public void setTypeOfOwner(int typeOfOwner) {
        this.typeOfOwner = typeOfOwner;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}

