package app.convergenciashow.events.data.repository;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.Gson;

import app.convergenciashow.events.data.database.SQLiteHelper;
import app.convergenciashow.events.data.model.request.AuthRequest;
import app.convergenciashow.events.data.model.request.AuthRequestLinkedIn;
import app.convergenciashow.events.data.model.request.CanBeReachedByMessageRequest;
import app.convergenciashow.events.data.model.request.GotoEventRequest;
import app.convergenciashow.events.data.model.request.NewRegisterRequest;
import app.convergenciashow.events.data.model.request.RecoverPassRequest;
import app.convergenciashow.events.data.model.request.UpdateSessionDeviceTokenRequest;
import app.convergenciashow.events.data.model.response.AuthEventResponse;
import app.convergenciashow.events.data.model.response.AuthResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.GetDetailsOfSpeakerResponse;
import app.convergenciashow.events.data.model.response.GetDetailsOfUserResponse;
import app.convergenciashow.events.domain.AuthEvent;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.GetDetailsOfSpeaker;
import app.convergenciashow.events.domain.GetDetailsOfUser;
import app.convergenciashow.events.domain.PutUser;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AuthCallBack;
import app.convergenciashow.events.util.WSCallbacks.DetailsOfSpeakerCallBack;
import app.convergenciashow.events.util.WSCallbacks.DetailsOfUserCallBack;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getAuthService;

public class AuthRepository {

    private SQLiteHelper helper;
    private Context ctx;
    private Activity act;

    public AuthRepository(Context context, Activity activity) {
        this.ctx = context;
        this.act = activity;
        helper = new SQLiteHelper(context);
    }

    // Login with email and password
    public void authenticate(AuthRequest request, final AuthCallBack cb) {
        Call<AuthResponse> call = getAuthService().getLogin(request);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        helper.deleteAuth();
                        helper.insertAuth(transform(response.body()));
                        cb.onSuccess(transform(response.body()));
                    } else {
                        BaseResponse baseResponse = Utils.parse409(response.errorBody(), ctx);
                        cb.onError(baseResponse);
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    // Login using linkedin credentials
    public void authenticateLinkedIn(AuthRequestLinkedIn request, final AuthCallBack cb) {
        Call<AuthResponse> call = getAuthService().getLoginLinkedIn(request);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        helper.deleteAuth();
                        helper.insertAuth(transform(response.body()));
                        cb.onSuccess(transform(response.body()));
                    } else {
                        BaseResponse baseResponse = Utils.parse409(response.errorBody(), ctx);
                        cb.onError(baseResponse);
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    // update-session-device-token
    public void updateSessionDeviceToken(final AuthUser authUser, UpdateSessionDeviceTokenRequest request, final AuthCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<AuthResponse> call = getAuthService().updateSessionDeviceToken(userHeader, request);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        helper.deleteAuth();
                        helper.insertAuth(transform(response.body()));
                        cb.onSuccess(transform(response.body()));
                    } else {
                        BaseResponse baseResponse = Utils.parse409(response.errorBody(), ctx);
                        cb.onError(baseResponse);
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    // logOut from device
    public void logOut() {
        helper.deleteAuth();
    }

    // logout
    public void logOut(final AuthUser authUser, final GenericCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<BaseResponse> call = getAuthService().getLoguot(userHeader);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        cb.onSuccess(response.body());
                        helper.deleteAuth();
                    } else {
                        cb.onError(Utils.parse409(response.errorBody(), ctx));
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    // recover-password
    public void recoverPassword(final RecoverPassRequest request, final GenericCallBack cb) {
        Call<BaseResponse> call = getAuthService().getPasswordRecovery(request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        cb.onSuccess(response.body());
                    } else {
                        cb.onError(Utils.parse409(response.errorBody(), ctx));
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void newRegister(NewRegisterRequest request, final AuthCallBack cb) {
        Call<AuthResponse> call = getAuthService().getNewRegister(request);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        helper.deleteAuth();
                        helper.insertAuth(transform(response.body()));
                        cb.onSuccess(transform(response.body()));
                    } else {
                        BaseResponse baseResponse = Utils.parse409(response.errorBody(), ctx);
                        cb.onError(baseResponse);
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                cb.onError(null);
            }
        });

    }

    public void markInEvent(final AuthUser authUser, GotoEventRequest request,
                            final GenericCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<BaseResponse> call = getAuthService().markInEvent(userHeader, request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        cb.onSuccess(response.body());
                    } else if (response.code() == HTTP_UNAUTHORIZED) {
                        BaseResponse baseResponse = new BaseResponse();
                        baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                        cb.onError(baseResponse);
                    } else {
                        cb.onError(Utils.parse409(response.errorBody(), ctx));
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });

    }

    public void canBeReachedByMessage(final AuthUser authUser, CanBeReachedByMessageRequest request, final GenericCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<BaseResponse> call = getAuthService().canBeReachedByMessage(userHeader, request);
        call.enqueue(new Callback<BaseResponse>() {
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        cb.onSuccess(response.body());
                    } else if (response.code() == HTTP_UNAUTHORIZED) {
                        BaseResponse baseResponse = new BaseResponse();
                        baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                        cb.onError(baseResponse);
                    } else {
                        cb.onError(Utils.parse409(response.errorBody(), ctx));
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getDetailsOfSpeaker(final AuthUser authUser, Long idSpeaker, final DetailsOfSpeakerCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<GetDetailsOfSpeakerResponse> call = getAuthService().getDetailsOfSpeaker(userHeader, idSpeaker);
        call.enqueue(new Callback<GetDetailsOfSpeakerResponse>() {
            @Override
            public void onResponse(Call<GetDetailsOfSpeakerResponse> call, Response<GetDetailsOfSpeakerResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    cb.onError(Utils.parse409(response.errorBody(), ctx));
                }
            }

            @Override
            public void onFailure(Call<GetDetailsOfSpeakerResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getDetailsOfUser(Long idUser, Long idEvent, final DetailsOfUserCallBack cb) {
        Call<GetDetailsOfUserResponse> call = getAuthService().getDetailsOfUser(idEvent, idUser);
        call.enqueue(new Callback<GetDetailsOfUserResponse>() {
            @Override
            public void onResponse(Call<GetDetailsOfUserResponse> call, Response<GetDetailsOfUserResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        cb.onSuccess(transform(response.body()));
                    } else if (response.code() == HTTP_UNAUTHORIZED) {
                        BaseResponse baseResponse = new BaseResponse();
                        baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                        cb.onError(baseResponse);
                    } else {
                        cb.onError(Utils.parse409(response.errorBody(), ctx));
                    }
                } catch (Exception ex) {
                    cb.onError(null);
                }
            }

            @Override
            public void onFailure(Call<GetDetailsOfUserResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void update(final AuthUser authUser, final PutUser userUpdate, Bitmap image,
                       final GenericCallBack callBack) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();

        RequestBody post = null;
        MultipartBody.Part body = null;
        Gson gson = new Gson();
        post = RequestBody.create(MediaType.parse("application/json"), gson.toJson(userUpdate));

        if (image != null) {
            File file = convertImageToFile(image);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        }

        Call<BaseResponse> call = getAuthService().update(userHeader, body, post);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                try {
                    if (response.code() == HTTP_OK) {
                        callBack.onSuccess(response.body());
                        helper.deleteAuth();
                        AuthUser user = authUser;
                        user.setRole(userUpdate.getRole());
                        user.setUserLastName(userUpdate.getLastName());
                        user.setUserName(userUpdate.getName());
                        user.setUserDescription(userUpdate.getUserDescription());
                        user.setImageVersion(user.getImageVersion() + 1);
                        helper.insertAuth(user);
                    } else {
                        BaseResponse baseResponse = Utils.parse409(response.errorBody(), ctx);
                        callBack.onError(baseResponse);
                    }
                } catch (Exception ex) {
                    callBack.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                callBack.onError(null);
            }
        });

    }

    private File convertImageToFile(Bitmap bitmap) {
        File f = new File(ctx.getCacheDir(), "imageProfile");
        try {
            f.createNewFile();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();


            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean checkAuth() {
        return helper.getAuth() != null;
    }

    public AuthUser getAuth() {
        return helper.getAuth();
    }

    private GetDetailsOfSpeaker transform(GetDetailsOfSpeakerResponse response) {
        GetDetailsOfSpeaker detail = new GetDetailsOfSpeaker();
        if (response != null) {
            detail.setId(response.getId());
            detail.setName(response.getName());
            detail.setLastName(response.getLastName());
            detail.setImageVersion(response.getImageVersion());
            detail.setRole(response.getRole());
            detail.setCompanyName(response.getCompanyName());
            detail.setRegistrationDate(response.getRegistrationDate());
            detail.setDescription(response.getDescription());
            detail.setExpos(response.getExpos());
        }
        return detail;
    }

    private GetDetailsOfUser transform(GetDetailsOfUserResponse response) {
        GetDetailsOfUser detail = new GetDetailsOfUser();
        if (response != null) {
            detail.setId(response.getId());
            detail.setName(response.getName());
            detail.setLastName(response.getLastName());
            detail.setCompanyName(response.getCompanyName());
            detail.setCanBeReached(response.isCanBeReached());
            detail.setRole(response.getRole());
            detail.setHasPicture(response.isHasPicture());
            detail.setImageVersion(response.getImageVersion());
            detail.setUserDescription(response.getUserDescription());
            detail.setEmail(response.getEmail());
        }
        return detail;
    }

    private AuthUser transform(AuthResponse response) {
        AuthUser authUser = new AuthUser();
        if (response != null) {
            authUser.setCodeNumber(response.getCodeNumber());
            authUser.setCodeDescription(response.getCodeDescription());
            authUser.setIdUser(response.getIdUser());
            authUser.setIdSession(response.getIdSession());
            authUser.setUserName(response.getUserName());
            authUser.setUserLastName(response.getUserLastName());
            authUser.setImageVersion(response.getImageVersion());
            authUser.setUserDescription(response.getUserDescription());
            authUser.setEmail(response.getEmail());
            authUser.setRole(response.getRole());
            authUser.setDefaultEvent(response.getDefaultEvent());
            authUser.setCompanyName(response.getCompanyName());
            authUser.setAlreadyHasPicture(response.isAlreadyHasPicture());
            authUser.setVerified(response.isVerified());
            authUser.setEvents(transform(response.getEvents()));
            authUser.setCanUserBeReached(response.isCanUserBeReached());
        }
        return authUser;
    }

    private ArrayList<AuthEvent> transform(ArrayList<AuthEventResponse> events) {
        ArrayList<AuthEvent> authEvents = new ArrayList<>();
        if (events != null) {
            for (AuthEventResponse r : events) {
                AuthEvent authEvent = new AuthEvent();
                authEvent.setId(r.getId());
                authEvent.setRoleInEvent(r.getRoleInEvent());
                authEvents.add(authEvent);
            }
        }
        return authEvents;
    }
}
