package app.convergenciashow.events.util.WSCallbacks;

import android.widget.ImageView;

public interface GalleryAdapterCallback {

    void updateNumber(String text, ImageView imageView);
}
