package app.convergenciashow.events.data.model.request;

public class GotoEventRequest {

    private Long idEvent;
    private boolean mark;

    public GotoEventRequest(Long idEvent, boolean mark) {
        this.idEvent = idEvent;
        this.mark = mark;
    }
}
