package app.convergenciashow.events.data.repository;

import android.content.Context;
import android.support.v4.app.Fragment;

import app.convergenciashow.events.data.model.request.MarkInEventRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ScheduleResponse;
import app.convergenciashow.events.data.model.response.ExposResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Expo;
import app.convergenciashow.events.fragment.EventScheduleFragment;
import app.convergenciashow.events.fragment.PersonalScheduleFragment;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.ScheduleCallBack;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getScheduleService;

public class ScheduleRepository {

    private Context context;

    public ScheduleRepository(Context context){
        this.context = context;
    }

    public void markForPersonalSchedule(final AuthUser authUser, MarkInEventRequest request, final GenericCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<BaseResponse> call = getScheduleService().markForPersonalSchedule(userHeader, request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getItemsAuth(final Fragment f, final AuthUser authUser, Long idEvent, final ScheduleCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<ScheduleResponse> callResponse = getScheduleService().listAuth(userHeader, idEvent);
        callResponse.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, Response<ScheduleResponse> response) {
                if (response.code() == HTTP_OK) {
                    if ((f instanceof EventScheduleFragment)) {
                        cb.onSuccess(transform(response.body()));
                    } else if ((f instanceof PersonalScheduleFragment)) {
                        ScheduleResponse scheduleResponse = new ScheduleResponse();
                        ArrayList<ExposResponse> expos = new ArrayList<>();
                        for (ExposResponse expo : response.body().getExpos()) {
                            if (expo.getMarkedForPersonalAgenda()) {
                                expos.add(expo);
                            }
                        }
                        scheduleResponse.setExpos(expos);
                        cb.onSuccess(transform(scheduleResponse));
                    }
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }

            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getItemsNoAuth(Long idEvent, final ScheduleCallBack cb) {
        Call<ScheduleResponse> callResponse = getScheduleService().listNoAuth(idEvent);
        callResponse.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, Response<ScheduleResponse> response) {
                cb.onSuccess(transform(response.body()));
            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }


    private ArrayList<Object> transform(ScheduleResponse response) {
        ArrayList<ExposResponse> expoResponses = response.getExpos();
        ArrayList<Expo> expos = new ArrayList<>();
        Set<String> set = new HashSet<>();
        for (ExposResponse expo : expoResponses) {
            Expo e = new Expo();
            e.setDate(expo.getDate());
            e.setId(expo.getId());
            e.setSpeakers(Utils.arrayToString(expo.getSpeakers()));
            e.setTitle(expo.getTitle());
            e.setIdRoom(expo.getIdRoom());
            e.setRoomTitle(expo.getRoomTitle());
            e.setMarkedForPersonalAgenda(expo.getMarkedForPersonalAgenda() == null ? false : expo.getMarkedForPersonalAgenda());
            expos.add(e);
            set.add(expo.getDate().substring(0, 11));
        }

        ArrayList<Object> list = new ArrayList<>();
        List<String> dateList = new ArrayList<>();
        dateList.addAll(set);
        Collections.sort(dateList);

        for (String s : dateList) {
            list.add(s);
            list.addAll(sortHour(s, expos));
        }

        return list;
    }

    private ArrayList<Object> sortHour(String date, ArrayList<Expo> expos) {
        ArrayList<Object> list = new ArrayList<>();
        ArrayList<Object> listSorted = new ArrayList<>();
        ArrayList<String> hours = new ArrayList<>();

        for (Expo e : expos) {
            hours.add(e.getDate().substring(11, 16));
            if (e.getDate().substring(0, 11).equals(date)) {
                list.add(e);
            }
        }

        Collections.sort(hours);


        for (String h : hours) {
            Iterator<Object> itr = list.iterator();
            while (itr.hasNext()) {
                Expo expo = (Expo) itr.next();
                if (expo.getDate().substring(11, 16).equals(h)) {
                    listSorted.add(expo);
                    itr.remove();
                }
            }
        }
        return listSorted;
    }



}
