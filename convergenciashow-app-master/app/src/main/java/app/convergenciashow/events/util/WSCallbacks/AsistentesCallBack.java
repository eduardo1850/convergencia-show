package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.User;

import java.util.ArrayList;

public interface AsistentesCallBack {

        void onSuccess(@NonNull ArrayList<User> value);

        void onError(@NonNull BaseResponse response);
}
