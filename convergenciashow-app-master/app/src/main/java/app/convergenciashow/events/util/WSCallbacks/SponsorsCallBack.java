package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;

import java.util.ArrayList;

public interface SponsorsCallBack {

    void onSuccess(@NonNull ArrayList<Object> sponsors);

    void onError(@NonNull BaseResponse baseResponse);
}
