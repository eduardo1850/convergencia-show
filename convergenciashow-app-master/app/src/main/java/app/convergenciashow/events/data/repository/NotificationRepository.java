package app.convergenciashow.events.data.repository;

import app.convergenciashow.events.domain.Notification;
import app.convergenciashow.events.data.model.response.GetNotificationsResponse;
import app.convergenciashow.events.data.model.response.NotificationResponse;
import app.convergenciashow.events.util.WSCallbacks.NotificationsCallback;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.convergenciashow.events.data.service.ServiceManger.getNotificationsService;

public class NotificationRepository {

    public void getNotifications(Long loadedElements, Integer numberOfRows, final NotificationsCallback cb) {
        Call<GetNotificationsResponse> callResponse = getNotificationsService().listNotifications(loadedElements, numberOfRows);
        callResponse.enqueue(new Callback<GetNotificationsResponse>() {
            @Override
            public void onResponse(Call<GetNotificationsResponse> call, Response<GetNotificationsResponse> response) {
                cb.onSuccess(transform(response.body()));
            }

            @Override
            public void onFailure(Call<GetNotificationsResponse> call, Throwable t) {
                cb.onError(t);
            }
        });
    }

    private ArrayList<Notification> transform (GetNotificationsResponse response) {
        ArrayList<NotificationResponse> notificationResponses = response.getNotifications();
        ArrayList<Notification> notifications = new ArrayList<>();
        for(NotificationResponse n : notificationResponses) {
            Notification notification = new Notification();
            notification.setAdminLastName(n.getAdminLastName());
            notification.setAdminName(n.getAdminName());
            notification.setDate(n.getDate());
            notification.setId(n.getId());
            notification.setIdAdmin(n.getIdAdmin());
            notification.setImageVersion(n.getImageVersion());
            notification.setMessage(n.getMessage());
            notifications.add(notification);
        }
        return notifications;
    }

}
