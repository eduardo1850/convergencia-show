package app.convergenciashow.events.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ChatBaseActivity;
import app.convergenciashow.events.adapter.ConversationAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.SendAMessageRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.MessagesResponse;
import app.convergenciashow.events.data.model.response.SendAMessageResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ConversationRepository;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.MessagesOfConversationCallback;
import app.convergenciashow.events.util.WSCallbacks.SendAMessageCallback;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.util.Constants.IDUSERTOCONVERSE;
import static app.convergenciashow.events.util.Constants.IMAGEUSERTOCONVERSE;
import static app.convergenciashow.events.util.Constants.NAMEUSERTOCONVERSE;

public class ConversationActivity extends ChatBaseActivity implements View.OnClickListener {

    @BindView(R.id.chat_rv_msjs)
    RecyclerView rv_msjs;
    @BindView(R.id.chat_et_comment)
    EditText et_comment;
    @BindView(R.id.chat_bt_send)
    TextView chat_bt_send;

    public ConversationAdapter adapter;
    public ArrayList<MessagesResponse> messagesResponses;
    public static boolean chatActive = false;
    public static Activity activity;
    private static final int rows = 10;
    private Long loadedElements = 0L;
    private ConversationRepository repository;
    private String userName, userImage;
    private Long userId;
    private Unbinder unbinder;
    private boolean loading = false;
    private int rvLastVisibleItem;
    private AuthRepository authRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_conversation);
        unbinder = ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        chat_bt_send.setOnClickListener(this);
    }

    @Override
    protected void setComponent() {
        if (getIntent().getExtras() != null) {
            userId = getIntent().getExtras().getLong(IDUSERTOCONVERSE);
            userName = getIntent().getExtras().getString(NAMEUSERTOCONVERSE);
            userImage = getIntent().getExtras().getString(IMAGEUSERTOCONVERSE);
            authRepository = new AuthRepository(getApplicationContext(), this);
            repository = new ConversationRepository(getApplicationContext());
            //unbinder = ButterKnife.bind(this);
            messagesResponses = new ArrayList<MessagesResponse>();
            adapter = new ConversationAdapter(messagesResponses, this);
            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
            manager.setReverseLayout(true);
            manager.setStackFromEnd(true);
            rv_msjs.setLayoutManager(manager);
            rv_msjs.swapAdapter(adapter, true);
            rv_msjs.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rv_msjs.getLayoutManager();
                    rvLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (rv_msjs.getChildCount() > 0) {
                        if (!loading && rvLastVisibleItem == rv_msjs.getChildCount() - 1) {
                            loading = true;
                            loadMessages();
                        }
                    }
                }
            });
            loadMessages();
        } else {
            finish();
        }
    }

    private void loadMessages() {
        //progressBuilder.show();
        repository.getMessagesOfConversation(authRepository.getAuth(), userId, loadedElements, rows, new MessagesOfConversationCallback() {
            @Override
            public void onSuccess(@NonNull ArrayList<MessagesResponse> response) {
                //progressBuilder.dismiss();
                if (response.size() > 0) {
                    messagesResponses.addAll(response);
                    adapter.update();
                    loadedElements = loadedElements + rows;
                    loading = false;
                    //rv_msjs.scrollToPosition(0);
                }
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                //progressBuilder.dismiss();
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        AlertBuilderView alertBuilderView = new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, getString(R.string.session_error));
                        alertBuilderView.show();
                        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                authRepository.logOut();
                                Utils.invokeActivity(ConversationActivity.this, LoginActivity.class, true);
                            }
                        });
                    } else {
                        new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chat_ab_ivback:
                Utils.invokeActivity(this, MenuContainerActivity.class, true);
                break;
            case R.id.chat_bt_send:
                if (!et_comment.getText().toString().isEmpty()) {
                    chat_bt_send.setClickable(false);
                    MessagesResponse messagesResponse = new MessagesResponse(
                            0L,
                            authRepository.getAuth().getIdUser(),
                            et_comment.getText().toString(),
                            Utils.formatDate(Constants.MESSAGES_DATE_FORMAT, new Date()));
                    updateMessages(messagesResponse);
                    SendAMessageRequest request = new SendAMessageRequest(userId, et_comment.getText().toString());
                    et_comment.setText("");
                    repository.sendMessageAConversation(authRepository.getAuth(), request, new SendAMessageCallback() {
                        @Override
                        public void onSuccess(@NonNull SendAMessageResponse response) {
                            chat_bt_send.setClickable(true);
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            if (baseResponse != null) {
                                if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                    AlertBuilderView alertBuilderView = new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, getString(R.string.session_error));
                                    alertBuilderView.show();
                                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            authRepository.logOut();
                                            Utils.invokeActivity(ConversationActivity.this, LoginActivity.class, true);
                                        }
                                    });
                                } else {
                                    new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                                }
                            } else {
                                new AlertBuilderView(ConversationActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                            }
                        }
                    });
                    break;
                }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        activity = ConversationActivity.this;
        chatActive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        activity = null;
        chatActive = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //Utils.invokeActivity(this, MenuContainerActivity.class, true);
    }

    @Override
    public void invokeActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_chat_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.MATCH_PARENT,
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        Gravity.LEFT)
        );

        AppCompatImageView ab_ivback = findViewById(R.id.chat_ab_ivback);
        TextView ab_tvuser = findViewById(R.id.chat_ab_tvuser);
        CircleImageView ab_civimage = findViewById(R.id.chat_ab_civimage);
        ab_ivback.setOnClickListener(this);
        Glide.with(this)
                .applyDefaultRequestOptions(
                        new RequestOptions()
                                .placeholder(R.drawable.default_user)
                                .error(R.drawable.default_user))
                .load(userImage)
                .into(ab_civimage);
        ab_tvuser.setText(userName);
    }

    public void updateMessages(MessagesResponse messagesResponse) {
        if (messagesResponses != null)
            messagesResponses.add(0, messagesResponse);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rv_msjs.removeAllViews();
                adapter.update();
                rv_msjs.scrollToPosition(0);
            }
        });
    }

}
