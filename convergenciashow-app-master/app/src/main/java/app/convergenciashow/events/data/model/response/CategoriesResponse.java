package app.convergenciashow.events.data.model.response;

import android.support.annotation.NonNull;

public class CategoriesResponse implements Comparable<CategoriesResponse> {

    private Long id;
    private String name;
    private Double level;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLevel() {
        return level;
    }

    public void setLevel(Double level) {
        this.level = level;
    }

    @Override
    public int compareTo(@NonNull CategoriesResponse categoriesResponse) {
        return level > categoriesResponse.getLevel() ? -1 : level < categoriesResponse.level ? 1 : 0;
    }
}
