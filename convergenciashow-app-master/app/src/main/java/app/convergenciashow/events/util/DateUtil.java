package app.convergenciashow.events.util;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Date;
import java.util.Locale;

public class DateUtil {

    public static String getRelative(String dateString) {
        PrettyTime p = new PrettyTime(new Locale("es"));
        return p.format(new Date(dateString));
    }
}
