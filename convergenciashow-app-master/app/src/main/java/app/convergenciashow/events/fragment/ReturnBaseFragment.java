package app.convergenciashow.events.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import app.convergenciashow.events.R;

public class ReturnBaseFragment extends Fragment{

    private String returnString = "";
    private static final String FRAGMENT_TO_SHOW = "fragmmentToShow";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setComponent();
        invokeActionBar(getReturnString());
    }

    protected void invokeActivity(Activity activity, Class clazz, boolean shouldFinish) {
        Intent intent = new Intent(activity, clazz);
        startActivity(intent);
        if(shouldFinish){
            activity.finish();
        }
    }

    private void invokeActionBar(String returnString) {
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        AppCompatImageView returnTxt = activity.findViewById(R.id.action_bar_return_string);
        //returnTxt.setText(returnString);
        returnTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(MenuEventFragment.newInstance());
            }
        });
    }

    protected void setComponent(){
        //Implement in activity
    }

    protected void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

    public void setFragmentToShow(int fragmentToShow) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(FRAGMENT_TO_SHOW, fragmentToShow);
        editor.commit();
    }

    public int getFragmentToShow(){
        return getPreferences().getInt(FRAGMENT_TO_SHOW, 1);
    }

    private SharedPreferences getPreferences() {
        return getActivity().getPreferences(Context.MODE_PRIVATE);
    }

    public String getReturnString() {
        return returnString;
    }

    public void setReturnString(String returnString) {
        this.returnString = returnString;
    }
}
