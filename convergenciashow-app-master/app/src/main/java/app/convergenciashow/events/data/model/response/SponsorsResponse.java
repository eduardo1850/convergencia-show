package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class SponsorsResponse extends BaseResponse {

    private ArrayList<CategoriesResponse> categories;
    private ArrayList<SponsorResponse> sponsors;

    public ArrayList<CategoriesResponse> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoriesResponse> categories) {
        this.categories = categories;
    }

    public ArrayList<SponsorResponse> getSponsors() {
        return sponsors;
    }

    public void setSponsors(ArrayList<SponsorResponse> sponsors) {
        this.sponsors = sponsors;
    }
}
