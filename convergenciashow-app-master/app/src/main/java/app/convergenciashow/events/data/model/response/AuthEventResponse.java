package app.convergenciashow.events.data.model.response;

public class AuthEventResponse {

    private Long id;
    private boolean isSpeaker;
    private Integer roleInEvent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRoleInEvent() {
        return roleInEvent;
    }

    public void setRoleInEvent(Integer roleInEvent) {
        this.roleInEvent = roleInEvent;
    }

    public boolean isSpeaker() {
        return isSpeaker;
    }

    public void setSpeaker(boolean speaker) {
        isSpeaker = speaker;
    }
}
