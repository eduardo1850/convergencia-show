package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.AsistentesAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AsistentesRepository;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.User;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AsistentesCallBack;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class AsistentesFragment extends ReturnBaseFragment {

    private RecyclerView asistentesRecyclerView;
    private LinearLayout llassistants;
    private AuthRepository authRepository;
    private AsistentesRepository repository;
    private AsistentesAdapter adapter;
    private EditText asistentesSearch;

    public static AsistentesFragment newInstance() {
        AsistentesFragment fragment = new AsistentesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setReturnString("< " + getString(R.string.backto_event));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        authRepository = new AuthRepository(getContext(), getActivity());
        repository = new AsistentesRepository(getContext());
        loadElements();
        return inflater.inflate(R.layout.fragment_asistentes, container, false);
    }

    @Override
    protected void setComponent() {
        adapter = new AsistentesAdapter(new ArrayList<User>(), this, authRepository.getAuth().getIdUser());
        llassistants = getActivity().findViewById(R.id.llassistants);
        asistentesSearch = getActivity().findViewById(R.id.asistentesSearch);
        asistentesSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        asistentesRecyclerView = getActivity().findViewById(R.id.asistentesRv);
        asistentesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        asistentesRecyclerView.setAdapter(adapter);
    }

    public void loadElements() {
        AuthUser authUser = authRepository.getAuth();
        String idEvent = AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT) + "";
        repository.getAsistentes(authUser, idEvent, new AsistentesCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<User> value) {
                if (value.size() > 0) {
                    llassistants.setVisibility(View.GONE);
                    asistentesRecyclerView.setVisibility(View.VISIBLE);
                    adapter.update(value);
                } else {
                    llassistants.setVisibility(View.VISIBLE);
                    asistentesRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        if (isAdded()) {
                            AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    authRepository.logOut();
                                    Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                                }
                            });
                        }
                    } else {
                        if (isAdded())
                            new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    if (isAdded())
                        new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });
    }

}
