package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.BaseActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.PostRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.PutPostResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ForoRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.PutPostCallBack;

import static app.convergenciashow.events.util.Constants.STACK_TRACE;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class UploadPostActivity extends BaseActivity implements IPickResult {

    private Button cancelButton;
    private Button addButton;
    private ImageView addImageView;
    private boolean isImageTaken = false;
    private TextView descriptionTxt;
    private ForoRepository repository;
    private AuthRepository authRepository;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_upload_post);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setComponent() {
        repository = new ForoRepository(this);
        authRepository = new AuthRepository(getApplicationContext(), this);
        cancelButton = findViewById(R.id.cancelBtn);
        addButton = findViewById(R.id.savePostBtn);
        addImageView = findViewById(R.id.addImageView);
        descriptionTxt = findViewById(R.id.descriptionTxt);

        final PickSetup setup = new PickSetup()
                .setTitle(getString(R.string.choose_text))
                .setCancelText(getString(R.string.cancel_text))
                .setFlip(true)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera_text))
                .setGalleryButtonText(getString(R.string.gallery_text))
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.VERTICAL)
                .setSystemDialog(false);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButton.setEnabled(false);
                AuthUser user = authRepository.getAuth();
                PostRequest request = new PostRequest(AppPreferences.getInstance(UploadPostActivity.this).getLongData(Constants.ID_EVENT), descriptionTxt.getText().toString());
                if (isImageTaken || !descriptionTxt.getText().toString().equals("")) {
                    repository.putPost(user, request, bitmap, new PutPostCallBack() {
                        @Override
                        public void onSuccess(@NonNull PutPostResponse value) {
                            AppPreferences.getInstance(UploadPostActivity.this).saveIntData(STACK_TRACE, Constants.FRAGMENT_FORUM);
                            finish();
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            if (baseResponse != null) {
                                if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                    AlertBuilderView alertBuilderView = new AlertBuilderView(UploadPostActivity.this, Operations.AlertBuilder_error, UploadPostActivity.this.getString(R.string.session_error));
                                    alertBuilderView.show();
                                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            authRepository.logOut();
                                            Utils.invokeActivity(UploadPostActivity.this, LoginActivity.class, true);
                                        }
                                    });
                                } else {
                                    new AlertBuilderView(UploadPostActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                                }
                            } else {
                                new AlertBuilderView(UploadPostActivity.this, Operations.AlertBuilder_error, UploadPostActivity.this.getString(R.string.generic_error)).show();
                            }
                        }
                    });
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickImageDialog.build(setup).show(UploadPostActivity.this);
            }
        });

    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            bitmap = pickResult.getBitmap();
            addImageView.setImageBitmap(bitmap);
            isImageTaken = true;
        }
    }

}
