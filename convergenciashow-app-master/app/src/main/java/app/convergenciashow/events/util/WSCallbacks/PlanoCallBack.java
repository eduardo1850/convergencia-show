package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.Plano;

public interface PlanoCallBack {

    void onSuccess(@NonNull Plano value);

    void onError(@NonNull BaseResponse baseResponse);
}
