package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.DocumentsAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.domain.Document;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.DocumentsCallBack;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ContentFragment extends Fragment {

    @BindView(R.id.documentsRv)
    RecyclerView documentsRv;
    @BindView(R.id.lldocuments)
    LinearLayout lldocuments;

    private EventRepository eventRepository;
    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;

    public ContentFragment() {
    }

    public static ContentFragment newInstance() {
        ContentFragment fragment = new ContentFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invokeActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_documents, container, false);
        ButterKnife.bind(this, view);
        eventRepository = new EventRepository(getContext());
        authRepository = new AuthRepository(getContext(), getActivity());
        progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
        progressBuilder.show();

        eventRepository.getDocuments(authRepository.getAuth(), AppPreferences.getInstance(getContext()).getLongData(Constants.ID_EVENT), new DocumentsCallBack() {
            @Override
            public void onSuccess(@NonNull List<Document> documents) {
                progressBuilder.dismiss();
                if (documents.size() > 0) {
                    lldocuments.setVisibility(View.GONE);
                    documentsRv.setVisibility(View.VISIBLE);
                    DocumentsAdapter adapter = new DocumentsAdapter(documents, ContentFragment.this);
                    documentsRv.setLayoutManager(new LinearLayoutManager(getContext()));
                    documentsRv.setAdapter(adapter);
                } else {
                    lldocuments.setVisibility(View.VISIBLE);
                    documentsRv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                progressBuilder.dismiss();
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        if (isAdded()) {
                            AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    authRepository.logOut();
                                    Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                                }
                            });
                        }
                    } else {
                        if (isAdded())
                            new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    if (isAdded())
                        new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });
        return view;
    }

    private void invokeActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar_fragment, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        TextView title = activity.findViewById(R.id.action_bar_return_string);
        title.setText(getActivity().getText(R.string.back));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(MenuEventFragment.newInstance());
            }
        });
    }

    protected void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

}
