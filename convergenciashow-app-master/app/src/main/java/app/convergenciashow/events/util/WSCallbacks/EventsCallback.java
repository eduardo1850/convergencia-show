package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.Event;

public interface EventsCallback {

    void onSuccess(@NonNull ArrayList<Event> response);

    void onError(@NonNull BaseResponse baseResponse);
}
