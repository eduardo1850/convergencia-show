package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class AsistentesResponse extends BaseResponse {
    private ArrayList<UserResponse> users;

    public ArrayList<UserResponse> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserResponse> users) {
        this.users = users;
    }
}
