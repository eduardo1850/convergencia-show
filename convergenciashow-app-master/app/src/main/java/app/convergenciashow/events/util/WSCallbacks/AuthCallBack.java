package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.AuthUser;

public interface AuthCallBack {
    void onSuccess(@NonNull AuthUser value);

    void onError(@NonNull BaseResponse baseResponse);
}
