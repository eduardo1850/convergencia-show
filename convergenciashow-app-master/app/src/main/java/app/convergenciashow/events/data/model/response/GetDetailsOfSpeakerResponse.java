package app.convergenciashow.events.data.model.response;

import app.convergenciashow.events.domain.ExposSpeaker;

import java.util.ArrayList;

public class GetDetailsOfSpeakerResponse extends BaseResponse {

    private Long id;
    private String name;
    private String lastName;
    private Integer imageVersion;
    private String role;
    private String companyName;
    private String registrationDate;
    private String description;
    private ArrayList<ExposSpeaker> expos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(Integer imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<ExposSpeaker> getExpos() {
        return expos;
    }

    public void setExpos(ArrayList<ExposSpeaker> expos) {
        this.expos = expos;
    }
}

