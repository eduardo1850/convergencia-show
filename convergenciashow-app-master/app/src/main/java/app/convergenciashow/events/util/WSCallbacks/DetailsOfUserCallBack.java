package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.GetDetailsOfUser;

public interface DetailsOfUserCallBack {

    void onSuccess(@NonNull GetDetailsOfUser response);

    void onError(@NonNull BaseResponse baseResponse);

}
