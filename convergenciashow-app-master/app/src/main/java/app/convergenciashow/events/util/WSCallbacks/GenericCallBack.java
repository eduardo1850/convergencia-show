package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;

public interface GenericCallBack {

    void onSuccess(@NonNull BaseResponse value);

    void onError(@NonNull BaseResponse baseResponse);
}
