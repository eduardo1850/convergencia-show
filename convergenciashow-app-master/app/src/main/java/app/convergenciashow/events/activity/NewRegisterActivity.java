package app.convergenciashow.events.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.NewRegisterRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AuthCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NewRegisterActivity extends AppCompatActivity implements View.OnClickListener, IPickResult {

    @BindView(R.id.singin_civ_profile_image)
    CircleImageView civ_profile_image;
    @BindView(R.id.singin_til_name)
    TextInputLayout til_name;
    @BindView(R.id.singin_et_name)
    EditText et_name;
    @BindView(R.id.singin_til_lastname)
    TextInputLayout til_lastname;
    @BindView(R.id.singin_et_lastname)
    EditText et_lastname;
    @BindView(R.id.singin_til_email)
    TextInputLayout til_email;
    @BindView(R.id.singin_et_email)
    EditText et_email;
    @BindView(R.id.singin_til_password)
    TextInputLayout til_password;
    @BindView(R.id.singin_et_password)
    EditText et_password;
    @BindView(R.id.singin_til_cpassword)
    TextInputLayout til_cpassword;
    @BindView(R.id.singin_et_cpassword)
    EditText et_cpassword;
    @BindView(R.id.singin_bt_getin)
    Button bt_getin;
    @BindView(R.id.switchButton)
    Switch switchButton;
    @BindView(R.id.switchButtontext)
    TextView switchButtontext;
    @BindView(R.id.switchButtonerror)
    TextView switchButtonerror;

    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;
    private Bitmap profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);
        ButterKnife.bind(this);
        setTitle("");
        authRepository = new AuthRepository(getApplicationContext(), this);
        progressBuilder = new AlertBuilderView(NewRegisterActivity.this, Operations.AlertBuilder_progress, "");
        civ_profile_image.setOnClickListener(this);
        bt_getin.setOnClickListener(this);
        switchButtontext.setOnClickListener(this);
        civ_profile_image.setBackgroundResource(R.drawable.default_user);
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            profileImage = pickResult.getBitmap();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            profileImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(this)
                    .asBitmap()
                    .load(stream.toByteArray())
                    .into(civ_profile_image);

            civ_profile_image.setImageBitmap(pickResult.getBitmap());
        } else {
            new AlertBuilderView(this, Operations.AlertBuilder_error, getString(R.string.picture_texterror)).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.switchButtontext:
                Intent intent = new Intent(NewRegisterActivity.this, DocumentDescriptionActivity.class);
                intent.putExtra(Constants.DOCUMENTURL, Constants.TERMS_AND_CONDITIONS);
                startActivity(intent);
                break;
            case R.id.singin_civ_profile_image:
                final PickSetup setup = new PickSetup()
                        .setTitle(getString(R.string.choose_text))
                        .setCancelText(getString(R.string.cancel_text))
                        .setFlip(true)
                        .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                        .setCameraButtonText(getString(R.string.camera_text))
                        .setGalleryButtonText(getString(R.string.gallery_text))
                        .setIconGravity(Gravity.LEFT)
                        .setButtonOrientation(LinearLayoutCompat.VERTICAL)
                        .setSystemDialog(false);

                PickImageDialog.build(setup).show(NewRegisterActivity.this);
                break;
            case R.id.singin_bt_getin:
                if (validate()) {
                    progressBuilder.show();
                    String profileImageBase64 = "";
                    if (profileImage != null)
                        profileImageBase64 = Utils.encodeImage(profileImage);
                    NewRegisterRequest request = new NewRegisterRequest(
                            et_name.getText().toString(),
                            et_lastname.getText().toString(),
                            et_email.getText().toString(),
                            et_password.getText().toString(),
                            profileImageBase64,
                            Constants.ANDROID_APP,
                            FirebaseInstanceId.getInstance().getToken()
                    );

                    authRepository.newRegister(request, new AuthCallBack() {
                        @Override
                        public void onSuccess(@NonNull AuthUser value) {
                            progressBuilder.dismiss();
                            AppPreferences.getInstance(NewRegisterActivity.this).saveLongData(Constants.ID_EVENT, value.getDefaultEvent());
                            if (value.isVerified())
                                Utils.validateEvent(NewRegisterActivity.this);
                            else
                                Utils.invokeActivity(NewRegisterActivity.this, UnverifiedAccount.class, true);
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            progressBuilder.dismiss();
                            if (baseResponse != null)
                                new AlertBuilderView(NewRegisterActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            else
                                new AlertBuilderView(NewRegisterActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                        }
                    });
                }
                break;
        }
    }

    // Validate Email and Pssword
    private boolean validate() {
        til_name.setError(null);
        til_lastname.setError(null);
        til_email.setError(null);
        til_password.setError(null);
        til_cpassword.setError(null);
        switchButtonerror.setVisibility(View.GONE);

        if (Utils.isEmpty(et_name)) {
            til_name.setError(getString(R.string.mandatory_field));
            return false;
        }

        if (Utils.isEmpty(et_lastname)) {
            til_lastname.setError(getString(R.string.mandatory_field));
            return false;
        }

        if (Utils.isEmpty(et_email)) {
            til_email.setError(getString(R.string.email_required));
            return false;
        } else if (!Utils.isEmailValid(et_email.getText().toString())) {
            til_email.setError(getString(R.string.email_invalid));
            return false;
        }

        if (Utils.isEmpty(et_password)) {
            til_password.setError(getString(R.string.password_required));
            return false;
        }

        if (Utils.isEmpty(et_cpassword)) {
            til_cpassword.setError(getString(R.string.password_required));
            return false;
        }

        if (!et_password.getText().toString().equals(et_cpassword.getText().toString())) {
            til_password.setError(getString(R.string.password_must_mact));
            til_cpassword.setError(getString(R.string.password_must_mact));
            return false;
        }

        if (!switchButton.isChecked()) {
            switchButtonerror.setVisibility(View.VISIBLE);
            return false;
        }


        return true;
    }

}
