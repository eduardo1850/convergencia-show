package app.convergenciashow.events.domain;

import java.util.ArrayList;

public class Speaker {

    private Long id;
    private String name;
    private String lastName;
    private String companyName;
    private String role;
    private int imageVersion;
    private int speakerType;
    private ArrayList<String> expos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(int imageVersion) {
        this.imageVersion = imageVersion;
    }

    public int getSpeakerType() {
        return speakerType;
    }

    public void setSpeakerType(int speakerType) {
        this.speakerType = speakerType;
    }

    public ArrayList<String> getExpos() {
        return expos;
    }

    public void setExpos(ArrayList<String> expos) {
        this.expos = expos;
    }
}
