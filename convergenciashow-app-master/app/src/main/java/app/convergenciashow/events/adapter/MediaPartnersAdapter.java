package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.domain.Sponsor;
import app.convergenciashow.events.fragment.MediaPartnersFragment;
import app.convergenciashow.events.fragment.SponsorsFragment;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;

public class MediaPartnersAdapter extends RecyclerView.Adapter<MediaPartnersAdapter.ViewHolder> {

    private ArrayList<Object> items;
    private Context context;
    private Activity activity;
    private AlertBuilderView progressBuilder;

    public MediaPartnersAdapter(ArrayList<Object> items, MediaPartnersFragment fragment) {
        this.items = items;
        this.context = fragment.getContext();
        this.activity = fragment.getActivity();
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
    }

    @NonNull
    @Override
    public MediaPartnersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        ViewHolder vh;
        switch (viewType) {
            case 2:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sponsor_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final MediaPartnersAdapter.ViewHolder viewHolder, final int i) {
        final Object n = items.get(i);
        if (n instanceof String) {
            viewHolder.category.setText(((String) n));
        } else {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.glideplaceholder);
            requestOptions.error(R.drawable.placeholderpicture);
            Glide.with(activity)
                    .setDefaultRequestOptions(requestOptions)
                    .load(Utils.getSponsorsUrl(((Sponsor) n).getId()))
                    .into(viewHolder.sponsorsImage);

            viewHolder.sponsorName.setText(((Sponsor) n).getTitle());
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof String) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sponsorName;
        TextView category;
        ImageView sponsorsImage;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == 2) {
                sponsorName = itemView.findViewById(R.id.sponsors_tv);
                sponsorsImage = itemView.findViewById(R.id.sponsors_iv);
            } else if (viewType == 1) {
                category = itemView.findViewById(R.id.sponsors_category);
            }
        }
    }


}
