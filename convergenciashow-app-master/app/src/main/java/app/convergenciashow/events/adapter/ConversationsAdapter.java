package app.convergenciashow.events.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.MenuContainerActivity;
import app.convergenciashow.events.data.model.response.ConversationResponse;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.convergenciashow.events.util.Constants.IMAGE_PROFILE_PATH;
import static app.convergenciashow.events.util.Constants.JPG;
import static app.convergenciashow.events.util.Constants.VER;

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

    private ArrayList<ConversationResponse> conversationsList;
    private Context context;

    public ConversationsAdapter(ArrayList<ConversationResponse> conversations, Context context) {
        this.conversationsList = conversations;
        this.context = context;
    }

    @NonNull
    @Override
    public ConversationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_chat_item, viewGroup, false);
        ConversationsAdapter.ViewHolder viewHolder = new ConversationsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ConversationResponse conversation = conversationsList.get(i);
        viewHolder.chatitem_name.setText(conversation.getName());
        if (!Utils.timeIsRunningOut(conversation.getDate(), context).isEmpty())
            viewHolder.chatitem_date.setText(Utils.timeIsRunningOut(conversation.getDate(), context));
        else
            viewHolder.chatitem_date.setVisibility(View.GONE);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.default_user);
        requestOptions.error(R.drawable.default_user);

        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(getProfileImage(conversation.getIdUser(), conversation.getImageVersion())).into(viewHolder.chatitem_image);

        viewHolder.rl_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MenuContainerActivity) context).gotoConversation(conversation.getIdUser(), conversation.getName(), getProfileImage(conversation.getIdUser(), conversation.getImageVersion()));
            }
        });
    }

    private String getProfileImage(Long userId, int version) {
        return IMAGE_PROFILE_PATH + userId + JPG + VER + version;
    }

    @Override
    public int getItemCount() {
        if (conversationsList != null)
            return conversationsList.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.chatitem_rl_item)
        RelativeLayout rl_item;
        @BindView(R.id.chatitem_image)
        CircleImageView chatitem_image;
        @BindView(R.id.chatitem_name)
        TextView chatitem_name;
        @BindView(R.id.chatitem_date)
        TextView chatitem_date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void update(ArrayList<ConversationResponse> newItems) {
        conversationsList.addAll(newItems);
        notifyDataSetChanged();
    }

}
