package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class SpeakersResponse extends BaseResponse {
    private ArrayList<SpeakerResponse> speakers;

    public ArrayList<SpeakerResponse> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(ArrayList<SpeakerResponse> speakers) {
        this.speakers = speakers;
    }
}
