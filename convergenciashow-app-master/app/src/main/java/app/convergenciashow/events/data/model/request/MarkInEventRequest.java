package app.convergenciashow.events.data.model.request;

public class MarkInEventRequest {

    private Long idExpo;
    private boolean mark;

    public MarkInEventRequest(Long idExpo, boolean mark) {
        this.idExpo = idExpo;
        this.mark = mark;
    }
}
