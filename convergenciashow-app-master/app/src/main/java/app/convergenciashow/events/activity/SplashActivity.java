package app.convergenciashow.events.activity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.NoActionBarAcitivity;
import app.convergenciashow.events.data.database.SQLiteHelper;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.Utils;

public class SplashActivity extends NoActionBarAcitivity {

    private static final int TIME_DELAYED = 2000;
    private ImageView splashImage;
    private SQLiteHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // get DBHelper to know if user is logged
        helper = new SQLiteHelper(this);
        final AuthUser user = helper.getAuth();
        splashImage = findViewById(R.id.splashImage);
        Glide.with(this).load(R.drawable.splash).into(splashImage);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (user != null)
                    if (user.isVerified()) {
                        Utils.validateEvent(SplashActivity.this);
                    } else {
                        invokeActivity(SplashActivity.this, UnverifiedAccount.class, true);
                    }
                else
                    invokeActivity(SplashActivity.this, LoginActivity.class, true);

            }
        }, TIME_DELAYED);
    }
}
