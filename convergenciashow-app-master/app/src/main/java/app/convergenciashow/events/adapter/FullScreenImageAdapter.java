package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import app.convergenciashow.events.R;
import app.convergenciashow.events.domain.Pictures;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GalleryAdapterCallback;

public class FullScreenImageAdapter extends PagerAdapter {

    private List<Pictures> _pictures;
    private Activity _activity;
    private LayoutInflater inflater;
    private GalleryAdapterCallback _cb;

    public FullScreenImageAdapter(List<Pictures> _pictures, Activity activity, GalleryAdapterCallback cb) {
        this._pictures = _pictures;
        this._activity = activity;
        this._cb = cb;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Pictures picture = _pictures.get(position);
        ImageView imgDisplay;
        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        //textDisplay = (TextView) viewLayout.findViewById(R.id.textDisplay);
        //btnClose = (AppCompatImageView) viewLayout.findViewById(R.id.btnClose);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.picture);
        requestOptions.error(R.drawable.picture);

        Glide.with(_activity)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getGalleryImageUrl(false, picture.getId(), picture.getImageVersion()))
                .into(imgDisplay);

        _cb.updateNumber((position) + "/" + _pictures.size(), imgDisplay);

        /*btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.finish();
            }
        });*/

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public int getCount() {
        if (_pictures != null)
            return _pictures.size();
        else
            return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((FrameLayout) object);
    }

}
