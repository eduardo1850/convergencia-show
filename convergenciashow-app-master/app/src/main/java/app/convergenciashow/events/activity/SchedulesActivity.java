package app.convergenciashow.events.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ReturnBaseActivity;
import app.convergenciashow.events.fragment.EventScheduleFragment;
import app.convergenciashow.events.fragment.PersonalScheduleFragment;

public class SchedulesActivity extends ReturnBaseActivity {

    private BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_schedule);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setComponent() {
        openFragment(EventScheduleFragment.newInstance());
        navigationView = findViewById(R.id.navigationView);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment selectedFragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_schedule:
                        selectedFragment = EventScheduleFragment.newInstance();
                        openFragment(selectedFragment);
                        break;
                    case R.id.action_schedule_personal:
                        selectedFragment = PersonalScheduleFragment.newInstance();
                        openFragment(selectedFragment);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

}
