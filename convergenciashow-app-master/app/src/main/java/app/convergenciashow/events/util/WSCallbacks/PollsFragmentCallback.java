package app.convergenciashow.events.util.WSCallbacks;

public interface PollsFragmentCallback {

    void getActiveQuestionCB(Long idPoll);

}
