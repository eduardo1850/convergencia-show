package app.convergenciashow.events.data.repository;

import android.content.Context;

import app.convergenciashow.events.data.model.request.SendAMessageRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.GetConversationsResponse;
import app.convergenciashow.events.data.model.response.GetMessagesResponse;
import app.convergenciashow.events.data.model.response.SendAMessageResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.ConversationsCallback;
import app.convergenciashow.events.util.WSCallbacks.MessagesOfConversationCallback;
import app.convergenciashow.events.util.WSCallbacks.SendAMessageCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getConversationsService;

public class ConversationRepository {

    private Context context;

    public ConversationRepository(Context context){
        this.context = context;
    }

    public void getConversations(final AuthUser authUser, Long loadedElements, Integer numberOfRows, final ConversationsCallback cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<GetConversationsResponse> call = getConversationsService().listConversations(userHeader, loadedElements, numberOfRows);
        call.enqueue(new Callback<GetConversationsResponse>() {
            @Override
            public void onResponse(Call<GetConversationsResponse> call, Response<GetConversationsResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body().getConversations());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<GetConversationsResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getMessagesOfConversation(final AuthUser authUser, Long idUserWith, Long loadedElements, Integer numberOfRows, final MessagesOfConversationCallback cb) {
        String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<GetMessagesResponse> call = getConversationsService().conversationsMessages(userHeader, idUserWith, loadedElements, numberOfRows);
        call.enqueue(new Callback<GetMessagesResponse>() {
            @Override
            public void onResponse(Call<GetMessagesResponse> call, Response<GetMessagesResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body().getMessages());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<GetMessagesResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }


    public void sendMessageAConversation(final AuthUser authUser, SendAMessageRequest request, final SendAMessageCallback cb) {
        String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<SendAMessageResponse> call = getConversationsService().sendMessageAConversation(userHeader, request);
        call.enqueue(new Callback<SendAMessageResponse>() {
            @Override
            public void onResponse(Call<SendAMessageResponse> call, Response<SendAMessageResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<SendAMessageResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

}
