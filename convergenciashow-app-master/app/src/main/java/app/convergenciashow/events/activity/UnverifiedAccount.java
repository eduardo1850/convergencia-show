package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.iid.FirebaseInstanceId;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.UpdateSessionDeviceTokenRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AuthCallBack;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnverifiedAccount extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.unverified_bt_reload)
    Button bt_reload;
    @BindView(R.id.unverified_bt_cancel)
    Button bt_cancel;

    private AuthRepository repository;
    private AlertBuilderView progressBuilder, alertBuilderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unverified_account);
        ButterKnife.bind(this);
        setTitle("");
        repository = new AuthRepository(getApplicationContext(), this);
        progressBuilder = new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_progress, "");
        bt_cancel.setOnClickListener(this);
        bt_reload.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.unverified_bt_reload:
                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                UpdateSessionDeviceTokenRequest request = new UpdateSessionDeviceTokenRequest(refreshedToken);
                repository.updateSessionDeviceToken(repository.getAuth(), request, new AuthCallBack() {
                    @Override
                    public void onSuccess(@NonNull AuthUser value) {
                        if (value.isVerified()) {
                            AppPreferences.getInstance(UnverifiedAccount.this).saveLongData(Constants.ID_EVENT, value.getDefaultEvent());
                            Utils.validateEvent(UnverifiedAccount.this);
                        } else {
                            AlertBuilderView alertBuilderView = new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_error, getString(R.string.unverifiedaccount_text));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    Utils.invokeActivity(UnverifiedAccount.this, UnverifiedAccount.class, true);
                                }
                            });
                        }

                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null)
                            new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                        else
                            new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                    }
                });
                break;
            case R.id.unverified_bt_cancel:
                progressBuilder.show();
                repository.logOut(repository.getAuth(), new GenericCallBack() {
                    @Override
                    public void onSuccess(@NonNull BaseResponse value) {
                        progressBuilder.dismiss();
                        Utils.invokeActivity(UnverifiedAccount.this, LoginActivity.class, true);
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null)
                            new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                        else
                            new AlertBuilderView(UnverifiedAccount.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                    }
                });
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

}
