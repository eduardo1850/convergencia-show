package app.convergenciashow.events.data.model.request;

public class LikeRequest {

    private Long idPost;
    private boolean vote;

    public LikeRequest(Long idPost, boolean vote) {
        this.idPost = idPost;
        this.vote = vote;
    }
}
