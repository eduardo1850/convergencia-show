package app.convergenciashow.events.data.model.request;

public class NewRegisterRequest {

    private String name;
    private String lastName;
    private String email;
    private String password;
    private String profilePicture;
    private String so;
    private String device;

    public NewRegisterRequest(String name, String lastname, String email, String password, String profilePicture, String so, String device) {
        this.name = name;
        this.lastName = lastname;
        this.email = email;
        this.password = password;
        this.profilePicture = profilePicture;
        this.so = so;
        this.device = device;
    }
}
