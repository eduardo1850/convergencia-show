package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ForoDetailActivity;
import app.convergenciashow.events.domain.Lines;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

public class ForoDetailAdapter extends RecyclerView.Adapter<ForoDetailAdapter.ViewHolder> {

    private ArrayList<Lines> items;
    private Lines first;
    private Activity context;
    private Lines firstItem;
    private int numberofcomments;
    private ArrayList<Lines> controlItems;

    public ForoDetailAdapter(Lines first, int numberofcomments, Activity context) {
        this.first = first;
        items = new ArrayList<>();
        items.add(first);
        this.numberofcomments = numberofcomments;
        this.context = context;
    }

    public void update(ArrayList<Lines> linesItems) {
        firstItem = items.get(0);

        controlItems = new ArrayList<>();
        for (int a = 1; a < items.size(); a++) {
            controlItems.add(items.get(a));
        }
        items.clear();
        items.add(firstItem);
        items.addAll(linesItems);

        notifyDataSetChanged();
    }

    public void update(Lines lineItem) {
        items.add(lineItem);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ForoDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.foro_detail_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ForoDetailAdapter.ViewHolder viewHolder, int i) {
        if (items.size() > 0) {
            Lines n = items.get(i);

            viewHolder.line1.setText(n.getLine1());

            if (n.getLine2().equals("")) {
                viewHolder.line2.setVisibility(View.GONE);
            } else {
                viewHolder.line2.setText(Utils.timeIsRunningOut(n.getLine2(), context));
            }

            if (viewHolder.line3.getText().equals("Comentar")) {
                viewHolder.line3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ForoDetailActivity) context).showBoxComment();
                    }
                });
            }
            viewHolder.line3.setText(n.getLine3());

            //int numberOfComments = n.getComments();
            if (i == 0) {
                viewHolder.commentsImage.setVisibility(View.VISIBLE);
                viewHolder.txtNumber.setVisibility(View.VISIBLE);
                viewHolder.txtNumber.setText(numberofcomments + "");
            } else {
                viewHolder.commentsImage.setVisibility(View.GONE);
                viewHolder.txtNumber.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView line1;
        TextView line2;
        TextView line3;
        TextView txtNumber;
        ImageView commentsImage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            line1 = itemView.findViewById(R.id.line1);
            line2 = itemView.findViewById(R.id.line2);
            line3 = itemView.findViewById(R.id.line3);
            txtNumber = itemView.findViewById(R.id.txtNumber);
            commentsImage = itemView.findViewById(R.id.numberImage);
        }
    }
}
