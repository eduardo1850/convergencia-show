package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.request.MarkInEventRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ScheduleResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ScheduleService {

    @GET("expos/mobile/get-expos-no-auth")
    Call<ScheduleResponse> listNoAuth(
            @Query("idEvent") Long idEvent);

    @POST("expos/mobile/mark-for-personal-agenda")
    Call<BaseResponse> markForPersonalSchedule(
            @Header("auth") String headerAut,
            @Body MarkInEventRequest request);

    @GET("expos/mobile/get-expos")
    Call<ScheduleResponse> listAuth(
            @Header("auth") String headerAut,
            @Query("idEvent") Long idEvent);
}
