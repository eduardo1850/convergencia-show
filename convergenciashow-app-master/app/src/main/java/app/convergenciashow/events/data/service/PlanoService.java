package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.PlanoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface PlanoService {

    @GET("stands/mobile/get-stands")
    Call<PlanoResponse> getPlano(
            @Header("auth") String headerAuth,
            @Query("idEvent") Long idEvent);
}