package app.convergenciashow.events.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertBuilderView extends Dialog implements View.OnClickListener {

    @BindView(R.id.alert_ll_alert)
    LinearLayout ll_alert;
    @BindView(R.id.alert_ll_progress)
    LinearLayout ll_progress;
    @BindView(R.id.alert_iv_logo)
    AppCompatImageView iv_logo;
    @BindView(R.id.alert_tv_text)
    TextView tv_text;
    @BindView(R.id.alert_btn_ok)
    Button btn_ok;
    @BindView(R.id.alert_btn_nok)
    Button btn_nok;

    private Activity act;
    private Operations op;
    private String txt;
    private View.OnClickListener lst = null;

    public AlertBuilderView(@NonNull Activity activity, Operations operation, String text) {
        super(activity);
        this.act = activity;
        this.op = operation;
        this.txt = text;
    }

    public AlertBuilderView(@NonNull Activity activity, View.OnClickListener listener, Operations operation, String text) {
        super(activity);
        this.act = activity;
        this.lst = listener;
        this.op = operation;
        this.txt = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_alertbuilder);
        ButterKnife.bind(this);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setCancelable(false);
        
        if (op == Operations.AlertBuilder_progress) {
            ll_alert.setVisibility(View.GONE);
            ll_progress.setVisibility(View.VISIBLE);
        } else {
            ll_alert.setVisibility(View.VISIBLE);
            ll_progress.setVisibility(View.GONE);

            if (lst != null) {
                btn_ok.setOnClickListener(lst);
                btn_nok.setOnClickListener(lst);
            } else {
                btn_ok.setOnClickListener(this);
                btn_nok.setOnClickListener(this);
            }


            if (op == Operations.AlertBuilder_success || op == Operations.AlertBuilder_success_double)
                iv_logo.setBackgroundDrawable(act.getResources().getDrawable(R.drawable.ic_check_mark));
            else
                iv_logo.setBackgroundDrawable(act.getResources().getDrawable(R.drawable.ic_error_mark));


            tv_text.setText(txt);
            btn_ok.setText(act.getResources().getText(R.string.accept_text));
            btn_nok.setText(act.getResources().getText(R.string.cancel_text));

            if (op == Operations.AlertBuilder_linkedin
                    || op == Operations.AlertBuilder_success_double
                    || op == Operations.AlertBuilder_error_double)
                btn_nok.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.alert_btn_ok:
                if (op == Operations.AlertBuilder_linkedin) {
                    final String appPackageName = act.getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        act.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Constants.LinkedIn_Package)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        act.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
                dismiss();
                break;
            case R.id.alert_btn_nok:
                dismiss();
                break;
        }
    }


}
