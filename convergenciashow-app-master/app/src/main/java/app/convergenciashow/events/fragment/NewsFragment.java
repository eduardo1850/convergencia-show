package app.convergenciashow.events.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.NewsAdapter;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.NewsRepository;
import app.convergenciashow.events.domain.New;
import app.convergenciashow.events.util.WSCallbacks.NewsCallBack;

import java.util.ArrayList;

public class NewsFragment extends BaseFragment {

    private static final String TITLE_BAR = "Noticias";
    private RecyclerView newsRecyclerView;
    private NewsRepository repository;
    private AuthRepository authRepository;
    private Long loadedElements = 0L;
    private static final int rows = 3;
    private int rvLastVisibleItem;
    private boolean loading = false;
    private NewsAdapter adapter;

    public static NewsFragment newInstance() {
        NewsFragment fragment = new NewsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(TITLE_BAR);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        repository = new NewsRepository();
        loadElements();
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    protected void setComponent() {
        adapter = new NewsAdapter(new ArrayList<New>(), getActivity(), getContext());
        newsRecyclerView = getActivity().findViewById(R.id.newsRv);
        newsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRecyclerView.setAdapter(adapter);

        newsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) newsRecyclerView.getLayoutManager();
                rvLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && rvLastVisibleItem == newsRecyclerView.getChildCount()) {
                    loading = true;
                    loadElements();
                }
            }
        });

    }

    public void loadElements() {
        authRepository = new AuthRepository(getContext(), getActivity());
        repository.getNews(authRepository.getAuth(), loadedElements, rows, new NewsCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<New> value) {
                adapter.update(value);
                loadedElements = loadedElements + rows;
                loading = false;
            }

            @Override
            public void onError(@NonNull Throwable throwable) {

            }
        });

    }

}
