package app.convergenciashow.events.data.model.request;

public class OptionsRequest {

    private Long id;
    private Integer answers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnswers() {
        return answers;
    }

    public void setAnswers(Integer answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Options{" +
                "id=" + id +
                ", answers=" + answers +
                '}';
    }

}
