package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.request.AnswerQuestionRequest;
import app.convergenciashow.events.data.model.response.ActiveQuestionResponse;
import app.convergenciashow.events.data.model.response.AnswerQuestionResponse;
import app.convergenciashow.events.data.model.response.EventsResponse;
import app.convergenciashow.events.data.model.response.GlobalQuestionResponse;
import app.convergenciashow.events.data.model.response.PollsResponse;
import app.convergenciashow.events.data.model.response.SponsorsResponse;
import app.convergenciashow.events.data.model.response.DocumentsResponse;
import app.convergenciashow.events.data.model.response.EventDetailResponse;
import app.convergenciashow.events.data.model.response.GalleryResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EventDetailService {

    @GET("events/get-event-details-mobile")
    Call<EventDetailResponse> getDetail(
            @Header("auth") String headerAut,
            @Query("idEvent") String idEvent);

    @GET("events/get-gallery")
    Call<GalleryResponse> getGallery(
            @Header("auth") String headerAut,
            @Query("idEvent") Long idEvent);

    @GET("events/get-documents")
    Call<DocumentsResponse> getDocuments(
            @Header("auth") String headerAut,
            @Query("idEvent") Long idEvent);

    @GET("events/get-events-mobile")
    Call<EventsResponse> getEvents(
            @Header("auth") String headerAut,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows);

    @GET("events/get-polls-mobile")
    Call<PollsResponse> getPolls(
            @Header("auth") String headerAut,
            @Query("idEvent") String idEvent,
            @Query("searchFilter") String filter);
    @GET("events/get-global-polls-mobile")
    Call<PollsResponse> getGlobalPolls(
            @Header("auth") String headerAut,
            @Query("idEvent") String idEvent,
            @Query("searchFilter") String filter);
    @POST("events/answer-a-question")
    Call<AnswerQuestionResponse> answerQuestion(
            @Header("auth") String headerAut,
            @Body AnswerQuestionRequest request);
    @POST("events/answer-a-global-question")
    Call<AnswerQuestionResponse> answerGlobalQuestion(
            @Header("auth") String headerAut,
            @Body AnswerQuestionRequest request);
    @GET("events/get-active-question")
    Call<ActiveQuestionResponse> getActiveQuestion(
            @Header("auth") String headerAut,
            @Query("idPoll") Long idPoll);

    @GET("events/get-next-question")
    Call<GlobalQuestionResponse> getNextQuestion(
            @Header("auth") String headerAut,
            @Query("idPoll") Long idPoll);

    @GET("sponsors/mobile/get-sponsors")
    Call<SponsorsResponse> getSponsors(
            @Header("auth") String headerAut,
            @Query("idEvent") Long idEvent);
}
