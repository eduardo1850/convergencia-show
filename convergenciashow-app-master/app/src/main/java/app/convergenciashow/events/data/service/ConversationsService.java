package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.request.SendAMessageRequest;
import app.convergenciashow.events.data.model.response.GetConversationsResponse;
import app.convergenciashow.events.data.model.response.GetMessagesResponse;
import app.convergenciashow.events.data.model.response.SendAMessageResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ConversationsService {

    @GET("conversations/get-conversations")
    Call<GetConversationsResponse> listConversations(
            @Header("auth") String headerAut,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows);


    @GET("conversations/get-messages")
    Call<GetMessagesResponse> conversationsMessages(
            @Header("auth") String headerAut,
            @Query("idUserWith") Long idUserWith,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows);

    @POST("conversations/send-a-message")
    Call<SendAMessageResponse> sendMessageAConversation(
            @Header("auth") String headerAut,
            @Body SendAMessageRequest request);

}
