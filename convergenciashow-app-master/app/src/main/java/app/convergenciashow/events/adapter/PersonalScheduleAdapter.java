package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.MarkInEventRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ScheduleRepository;
import app.convergenciashow.events.domain.Expo;
import app.convergenciashow.events.fragment.PersonalScheduleFragment;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.util.ArrayList;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class PersonalScheduleAdapter extends RecyclerView.Adapter<PersonalScheduleAdapter.ViewHolder> {

    private ArrayList<Object> items;
    private final AuthRepository authRepository;
    private final ScheduleRepository repository;
    private Activity activity;
    private PersonalScheduleFragment fragment;
    private AlertBuilderView progressBuilder;

    public PersonalScheduleAdapter(ArrayList<Object> items, PersonalScheduleFragment fragment, AuthRepository authRepository) {
        this.items = items;
        this.activity = fragment.getActivity();
        this.authRepository = authRepository;
        this.repository = new ScheduleRepository(fragment.getContext());
        this.fragment = fragment;
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
    }

    public void update(ArrayList<Object> expoItems) {
        items.clear();
        items.addAll(expoItems);
        progressBuilder.dismiss();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PersonalScheduleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        ViewHolder vh;
        // create a new view
        switch (viewType) {
            case 2:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.personal_schedule_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.date_item, viewGroup, false);
                vh = new ViewHolder(v, viewType);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final PersonalScheduleAdapter.ViewHolder viewHolder, final int i) {
        final Object n = items.get(i);
        String date;
        String hour;

        if (n instanceof String) {
            viewHolder.dateCalendar.setText(Utils.setDateSchedule(((String) n), activity));
        } else {
            hour = ((Expo) n).getDate().substring(11, 16);
            viewHolder.titleCalendar.setText(((Expo) n).getTitle());
            //viewHolder.hourCalendar.setText(hour + " - " + ((Expo) n).getRoomTitle());
            viewHolder.hourCalendar.setText(Utils.getDescription(n));
            viewHolder.speakersCalendar.setText(((Expo) n).getSpeakers());

            viewHolder.idExpo = ((Expo) n).getId();
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((Expo) n).isMarkedForPersonalAgenda()) {
                        progressBuilder.show();
                        MarkInEventRequest request = new MarkInEventRequest(viewHolder.idExpo, false);
                        repository.markForPersonalSchedule(authRepository.getAuth(), request, new GenericCallBack() {
                            @Override
                            public void onSuccess(@NonNull BaseResponse value) {
                                fragment.updateReciclerView(cleanPersonalScheduleInfo(viewHolder.idExpo));
                            }

                            @Override
                            public void onError(@NonNull BaseResponse baseResponse) {
                                progressBuilder.dismiss();
                                if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                    AlertBuilderView alertBuilderView = new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.session_error));
                                    alertBuilderView.show();
                                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            authRepository.logOut();
                                            Utils.invokeActivity(activity, LoginActivity.class, true);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof String) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleCalendar;
        TextView hourCalendar;
        TextView speakersCalendar;
        TextView dateCalendar;
        TextView delete;
        Long idExpo;
        boolean isMarked;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);


            if (viewType == 2) {
                titleCalendar = itemView.findViewById(R.id.titleCalendar);
                hourCalendar = itemView.findViewById(R.id.hourCalendar);
                speakersCalendar = itemView.findViewById(R.id.speakersCalendar);
                delete = itemView.findViewById(R.id.deleteText);
            } else if (viewType == 1) {
                dateCalendar = itemView.findViewById(R.id.itemDate);
            }

        }

    }

    private ArrayList<Object> cleanPersonalScheduleInfo(final Long idExpo) {
        ArrayList<Object> list = new ArrayList<>();
        try {
            for (int i = 0; i < items.size(); i++) {
                if ((items.get(i) instanceof String)) {
                    list.add(items.get(i));
                } else {
                    if (((Expo) items.get(i)).getId() != idExpo)
                        list.add(items.get(i));
                }
            }

            int stringCount = 0;
            for (int i = 0; i < list.size(); i++) {
                if ((list.get(i) instanceof String)) {
                    stringCount++;
                    if (stringCount == 2) {
                        list.remove(i - 1);
                        stringCount = 0;
                    }
                } else {
                    stringCount = 0;
                }
            }

            if ((list.get(list.size() - 1) instanceof String))
                list.remove(list.get(list.size() - 1));

        } catch (Exception exception) {
            return list;
        }
        return list;
    }


}
