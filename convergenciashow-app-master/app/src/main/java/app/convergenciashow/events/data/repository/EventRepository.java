package app.convergenciashow.events.data.repository;

import android.content.Context;

import app.convergenciashow.events.data.model.request.AnswerQuestionRequest;
import app.convergenciashow.events.data.model.response.ActiveQuestionResponse;
import app.convergenciashow.events.data.model.response.AnswerQuestionResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.CategoriesResponse;
import app.convergenciashow.events.data.model.response.DocumentResponse;
import app.convergenciashow.events.data.model.response.DocumentsResponse;
import app.convergenciashow.events.data.model.response.EventDetailResponse;
import app.convergenciashow.events.data.model.response.EventResponse;
import app.convergenciashow.events.data.model.response.EventsResponse;
import app.convergenciashow.events.data.model.response.GalleryResponse;
import app.convergenciashow.events.data.model.response.GlobalQuestionResponse;
import app.convergenciashow.events.data.model.response.PicturesResponse;
import app.convergenciashow.events.data.model.response.PollsResponse;
import app.convergenciashow.events.data.model.response.SponsorResponse;
import app.convergenciashow.events.data.model.response.SponsorsResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Document;
import app.convergenciashow.events.domain.Event;
import app.convergenciashow.events.domain.EventDetail;
import app.convergenciashow.events.domain.GallerySections;
import app.convergenciashow.events.domain.Pictures;
import app.convergenciashow.events.domain.Sponsor;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.ActiveQuestionCallback;
import app.convergenciashow.events.util.WSCallbacks.AnswerQuestionCallback;
import app.convergenciashow.events.util.WSCallbacks.DocumentsCallBack;
import app.convergenciashow.events.util.WSCallbacks.EventDetailCallBack;
import app.convergenciashow.events.util.WSCallbacks.EventsCallback;
import app.convergenciashow.events.util.WSCallbacks.GalleryCallBack;
import app.convergenciashow.events.util.WSCallbacks.GlobalQuestionCallback;
import app.convergenciashow.events.util.WSCallbacks.PollsCallback;
import app.convergenciashow.events.util.WSCallbacks.SponsorsCallBack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getEventDetailService;

public class EventRepository {

    private Context context;

    public EventRepository(Context context) {
        this.context = context;
    }

    public void getDetail(final AuthUser authUser, String idEvent, final EventDetailCallBack cb) {
        final String userHeader = authUser.getIdUser() + " " + authUser.getIdSession();
        Call<EventDetailResponse> callResponse = getEventDetailService().getDetail(userHeader, idEvent);
        callResponse.enqueue(new Callback<EventDetailResponse>() {
            @Override
            public void onResponse(Call<EventDetailResponse> call, Response<EventDetailResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<EventDetailResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getGallery(AuthUser user, Long idEvent, final GalleryCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<GalleryResponse> call = getEventDetailService().getGallery(userHeader, idEvent);
        call.enqueue(new Callback<GalleryResponse>() {
            @Override
            public void onResponse(Call<GalleryResponse> call, Response<GalleryResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<GalleryResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getDocuments(AuthUser user, Long idEvent, final DocumentsCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<DocumentsResponse> call = getEventDetailService().getDocuments(userHeader, idEvent);
        call.enqueue(new Callback<DocumentsResponse>() {
            @Override
            public void onResponse(Call<DocumentsResponse> call, Response<DocumentsResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<DocumentsResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getEvents(AuthUser user, Long loadedElements, Integer numberOfRows, final EventsCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<EventsResponse> call = getEventDetailService().getEvents(userHeader, loadedElements, numberOfRows);
        call.enqueue(new Callback<EventsResponse>() {
            @Override
            public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<EventsResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getPolls(AuthUser user,int pollType,String idEvent, String filter, final PollsCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<PollsResponse> call = pollType==1?getEventDetailService().getGlobalPolls(userHeader, idEvent, filter):getEventDetailService().getPolls(userHeader, idEvent, filter);
        call.enqueue(new Callback<PollsResponse>() {
            @Override
            public void onResponse(Call<PollsResponse> call, Response<PollsResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<PollsResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void answerQuestion(AuthUser user, AnswerQuestionRequest request, final AnswerQuestionCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<AnswerQuestionResponse> call = getEventDetailService().answerQuestion(userHeader, request);
        call.enqueue(new Callback<AnswerQuestionResponse>() {
            @Override
            public void onResponse(Call<AnswerQuestionResponse> call, Response<AnswerQuestionResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<AnswerQuestionResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }
    public void answerGlobalQuestion(AuthUser user, AnswerQuestionRequest request, final AnswerQuestionCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<AnswerQuestionResponse> call = getEventDetailService().answerGlobalQuestion(userHeader, request);
        call.enqueue(new Callback<AnswerQuestionResponse>() {
            @Override
            public void onResponse(Call<AnswerQuestionResponse> call, Response<AnswerQuestionResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<AnswerQuestionResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getActiveQuestion(AuthUser user, Long idPoll, final ActiveQuestionCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<ActiveQuestionResponse> call = getEventDetailService().getActiveQuestion(userHeader, idPoll);
        call.enqueue(new Callback<ActiveQuestionResponse>() {
            @Override
            public void onResponse(Call<ActiveQuestionResponse> call, Response<ActiveQuestionResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<ActiveQuestionResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    public void getNextQuestion(AuthUser user, Long idPoll, final GlobalQuestionCallback cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<GlobalQuestionResponse> call = getEventDetailService().getNextQuestion(userHeader, idPoll);
        call.enqueue(new Callback<GlobalQuestionResponse>(){
            @Override
            public void onResponse(Call<GlobalQuestionResponse> call, Response<GlobalQuestionResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(response.body());
                }
                else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<GlobalQuestionResponse> call, Throwable t) {
                cb.onError(null);
            }

        });
    }


    public void getSponsors(AuthUser user, Long IdEvent, final SponsorsCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<SponsorsResponse> call = getEventDetailService().getSponsors(userHeader, IdEvent);
        call.enqueue(new Callback<SponsorsResponse>() {
            @Override
            public void onResponse(Call<SponsorsResponse> call, Response<SponsorsResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<SponsorsResponse> call, Throwable t) {
                cb.onError(null);
            }
        });

    }

    private ArrayList<Event> transform(EventsResponse eventsResponse) {
        ArrayList<Event> events = new ArrayList<>();
        for (EventResponse e : eventsResponse.getEvents()) {
            Event event = new Event();
            event.setId(e.getId());
            event.setName(e.getName());
            event.setAddress(e.getAddress());
            event.setDescription(e.getDescription());
            event.setDateInit(e.getDateInit());
            event.setDateEnd(e.getDateEnd());
            event.setLat(e.getLat());
            event.setLon(e.getLon());
            event.setImageVersion(e.getImageVersion());
            events.add(event);
        }
        return events;
    }

    private ArrayList<Object> transform(SponsorsResponse sponsorsResponse) {
        ArrayList<Object> list = new ArrayList<>();
        if ((sponsorsResponse.getSponsors().size() > 0) && (sponsorsResponse.getCategories().size() > 0)) {
            Collections.sort(sponsorsResponse.getCategories());
            for (CategoriesResponse c : sponsorsResponse.getCategories()) {
                list.add(c.getName());
                for (SponsorResponse s : sponsorsResponse.getSponsors()) {
                    if (s.getCategoryId().equals(c.getId())) {
                        Sponsor sponsor = new Sponsor();
                        sponsor.setCategoryId(s.getCategoryId());
                        sponsor.setId(s.getId());
                        sponsor.setImageVersion(s.getImageVersion());
                        sponsor.setTitle(s.getTitle());
                        sponsor.setWebPage(s.getWebPage());
                        list.add(sponsor);
                    }
                }

                if (list.get(list.size() - 1) instanceof String) {
                    list.remove(list.size() - 1);
                }
            }

            if (list.get(list.size() - 1) instanceof String)
                list.remove(list.size() - 1);
        }
        return list;
    }

    private ArrayList<Object> transform(GalleryResponse galleryResponse) {
        ArrayList<Object> list = new ArrayList<>();
        if (galleryResponse != null) {
            if (galleryResponse.getPictures().size() > 0) {
                ArrayList<Pictures> pictures = new ArrayList<>();
                for (PicturesResponse pr : galleryResponse.getPictures()) {
                    Pictures p = new Pictures();
                    p.setId(pr.getId());
                    p.setTitle(pr.getTitle());
                    p.setUploadDate(pr.getUploadDate());
                    p.setDate(pr.getUploadDate());
                    p.setImageVersion(pr.getImageVersion());
                    pictures.add(p);
                }
                Collections.sort(pictures);

                // Gallery Sections
                ArrayList<GallerySections> sections = new ArrayList<>();
                sections.add(new GallerySections(0, Utils.getGalleryDate(pictures.get(0).getUploadDate(), context)));
                if (pictures.size() > 1) {
                    for (int i = 0; i < pictures.size() - 1; i++) {
                        list.add(pictures.get(i));
                        if (i > 0)
                            if (pictures.get(i - 1).getDate().before(pictures.get(i).getDate()))
                                sections.add(new GallerySections(i, Utils.getGalleryDate(pictures.get(i).getUploadDate(), context)));
                    }
                }
                list.add(sections);
            } else {
                return list;
            }
        } else {
            return list;
        }
        return list;
    }

    private List<Document> transform(DocumentsResponse documentsResponse) {
        List<Document> documents = new ArrayList<>();
        for (DocumentResponse dr : documentsResponse.getDocuments()) {
            Document d = new Document();
            d.setId(dr.getId());
            d.setTitle(dr.getTitle());
            d.setDocumentVersion(dr.getDocumentVersion());
            documents.add(d);
        }
        return documents;
    }

    private EventDetail transform(EventDetailResponse response) {
        EventDetail detail = new EventDetail();
        detail.setAddress(response.getAddress() + "");
        detail.setDateEnd(response.getDateEnd());
        detail.setDateInit(response.getDateInit());
        detail.setDescription(response.getDescription());
        detail.setId(response.getId());
        detail.setImageVersion(response.getImageVersion());
        detail.setLat(response.getLat());
        detail.setLon(response.getLon());
        detail.setName(response.getName());
        detail.setWillUserBeInEvent(response.isWillUserBeInEvent());
        detail.setGlobalMapAvailable(response.isGlobalMapAvailable());
        detail.setGlobalMapVersion(response.getGlobalMapVersion());
        detail.setGlobalMapVersion(response.getGlobalMapVersion());
        return detail;
    }
}
