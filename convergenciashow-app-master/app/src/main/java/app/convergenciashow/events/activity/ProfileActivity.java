package app.convergenciashow.events.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.NoActionBarAcitivity;
import app.convergenciashow.events.data.model.request.AuthRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.WSCallbacks.AuthCallBack;

public class ProfileActivity extends NoActionBarAcitivity {
    private Button authButton;
    private EditText claveEditText;
    private TextView laterTxt;
    private ImageView close;

    private AuthRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
    }

    private AuthRequest getRequest() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AuthRequest request = new AuthRequest(claveEditText.getText().toString(), "", SO, refreshedToken);
        return request;
    }

    @Override
    protected void setComponent() {
        authButton = findViewById(R.id.authBtn);
        claveEditText = findViewById(R.id.claveEditText);
        laterTxt = findViewById(R.id.laterTxt);
        close = findViewById(R.id.close_btn);
        authButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repository.authenticate(getRequest(), new AuthCallBack() {
                    @Override
                    public void onSuccess(@NonNull AuthUser value) {
                        setFragmentToShow(1);
                        finish();
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        //LANZAR DIALOG
                        finish();
                    }
                });
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    protected void initRepository() {
        repository = new AuthRepository(getApplicationContext(), this);
    }
}
