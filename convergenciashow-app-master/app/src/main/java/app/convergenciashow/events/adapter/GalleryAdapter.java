package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.PictureDescriptionActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.domain.Pictures;
import app.convergenciashow.events.fragment.GalleryFragment;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private GalleryFragment fragment;
    private Activity activity;
    private Context context;
    private List<Object> pictures;
    private AlertBuilderView progressBuilder;


    public GalleryAdapter(List<Object> picture, GalleryFragment fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
        this.context = fragment.getContext();
        this.pictures = picture;
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
        progressBuilder.show();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        GalleryAdapter.ViewHolder vh;
        progressBuilder.dismiss();
        switch (viewType) {
            case 2:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
                vh = new GalleryAdapter.ViewHolder(v, viewType);
                return vh;
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_date_item, viewGroup, false);
                vh = new GalleryAdapter.ViewHolder(v, viewType);
                return vh;
        }

        //View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        //GalleryAdapter.ViewHolder vh = new GalleryAdapter.ViewHolder(v);
        //return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder viewHolder, int i) {
        final Object n = pictures.get(i);
        if (n instanceof String) {
            viewHolder.date.setText(((String) n));
        } else {
            final Pictures picture = (Pictures) pictures.get(i);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.picture);
            requestOptions.error(R.drawable.picture);
            Glide.with(activity)
                    .setDefaultRequestOptions(requestOptions)
                    .load(Utils.getGalleryImageUrl(true, picture.getId(), picture.getImageVersion()))
                    .into(viewHolder.picture);

            viewHolder.picture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, PictureDescriptionActivity.class);
                    intent.putExtra(Constants.PICTUREURL, Utils.getGalleryImageUrl(false, picture.getId(), picture.getImageVersion()));
                    intent.putExtra(Constants.PICTUREDECRIPTION, picture.getTitle());
                    activity.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (pictures.get(position) instanceof String) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView picture;
        TextView date;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == 2) {
                picture = itemView.findViewById(R.id.gallery_iv_item);
            } else if (viewType == 1) {
                date = itemView.findViewById(R.id.gallery_date);
            }
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
    }

}
