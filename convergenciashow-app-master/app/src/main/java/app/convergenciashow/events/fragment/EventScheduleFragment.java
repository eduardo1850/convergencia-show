package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.EventScheduleAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ScheduleRepository;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.ScheduleCallBack;

import java.util.ArrayList;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class EventScheduleFragment extends BaseReturnFragment {

    private RecyclerView calendarRecyclerView;
    private LinearLayout ll_empty_schedule;
    private AuthRepository authRepository;
    private ScheduleRepository repository;
    private EventScheduleAdapter adapter;
    private AlertBuilderView progressBuilder;

    public static EventScheduleFragment newInstance() {
        EventScheduleFragment fragment = new EventScheduleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setTitleBar(getActivity().getString(R.string.back));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        repository = new ScheduleRepository(getContext());
        authRepository = new AuthRepository(getContext(), getActivity());
        loadElements();
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    protected void setComponent() {
        ll_empty_schedule = getActivity().findViewById(R.id.llmyschedule);
        adapter = new EventScheduleAdapter(new ArrayList<>(), this, authRepository);
        calendarRecyclerView = getActivity().findViewById(R.id.calendarRv);
        calendarRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        calendarRecyclerView.setAdapter(adapter);
    }

    public void loadElements() {
        progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
        progressBuilder.show();
        repository.getItemsAuth(this, authRepository.getAuth(), AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT), new ScheduleCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<Object> value) {
                progressBuilder.dismiss();
                if (value.size() == 0) {
                    ll_empty_schedule.setVisibility(View.VISIBLE);
                    calendarRecyclerView.setVisibility(View.GONE);
                } else {
                    ll_empty_schedule.setVisibility(View.GONE);
                    calendarRecyclerView.setVisibility(View.VISIBLE);
                    adapter.update(value);
                }

            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                progressBuilder.dismiss();
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        if (isAdded()) {
                            AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    authRepository.logOut();
                                    Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                                }
                            });
                        }
                    } else {
                        if (isAdded())
                            new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    if (isAdded())
                        new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });

    }

}
