package app.convergenciashow.events.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.NotificationsAdapter;
import app.convergenciashow.events.domain.Notification;
import app.convergenciashow.events.data.repository.NotificationRepository;
import app.convergenciashow.events.util.WSCallbacks.NotificationsCallback;

import java.util.ArrayList;

public class NotificationsFragment extends BaseFragment {

    private static final String TITLE_BAR = "Notificaciones";
    private RecyclerView notificationsRecyclerView;
    private NotificationRepository repository;
    private Long loadedElements = 0L;
    private static final int rows = 7;
    private int rvLastVisibleItem;
    private boolean loading = false;
    private LinearLayout llnotifications;
    private NotificationsAdapter adapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    public static NotificationsFragment newInstance() {
        NotificationsFragment fragment = new NotificationsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(TITLE_BAR);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        repository = new NotificationRepository();
        loadElements();
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    protected void setComponent() {
        adapter = new NotificationsAdapter(new ArrayList<Notification>(), getContext());
        notificationsRecyclerView = getActivity().findViewById(R.id.notificationsRv);
        llnotifications = getActivity().findViewById(R.id.llnotifications);
        notificationsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        notificationsRecyclerView.setAdapter(adapter);

        notificationsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) notificationsRecyclerView
                        .getLayoutManager();
                rvLastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                if (!loading
                        && rvLastVisibleItem == notificationsRecyclerView.getChildCount()) {
                    loading = true;
                    loadElements();
                }
            }
        });

    }

    public void loadElements() {
        repository.getNotifications(loadedElements, rows, new NotificationsCallback() {
            @Override
            public void onSuccess(@NonNull ArrayList<Notification> value) {
                if (value.size() > 0) {
                    adapter.update(value);
                    loadedElements = loadedElements + rows;
                    loading = false;
                    llnotifications.setVisibility(View.GONE);
                    notificationsRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    if (notificationsRecyclerView.getChildCount() == 0) {
                        llnotifications.setVisibility(View.VISIBLE);
                        notificationsRecyclerView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                llnotifications.setVisibility(View.VISIBLE);
                notificationsRecyclerView.setVisibility(View.GONE);
            }
        });

    }

}
