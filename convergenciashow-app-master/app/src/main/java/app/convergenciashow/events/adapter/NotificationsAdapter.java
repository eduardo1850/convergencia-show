package app.convergenciashow.events.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import app.convergenciashow.events.R;
import app.convergenciashow.events.domain.Notification;
import app.convergenciashow.events.util.Constants;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder>{

    private ArrayList<Notification> items;
    private Context context;

    public NotificationsAdapter(ArrayList<Notification> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public void update(ArrayList<Notification> notificationItems){
        items.addAll(notificationItems);
        notifyDataSetChanged();
    }

    private String getAuthorImageUrl(Long authorId, int version) {
        return Constants.AUTHOR_URL_PATH + authorId + Constants.JPG  + Constants.VER + version;
    }

    @NonNull
    @Override
    public NotificationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsAdapter.ViewHolder viewHolder, int i) {
        Notification n = items.get(i);
        viewHolder.authorName.setText(n.getAdminName() + " " + n.getAdminLastName());
        Glide.with(context).load(getAuthorImageUrl(n.getIdAdmin(), n.getImageVersion())).into(viewHolder.authorImage);
        viewHolder.date.setText(n.getDate());
        viewHolder.newsContent.setText(n.getMessage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView authorName;
        TextView date;
        ImageView authorImage;
        TextView newsContent;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            authorImage = itemView.findViewById(R.id.authorImage);
            authorName = itemView.findViewById(R.id.authorName);
            date = itemView.findViewById(R.id.date);
            newsContent = itemView.findViewById(R.id.notificationContent);
        }
    }
}
