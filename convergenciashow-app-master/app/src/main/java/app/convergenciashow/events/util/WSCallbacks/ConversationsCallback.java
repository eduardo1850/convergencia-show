package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ConversationResponse;

public interface ConversationsCallback {
    void onSuccess(@NonNull ArrayList<ConversationResponse> response);

    void onError(@NonNull BaseResponse baseResponse);
}
