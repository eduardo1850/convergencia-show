package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ForoDetailActivity;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.LikeRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ForoRepository;
import app.convergenciashow.events.domain.Post;
import app.convergenciashow.events.fragment.ForoFragment;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ForoAdapter extends RecyclerView.Adapter<ForoAdapter.ViewHolder> {

    private ArrayList<Post> items;
    private ForoFragment fragment;
    private ForoRepository repository;
    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;

    public ForoAdapter(ArrayList<Post> items, ForoFragment fragment, AuthRepository authRepository) {
        this.items = items;
        this.fragment = fragment;
        this.authRepository = authRepository;
    }

    public void update(ArrayList<Post> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.post_item, viewGroup, false);
        ForoAdapter.ViewHolder vh = new ForoAdapter.ViewHolder(v);
        repository = new ForoRepository(fragment.getActivity());
        progressBuilder = new AlertBuilderView(fragment.getActivity(), Operations.AlertBuilder_progress, "");

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ForoAdapter.ViewHolder viewHolder, int i) {
        final Post item = items.get(i);
        final Activity activity = fragment.getActivity();

        Glide.with(activity).load(Utils.getForumImageUrl(fragment.getActivity(), item.getId(), item.getImageVersion())).into(viewHolder.postImage);
        viewHolder.postName.setText(item.getDescription());
        viewHolder.postDate.setText(Utils.getParseForumDate(item.getDate()));
        viewHolder.tv_comments.setText(item.getMessages() + "");
        viewHolder.tv_likes.setText(item.getUpvotes() + "");

        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ForoDetailActivity.class);
                intent.putExtra("postObject", item);
                intent.putExtra("postName", item.getDescription());
                intent.putExtra("id", item.getId());
                activity.startActivity(intent);
            }
        });

        //  ****************  Likes ****************
        Utils.changeLikeStatus(item.isVoted(), viewHolder.iv_likes);
        viewHolder.likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBuilder.show();
                final boolean isVoted = item.isVoted() ? false : true;
                LikeRequest request = new LikeRequest(item.getId(), item.isVoted() ? false : true);
                repository.putLike(authRepository.getAuth(), request, new GenericCallBack() {
                    @Override
                    public void onSuccess(@NonNull BaseResponse value) {
                        progressBuilder.dismiss();
                        if (isVoted) {
                            item.setVoted(true);
                            item.setUpvotes(item.getUpvotes() + 1);
                        } else {
                            item.setVoted(false);
                            item.setUpvotes(item.getUpvotes() - 1);
                        }
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null) {
                            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                AlertBuilderView alertBuilderView = new AlertBuilderView(fragment.getActivity(), Operations.AlertBuilder_error, fragment.getActivity().getString(R.string.session_error));
                                alertBuilderView.show();
                                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        authRepository.logOut();
                                        Utils.invokeActivity(fragment.getActivity(), LoginActivity.class, true);
                                    }
                                });
                            } else {

                                new AlertBuilderView(fragment.getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            }
                        } else {
                            new AlertBuilderView(fragment.getActivity(), Operations.AlertBuilder_error, fragment.getActivity().getString(R.string.generic_error)).show();
                        }
                    }
                });
            }
        });

        viewHolder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri bmpUri = Utils.getLocalBitmapUri(viewHolder.postImage);
                if (bmpUri != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    shareIntent.setType("image/*");
                    fragment.getActivity().startActivity(Intent.createChooser(shareIntent, "Share Image"));
                } else {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, activity.getString(R.string.app_name));
                    fragment.getActivity().startActivity(Intent.createChooser(shareIntent, "Share Image"));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.postImage)
        ImageView postImage;
        @BindView(R.id.postName)
        TextView postName;
        @BindView(R.id.postDate)
        TextView postDate;
        @BindView(R.id.container)
        View container;
        @BindView(R.id.ll_comments)
        LinearLayout comments;
        @BindView(R.id.tv_comments)
        TextView tv_comments;
        @BindView(R.id.ll_likes)
        LinearLayout likes;
        @BindView(R.id.tv_likes)
        TextView tv_likes;
        @BindView(R.id.ll_share)
        LinearLayout share;
        @BindView(R.id.iv_likes)
        ImageView iv_likes;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
