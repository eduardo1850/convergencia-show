package app.convergenciashow.events.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import app.convergenciashow.events.R;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static app.convergenciashow.events.util.Constants.FRAGMENT_GALLERY;
import static app.convergenciashow.events.util.Constants.PICTUREDECRIPTION;
import static app.convergenciashow.events.util.Constants.PICTUREURL;

public class PictureDescriptionActivity extends AppCompatActivity {

    @BindView(R.id.imagedes_iv)
    ImageView imagedes_iv;
    @BindView(R.id.imagedes_iv_close)
    ImageView imagedes_iv_close;
    @BindView(R.id.imagedes_iv_share)
    ImageView imagedes_iv_share;
    //@BindView(R.id.imagedes_tv_desc)
    //TextView imagedes_tv_desc;

    private String imageURL, imageTitle;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_description);
        unbinder = ButterKnife.bind(this);
        this.setFinishOnTouchOutside(false);

        if (getIntent().getExtras() != null) {
            imageURL = getIntent().getExtras().getString(PICTUREURL);
            imageTitle = getIntent().getExtras().getString(PICTUREDECRIPTION);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.glideplaceholder);
            requestOptions.error(R.drawable.placeholderpicture);
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions)
                    .load(imageURL)
                    .into(imagedes_iv);

            imagedes_iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppPreferences.getInstance(PictureDescriptionActivity.this).saveIntData(Constants.STACK_TRACE, FRAGMENT_GALLERY);
                    unbinder.unbind();
                    finish();
                }
            });

            //imagedes_tv_desc.setText(imageTitle);
            imagedes_iv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareImage();
                }
            });

        }

    }

    private void shareImage() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        Uri screenshotUri = Uri.parse(imageURL);
        sharingIntent.setType("image/png");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        startActivity(Intent.createChooser(sharingIntent, "Share image using"));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
