package app.convergenciashow.events.data.model.request;

public class CanBeReachedByMessageRequest {

    private boolean mark;

    public CanBeReachedByMessageRequest(boolean mark) {
        this.mark = mark;
    }
}
