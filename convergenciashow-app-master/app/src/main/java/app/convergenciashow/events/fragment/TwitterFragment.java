package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ConvergenciaActivity;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ConvergenciaResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;

import app.convergenciashow.events.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.convergenciashow.events.data.service.ServiceManger.getStreamingService;
import static com.facebook.FacebookSdk.getApplicationContext;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class TwitterFragment extends Fragment {

    @BindView(R.id.wv_twitter)
    WebView webView;

    private String url;
    private Unbinder unbinder;
    private AlertBuilderView progressBuilder, alertBuilderView;

    public TwitterFragment() {
    }

    public static TwitterFragment newInstance() {
        TwitterFragment fragment = new TwitterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invokeActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twitter, container, false);
        unbinder = ButterKnife.bind(this, view);
        progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
        invokeActionBar();
        progressBuilder.show();
        getTwitter();
        return view;
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

    private void invokeActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar_fragment, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        AppCompatImageView title = activity.findViewById(R.id.action_bar_return_string);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(MenuEventFragment.newInstance());
            }
        });
    }

    protected void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    public void getTwitter() {
        Call<ConvergenciaResponse> callResponse = getStreamingService().getConvergenciaInfo("TWITTER");
        callResponse.enqueue(new retrofit2.Callback<ConvergenciaResponse>() {
            @Override
            public void onResponse(Call<ConvergenciaResponse> call, final Response<ConvergenciaResponse> response) {
                if (response.code() == HTTP_OK) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            url = response.body().getDescription();
                            loadWebView();
                        }
                    });
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    progressBuilder.dismiss();
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            AuthRepository authRepository = new AuthRepository(getApplicationContext(), getActivity());
                            authRepository.logOut();
                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                        }
                    });
                } else {
                    progressBuilder.dismiss();
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), getApplicationContext());
                    alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription());
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            openFragment(MenuEventFragment.newInstance());
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ConvergenciaResponse> call, Throwable t) {
                alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error));
                alertBuilderView.show();
                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        openFragment(MenuEventFragment.newInstance());
                    }
                });
            }
        });
    }

    private void loadWebView(){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new Callback());
        webView.loadUrl(url);

        if (Build.VERSION.SDK_INT >= 19)
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        else
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                //progressBuilder.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBuilder.dismiss();
            }
        });

    }


}
