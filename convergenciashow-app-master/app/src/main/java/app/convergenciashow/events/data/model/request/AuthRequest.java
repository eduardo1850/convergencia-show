package app.convergenciashow.events.data.model.request;

public class AuthRequest {

    private String email;
    private String password;
    private String so;
    private String device;

    public AuthRequest(String email, String password, String so, String device) {
        this.email = email;
        this.password = password;
        this.so = so;
        this.device = device;
    }

}
