package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.domain.Speaker;
import app.convergenciashow.events.fragment.SpeakersFragment;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SpeakersAdapter extends RecyclerView.Adapter<SpeakersAdapter.ViewHolder> implements Filterable {

    private ArrayList<Speaker> items;
    private ArrayList<Speaker> itemsFiltered;
    private SpeakersFragment fragment;
    private Unbinder unbinder;
    private Long idUser;
    private Activity activity;

    public SpeakersAdapter(ArrayList<Speaker> items, SpeakersFragment fragment, Long idUser) {
        this.idUser = idUser;
        this.items = items;
        this.fragment = fragment;
        this.itemsFiltered = items;
    }

    public void update(ArrayList<Speaker> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.visitante_item, viewGroup, false);
        SpeakersAdapter.ViewHolder vh = new SpeakersAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SpeakersAdapter.ViewHolder viewHolder, int i) {
        final Speaker item = itemsFiltered.get(i);
        activity = fragment.getActivity();

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.default_user);
        requestOptions.error(R.drawable.default_user);
        Glide.with(activity)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getSpeakersImageUrl(item.getId(), item.getImageVersion()))
                .into(viewHolder.visitanteImage);

        if (!idUser.equals(item.getId()))
            viewHolder.visitanteName.setText(item.getName() + " " + item.getLastName());
        else
            viewHolder.visitanteName.setText(item.getName() + " " + item.getLastName() + " (" + activity.getString(R.string.you) + ")");


        viewHolder.visitanteEmpresa.setText(Utils.getDescription(item));
        viewHolder.contactImage.setVisibility(View.GONE);


        viewHolder.speaker_item_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.gotoSpeakerDetail(item.getId(), activity);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemsFiltered = items;
                } else {
                    ArrayList<Speaker> filteredList = new ArrayList<>();
                    for (Speaker row : items) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    itemsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemsFiltered = (ArrayList<Speaker>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.speaker_item_info)
        View speaker_item_info;
        @BindView(R.id.visitanteImage)
        ImageView visitanteImage;
        @BindView(R.id.visitanteName)
        TextView visitanteName;
        @BindView(R.id.visitanteEmpresa)
        TextView visitanteEmpresa;
        @BindView(R.id.contactImage)
        ImageView contactImage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

}
