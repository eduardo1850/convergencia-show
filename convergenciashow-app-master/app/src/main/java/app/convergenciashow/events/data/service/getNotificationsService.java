package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.GetNotificationsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getNotificationsService {

    @GET("general/get-notifications-mobile")
    Call<GetNotificationsResponse> listNotifications(
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows);
}
