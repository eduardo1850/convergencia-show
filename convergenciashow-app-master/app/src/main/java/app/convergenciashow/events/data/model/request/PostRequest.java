package app.convergenciashow.events.data.model.request;

public class PostRequest {
    private Long idEvent;
    private String description;

    public PostRequest(Long idEvent, String description) {
        this.setIdEvent(idEvent);
        this.setDescription(description);
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
