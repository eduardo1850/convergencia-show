package app.convergenciashow.events.data.repository;

import android.content.Context;

import app.convergenciashow.events.data.model.response.AsistentesResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.UserResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.User;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AsistentesCallBack;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getAsistentesService;

public class AsistentesRepository {

    private Context context;

    public AsistentesRepository(Context context) {
        this.context = context;
    }


    public void getAsistentes(AuthUser user, String idEvent, final AsistentesCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<AsistentesResponse> callResponse = getAsistentesService().list(userHeader, idEvent);
        callResponse.enqueue(new Callback<AsistentesResponse>() {
            @Override
            public void onResponse(Call<AsistentesResponse> call, Response<AsistentesResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<AsistentesResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    private ArrayList<User> transform(AsistentesResponse response) {
        ArrayList<UserResponse> usersResponses = response.getUsers();
        ArrayList<User> users = new ArrayList<>();
        for (UserResponse user : usersResponses) {
            User u = new User();
            u.setCanBeReached(user.isCanBeReached());
            u.setCompanyName(user.getCompanyName());
            u.setHasPicture(user.isHasPicture());
            u.setId(user.getId());
            u.setImageVersion(user.getImageVersion());
            u.setSpeaker(user.isSpeaker());
            u.setLastName(user.getLastName());
            u.setName(user.getName());
            u.setRole(user.getRole());
            users.add(u);
        }

        return users;
    }

}
