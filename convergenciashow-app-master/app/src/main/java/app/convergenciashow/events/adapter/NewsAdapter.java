package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.NewsActivity;
import app.convergenciashow.events.domain.New;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private ArrayList<New> items;
    private Context context;
    private Activity activity;

    public NewsAdapter(ArrayList<New> items, Activity activity, Context context) {
        this.items = items;
        this.activity = activity;
        this.context = context;
    }

    public void update(ArrayList<New> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.ViewHolder viewHolder, int i) {
        final New n = items.get(i);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.picture);
        requestOptions.error(R.drawable.placeholderpicture);

        viewHolder.authorName.setText(n.getOwnerName());
        viewHolder.newTitle.setText(n.getTitle());
        viewHolder.date.setText(Utils.timeIsRunningOut(n.getDate(), context));
        viewHolder.newsContent.setText(n.getDescription());
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getAuthorImageUrl(n.getIdOwner(), 0)).into(viewHolder.authorImage);
        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getNewsImageUrl(n.getId(), n.getImageVersion())).into(viewHolder.newsImage);

        viewHolder.ll_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewsActivity.class);
                intent.putExtra("new_data", n);
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_new;
        TextView authorName;
        TextView date;
        ImageView authorImage;
        ImageView newsImage;
        TextView newsContent;
        TextView newTitle;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ll_new = itemView.findViewById(R.id.ll_new);
            authorImage = itemView.findViewById(R.id.authorImage);
            authorName = itemView.findViewById(R.id.authorName);
            date = itemView.findViewById(R.id.date);
            newsImage = itemView.findViewById(R.id.newsImage);
            newsContent = itemView.findViewById(R.id.newsContent);
            newTitle = itemView.findViewById(R.id.newTitle);
        }
    }
}
