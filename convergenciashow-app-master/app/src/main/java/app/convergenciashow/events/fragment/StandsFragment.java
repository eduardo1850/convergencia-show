package app.convergenciashow.events.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.StandsAdapter;
import app.convergenciashow.events.domain.Plano;
import app.convergenciashow.events.domain.Stand;

public class StandsFragment extends BaseReturnFragment {

    private static final String PLANO = "Plano";
    private ArrayList<Stand> stands;
    private StandsAdapter adapter;
    private LinearLayout llstands;
    private Plano plano;
    private RecyclerView standsRecyclerView;
    private EditText standsSearch;

    public StandsFragment() {
    }

    public static StandsFragment newInstance(Plano param1) {
        StandsFragment fragment = new StandsFragment();
        Bundle args = new Bundle();
        args.putParcelable(PLANO, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(getString(R.string.back));
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            plano = getArguments().getParcelable(PLANO);
            stands = plano.getStands();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stands, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setComponent() {
        adapter = new StandsAdapter(new ArrayList<Stand>(), getContext());
        llstands = getActivity().findViewById(R.id.llstands);
        standsSearch = getActivity().findViewById(R.id.standsSearch);
        standsRecyclerView = getActivity().findViewById(R.id.standsRv);
        if (stands.size() > 0) {
            standsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            standsRecyclerView.setAdapter(adapter);
            llstands.setVisibility(View.GONE);
            standsRecyclerView.setVisibility(View.VISIBLE);
            adapter.update(stands);
        } else {
            llstands.setVisibility(View.VISIBLE);
            standsRecyclerView.setVisibility(View.GONE);
        }

        standsSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });

    }
}
