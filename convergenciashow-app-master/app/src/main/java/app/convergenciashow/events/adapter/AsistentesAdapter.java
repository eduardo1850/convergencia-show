package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ConversationActivity;
import app.convergenciashow.events.domain.User;
import app.convergenciashow.events.fragment.AsistentesFragment;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static app.convergenciashow.events.util.Constants.FRAGMENT_ASSISTANTS;

public class AsistentesAdapter extends RecyclerView.Adapter<AsistentesAdapter.ViewHolder> implements Filterable {

    private ArrayList<User> items;
    private ArrayList<User> itemsFiltered;
    private AsistentesFragment fragment;
    private static final String JPG = ".jpg";
    private static final String VER = "?v=";
    private Unbinder unbinder;
    private Long idUser;
    private Activity activity;

    public AsistentesAdapter(ArrayList<User> items, AsistentesFragment fragment, Long idUser) {
        this.idUser = idUser;
        this.items = items;
        this.fragment = fragment;
        this.itemsFiltered = items;
    }

    public void update(ArrayList<User> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.visitante_item, viewGroup, false);
        AsistentesAdapter.ViewHolder vh = new AsistentesAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AsistentesAdapter.ViewHolder viewHolder, int i) {
        final User item = itemsFiltered.get(i);
        activity = fragment.getActivity();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.default_user);
        requestOptions.error(R.drawable.default_user);
        if (item.isHasPicture()) {
            Glide.with(activity)
                    .setDefaultRequestOptions(requestOptions)
                    .load(getProfileImageUrl(item.getId(), item.getImageVersion()))
                    .into(viewHolder.visitanteImage);
        } else {
            Glide.with(activity).load(R.drawable.default_user).into(viewHolder.visitanteImage);
        }

        if (!idUser.equals(item.getId()))
            viewHolder.visitanteName.setText(item.getName() + " " + item.getLastName());
        else
            viewHolder.visitanteName.setText(item.getName() + " " + item.getLastName() + " (" + activity.getString(R.string.you) + ")");

       /* switch (item.getUserType()) {
            case 1:
                typeImage = R.drawable.speaker_visitor;
                typeString = activity.getString(R.string.participante);
                break;
            case 2:
                typeImage = R.drawable.speaker_speaker;
                typeString = activity.getString(R.string.ponente);
                break;
            case 3:
                typeImage = R.drawable.speaker_moderator;
                typeString = activity.getString(R.string.moderador);
                break;
            case 4:
                typeImage = R.drawable.speaker_relator;
                typeString = activity.getString(R.string.relator);
                break;
            case 5:
                typeImage = R.drawable.speaker_special;
                typeString = activity.getString(R.string.invitado_Especial);
                break;
            case 6:
                typeImage = R.drawable.speaker_helper;
                typeString = activity.getString(R.string.staff);
                break;
            case 7:
                typeImage = R.drawable.speaker_volunteer;
                typeString = activity.getString(R.string.voluntario);
                break;
            case 8:
                typeImage = R.drawable.speaker_expositor;
                typeString = activity.getString(R.string.expositor);
                break;
            case 9:
                typeImage = R.drawable.speaker_provider;
                typeString = activity.getString(R.string.proveedor);
                break;

            default:
                typeImage = R.drawable.speaker_panelist;
                typeString = "";
                break;
        }*/

        //viewHolder.visitanteType.setText(typeString);
        viewHolder.visitanteEmpresa.setText(Utils.getDescription(item));
        //Glide.with(activity).load(typeImage).into(viewHolder.typeImage);

        if (!idUser.equals(item.getId())) {
            if (item.isCanBeReached()) {
                Glide.with(activity).load(R.drawable.ic_chat).into(viewHolder.contactImage);
                viewHolder.contactImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppPreferences.getInstance(activity).saveIntData(Constants.STACK_TRACE, FRAGMENT_ASSISTANTS);
                        Intent intent = new Intent(activity, ConversationActivity.class);
                        intent.putExtra(Constants.IDUSERTOCONVERSE, item.getId());
                        intent.putExtra(Constants.NAMEUSERTOCONVERSE, item.getName() + " " + item.getLastName());
                        intent.putExtra(Constants.IMAGEUSERTOCONVERSE, getProfileImageUrl(item.getId(), item.getImageVersion()));
                        activity.startActivity(intent);
                    }
                });
            }
        }

        viewHolder.visitanteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.gotoUserDetail(item.getId(), activity);
            }
        });

        viewHolder.visitanteName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.gotoUserDetail(item.getId(), activity);
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemsFiltered = items;
                } else {
                    ArrayList<User> filteredList = new ArrayList<>();
                    for (User row : items) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    itemsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemsFiltered = (ArrayList<User>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.visitanteImage)
        ImageView visitanteImage;
        @BindView(R.id.visitanteName)
        TextView visitanteName;
        @BindView(R.id.visitanteEmpresa)
        TextView visitanteEmpresa;
        @BindView(R.id.contactImage)
        ImageView contactImage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    private String getProfileImageUrl(Long profileId, int version) {
        return Constants.IMAGE_PROFILE_PATH + profileId + JPG + VER + version;
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


}
