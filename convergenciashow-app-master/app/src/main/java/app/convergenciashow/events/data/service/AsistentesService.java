package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.AsistentesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface AsistentesService {

    @GET("users/get-users-in-event")
    Call<AsistentesResponse> list(
            @Header("auth") String headerAuth,
            @Query("idEvent") String idEvent);

}