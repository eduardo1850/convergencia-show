package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class GetMessagesResponse extends BaseResponse {

    private ArrayList<MessagesResponse> messages;

    public ArrayList<MessagesResponse> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<MessagesResponse> messages) {
        this.messages = messages;
    }

}
