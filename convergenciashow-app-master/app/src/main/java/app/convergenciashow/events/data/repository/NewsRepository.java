package app.convergenciashow.events.data.repository;

import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.New;
import app.convergenciashow.events.data.model.response.NewsResponse;
import app.convergenciashow.events.data.model.response.NewResponse;
import app.convergenciashow.events.util.WSCallbacks.NewsCallBack;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.convergenciashow.events.data.service.ServiceManger.getNewsService;

public class NewsRepository {

    public void getNews(AuthUser user, Long loadedElements, Integer numberOfRows, final NewsCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<NewsResponse> callResponse = getNewsService().listNews(userHeader,loadedElements, numberOfRows);
        callResponse.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                cb.onSuccess(transform(response.body()));
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                cb.onError(t);
            }
        });
    }

    private ArrayList<New> transform(NewsResponse response) {
        ArrayList<NewResponse> newsResponses = response.getNews();
        ArrayList<New> news = new ArrayList<>();
        for(NewResponse nr : newsResponses){
            New n = new New();
            n.setDate(nr.getDate());
            n.setDescription(nr.getDescription());
            n.setHasPicture(nr.isHasPicture());
            n.setId(nr.getId());
            n.setIdOwner(nr.getIdOwner());
            n.setImageVersion(nr.getImageVersion());
            n.setOwnerName(nr.getOwnerName());
            n.setTitle(nr.getTitle());
            news.add(n);
        }

        return news;
    }

}
