package app.convergenciashow.events.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Pictures implements Comparable<Pictures> {

    private Long id;
    private String title;
    private Integer imageVersion;
    private String uploadDate;
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(Integer imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String uploadDate) {
        try {
            this.date = new SimpleDateFormat("yyyy-MM-dd").parse(uploadDate);
        } catch (Exception e) {
            this.date = new Date();
        }
    }

    @Override
    public int compareTo(Pictures o) {
        if (getDate() == null || o.getDate() == null)
            return 0;
        return getDate().compareTo(o.getDate());
    }


}
