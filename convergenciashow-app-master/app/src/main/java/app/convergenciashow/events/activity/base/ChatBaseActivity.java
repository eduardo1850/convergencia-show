package app.convergenciashow.events.activity.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class ChatBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setComponent();
        invokeActionBar();
    }

    protected void invokeActionBar() {
        // implement in activity
    }

    protected void setComponent() {
        //Implement in activity
    }

}
