package app.convergenciashow.events.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.SimpleScannerActivity;
import app.convergenciashow.events.domain.MenuEventItem;
import app.convergenciashow.events.fragment.ForoFragment;
import app.convergenciashow.events.fragment.MenuEventFragment;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

public class MenuEventAdapter extends RecyclerView.Adapter<MenuEventAdapter.ViewHolder> {

    private ArrayList<MenuEventItem> items;
    private MenuEventFragment fragment;

    public MenuEventAdapter(ArrayList<MenuEventItem> items, MenuEventFragment fragment) {
        this.items = items;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public MenuEventAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_event_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuEventAdapter.ViewHolder viewHolder, int i) {
        final MenuEventItem item = items.get(i);
        viewHolder.itemName.setText(item.getName());
        viewHolder.itemImage.setImageResource(items.get(i).getImage());

        viewHolder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getActivity() == null) {
                    if (item.getFragment() instanceof ForoFragment) {
                        ArrayList<String> permissions = new ArrayList<>();
                        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        if (!Utils.requestPermissions(fragment.getActivity(), permissions))
                            fragment.openFragment(item.getFragment());
                    } else {
                        fragment.openFragment(item.getFragment());
                    }
                } else {
                    if (item.getActivity().getSimpleName().trim().equals("SimpleScannerActivity")) {
                        if (ContextCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(fragment.getActivity(), new String[]{Manifest.permission.CAMERA}, Constants.ZBAR_CAMERA_PERMISSION);
                        } else {
                            Utils.invokeActivity(fragment.getActivity(), item.getActivity(), false);
                        }
                    } else {
                        Utils.invokeActivity(fragment.getActivity(), item.getActivity(), false);
                    }
                }

            }
        });
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemName);
            itemImage = itemView.findViewById(R.id.itemImage);
        }
    }
}
