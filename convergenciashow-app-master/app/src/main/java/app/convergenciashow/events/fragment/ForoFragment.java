package app.convergenciashow.events.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.ForoAdapter;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.ForoRepository;
import app.convergenciashow.events.domain.Post;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.WSCallbacks.ForoCallBack;

public class ForoFragment extends ReturnAddBaseFragment {

    private RecyclerView foroRecyclerView;
    private LinearLayout llforum;
    private AuthRepository authRepository;
    private ForoRepository repository;
    private ForoAdapter adapter;
    private Long loadedElements = 0L;
    private static final int rows = 8;
    private int rvLastVisibleItem;
    private boolean loading = false;

    public static ForoFragment newInstance() {
        ForoFragment fragment = new ForoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setReturnString("< " + getString(R.string.backto_event));
        setTitleString(getString(R.string.app_forum));
        super.onCreate(savedInstanceState);
        //setFragmentToShow(10);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        authRepository = new AuthRepository(getContext(), getActivity());
        repository = new ForoRepository(getActivity());
        loadElements();
        return inflater.inflate(R.layout.fragment_foro, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void setComponent() {
        adapter = new ForoAdapter(new ArrayList<Post>(), this, authRepository);
        llforum = getActivity().findViewById(R.id.llforum);
        foroRecyclerView = getActivity().findViewById(R.id.foroRv);
        foroRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        foroRecyclerView.setAdapter(adapter);

        foroRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) foroRecyclerView.getLayoutManager();
                rvLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!loading && rvLastVisibleItem == foroRecyclerView.getChildCount()) {
                    loading = true;
                    loadElements();
                }
            }
        });
    }

    public void loadElements() {
        String idEvent = AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT) + "";
        repository.getPost(authRepository.getAuth(), idEvent, loadedElements, rows, new ForoCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<Post> value) {
                if (value.size() > 0) {
                    llforum.setVisibility(View.GONE);
                    foroRecyclerView.setVisibility(View.VISIBLE);
                    foroRecyclerView.removeAllViews();
                    adapter.update(value);
                    loadedElements = loadedElements + rows;
                    loading = false;
                } else {
                    if (foroRecyclerView.getChildCount() == 0){
                        llforum.setVisibility(View.VISIBLE);
                        foroRecyclerView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {

            }
        });
    }


}
