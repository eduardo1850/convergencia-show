package app.convergenciashow.events.data.repository;

import android.content.Context;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.PlanoResponse;
import app.convergenciashow.events.data.model.response.StandsResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Plano;
import app.convergenciashow.events.domain.Stand;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.PlanoCallBack;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getPlanoService;

public class PlanoRepository {

    private Context context;

    public PlanoRepository(Context context){
        this.context = context;
    }

    public void getPlano(AuthUser user, Long idEvent, final PlanoCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<PlanoResponse> callResponse = getPlanoService().getPlano(userHeader, idEvent);
        callResponse.enqueue(new Callback<PlanoResponse>() {
            @Override
            public void onResponse(Call<PlanoResponse> call, Response<PlanoResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<PlanoResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    private Plano transform(PlanoResponse response) {
        Plano plano = new Plano();
        ArrayList<Stand> stands = new ArrayList<>();

        plano.setMapAvailable(response.getMapAvailable());
        plano.setMapVersion(response.getMapVersion());

        for (StandsResponse stand : response.getStands()) {
            Stand s = new Stand();

            s.setDescription(stand.getDescription());
            s.setExhibitor(stand.getExhibitor());
            s.setId(stand.getId());
            s.setKey(stand.getKey());

            stands.add(s);
        }

        plano.setStands(stands);

        return plano;
    }
}
