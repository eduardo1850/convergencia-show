package app.convergenciashow.events.data.model.request;

public class AuthRequestLinkedIn {

    private String linkedinId;
    private String linkedinToken;
    private String email;
    private String firstName;
    private String lastName;
    private String so;
    private String device;

    public AuthRequestLinkedIn(String linkedinId, String linkedinToken, String email, String firstName, String lastName, String so, String device) {
        this.linkedinId = linkedinId;
        this.linkedinToken = linkedinToken;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.so = so;
        this.device = device;
    }
}
