package app.convergenciashow.events.activity;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.CancelableActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.GetDetailsOfUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.DetailsOfUserCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.util.Constants.FRAGMENT_ASSISTANTS;
import static app.convergenciashow.events.util.Constants.IDEVENT;
import static app.convergenciashow.events.util.Constants.IDUSERTOCONVERSE;

public class GetUserDetailActivity extends CancelableActivity implements View.OnClickListener {

    @BindView(R.id.details_civ_image)
    CircleImageView civ_image;
    @BindView(R.id.details_tv_email)
    TextView tv_email;
    @BindView(R.id.details_tv_name)
    TextView tv_name;
    @BindView(R.id.details_tv_info)
    TextView tv_info;
    @BindView(R.id.details_tv_description)
    TextView tv_description;

    private Unbinder unbinder;
    private AuthRepository repository;
    private Long userId, eventId;
    private AlertBuilderView alertBuilderView, progressBuilder;
    private String codeNumber;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_user_speaker_detail);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void setComponent() {
        this.activity = this;
        if (getIntent().getExtras() != null) {
            userId = getIntent().getExtras().getLong(IDUSERTOCONVERSE);
            eventId = getIntent().getExtras().getLong(IDEVENT);
            if ((userId != null) && (eventId != null)) {
                progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
                progressBuilder.show();
                repository.getDetailsOfUser(userId, eventId, new DetailsOfUserCallBack() {
                    @Override
                    public void onSuccess(@NonNull GetDetailsOfUser response) {

                        AppPreferences.getInstance(activity).saveIntData(Constants.STACK_TRACE, FRAGMENT_ASSISTANTS);
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.placeholder(R.drawable.picture);
                        requestOptions.error(R.drawable.default_user);
                        Glide.with(activity)
                                .setDefaultRequestOptions(requestOptions)
                                .load(Utils.getProfileImageUrl(response.getId(), response.getImageVersion()))
                                .into(civ_image);

                        tv_email.setText(response.getEmail());
                        tv_name.setText(response.getName() + " " + response.getLastName());

                        if ((!response.getCompanyName().isEmpty()) && (!response.getRole().isEmpty()))
                            tv_info.setText(response.getCompanyName() + " • " + response.getRole());
                        else if ((response.getCompanyName().isEmpty()) && (!response.getRole().isEmpty()))
                            tv_info.setText(response.getRole());
                        else if ((!response.getCompanyName().isEmpty()) && (response.getRole().isEmpty()))
                            tv_info.setText(response.getCompanyName());
                        else
                            tv_info.setVisibility(View.GONE);

                        tv_description.setText(response.getUserDescription());
                        progressBuilder.dismiss();
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null) {
                            codeNumber = baseResponse.getCodeNumber();
                            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                                alertBuilderView = new AlertBuilderView(activity, GetUserDetailActivity.this, Operations.AlertBuilder_error, getString(R.string.session_error));
                                alertBuilderView.show();
                            } else {
                                alertBuilderView = new AlertBuilderView(activity, GetUserDetailActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription());
                                alertBuilderView.show();
                            }
                        } else {
                            alertBuilderView = new AlertBuilderView(activity, GetUserDetailActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error));
                            alertBuilderView.show();
                        }
                    }
                });
            }
        } else {
            finish();
        }
    }

    @Override
    protected void initRepository() {
        repository = new AuthRepository(getApplicationContext(), this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.alert_btn_ok:
                if (codeNumber.equals(String.valueOf(HTTP_UNAUTHORIZED)))
                    repository.logOut();
                finish();
                break;
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
