package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.Speaker;

public interface SpeakersCallBack {

    void onSuccess(@NonNull ArrayList<Speaker> value);

    void onError(@NonNull BaseResponse baseResponse);
}
