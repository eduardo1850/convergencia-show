package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.NewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface NewsService {

    @GET("news/get-news-mobile")
    Call<NewsResponse> listNews(
            @Header("auth") String headerAuth,
            @Query("numOfLoadedElements") Long numOfLoadedElements,
            @Query("numberOfRows") Integer numberOfRows);
}