package app.convergenciashow.events.util;

public enum Operations {

    /**
     * AlertBuilder success operation
     **/
    AlertBuilder_success,
    /**
     * AlertBuilder success operation double button
     **/
    AlertBuilder_success_double,
    /**
     * AlertBuilder error operation
     **/
    AlertBuilder_error,
    /**
     * AlertBuilder Image
     **/
    AlertBuilder_image,
    /**
     * AlertBuilder error operation
     **/
    AlertBuilder_error_double,
    /**
     * AlertBuilder progress
     **/
    AlertBuilder_progress,
    /**
     * AlertBuilder linkedIn
     */
    AlertBuilder_linkedin;

}
