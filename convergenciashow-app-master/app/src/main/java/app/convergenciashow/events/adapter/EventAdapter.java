package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.MenuContainerActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.domain.Event;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private ArrayList<Event> events;
    private Context context;
    private Activity activity;
    private AlertBuilderView progressBuilder;
    private Unbinder unbinder;

    public EventAdapter(ArrayList<Event> items, Activity activity) {
        this.events = items;
        this.context = activity.getApplicationContext();
        this.activity = activity;
        this.progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
    }

    @NonNull
    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_item, viewGroup, false);
        ViewHolder vh = new ViewHolder(v, viewType);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final EventAdapter.ViewHolder viewHolder, final int i) {

        final Event event = events.get(i);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.glideplaceholder);
        requestOptions.error(R.drawable.placeholderpicture);
        Glide.with(activity)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getEventImageUrl(event.getId(), event.getImageVersion()))
                .into(viewHolder.event_logo);


        viewHolder.event_title.setText(event.getName());

        if (Utils.getParseForumDate(event.getDateInit()).contains(",") && Utils.getParseForumDate(event.getDateEnd()).contains(",")) {
            String[] initDate = Utils.getParseForumDate(event.getDateInit()).split(",");
            String[] endDate = Utils.getParseForumDate(event.getDateEnd()).split(",");
            viewHolder.event_data.setText(initDate[0] + " - " + endDate[0] + " • " + event.getAddress());
        } else {
            String[] initDate = event.getDateInit().split(" ");
            String[] endDate = event.getDateEnd().split(" ");
            viewHolder.event_data.setText(initDate[0] + " - " + endDate[0] + " • " + event.getAddress());
        }

        viewHolder.event_description.setText(event.getDescription());

        viewHolder.event_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppPreferences.getInstance(activity).saveLongData(Constants.ID_EVENT, event.getId());
                Utils.invokeActivity(activity, MenuContainerActivity.class, true);
            }
        });


    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_event_item)
        View event_item;
        @BindView(R.id.event_iv_logo)
        CircleImageView event_logo;
        @BindView(R.id.event_tv_title)
        TextView event_title;
        @BindView(R.id.event_tv_data)
        TextView event_data;
        @BindView(R.id.event_tv_description)
        TextView event_description;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    public void update() {
        notifyDataSetChanged();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


}
