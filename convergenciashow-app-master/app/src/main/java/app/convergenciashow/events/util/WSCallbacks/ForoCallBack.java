package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.domain.Post;

public interface ForoCallBack {

    void onSuccess(@NonNull ArrayList<Post> value);

    void onError(@NonNull Throwable throwable);
}
