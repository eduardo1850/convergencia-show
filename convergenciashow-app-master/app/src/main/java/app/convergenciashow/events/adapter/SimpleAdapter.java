package app.convergenciashow.events.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.FullScreenViewActivity;
import app.convergenciashow.events.domain.Pictures;
import app.convergenciashow.events.util.Utils;

import java.util.ArrayList;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

    private final Context mContext;
    private ArrayList<Object> pictures;

    public SimpleAdapter(Context context, ArrayList<Object> _Pictures) {
        mContext = context;
        this.pictures = _Pictures;
    }

    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.gallery_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, final int position) {
        final Pictures picture = (Pictures) pictures.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.picture);
        requestOptions.error(R.drawable.picture);
        Glide.with(mContext)
                .setDefaultRequestOptions(requestOptions)
                .load(Utils.getGalleryImageUrl(true, picture.getId(), picture.getImageVersion()))
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, FullScreenViewActivity.class);
                i.putExtra("position", position);
                String json = new Gson().toJson(pictures);
                i.putExtra("pictures", json);
                mContext.startActivity(i);

                /*Intent intent = new Intent(mContext, FullScreenViewActivity.class);
                intent.putExtra(Constants.PICTUREURL, Utils.getGalleryImageUrl(false, picture.getId(), picture.getImageVersion()));
                intent.putExtra(Constants.PICTUREDECRIPTION, picture.getTitle());
                mContext.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        if (pictures != null)
            return pictures.size();
        else
            return 0;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final ImageView image;

        public SimpleViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.gallery_iv_item);
        }
    }

}