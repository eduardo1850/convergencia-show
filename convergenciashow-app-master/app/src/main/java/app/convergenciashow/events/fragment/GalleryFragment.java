package app.convergenciashow.events.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.adapter.SectionedGridRecyclerViewAdapter;
import app.convergenciashow.events.adapter.SimpleAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.domain.GallerySections;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GalleryCallBack;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class GalleryFragment extends Fragment {

    @BindView(R.id.galleryRv)
    RecyclerView galleryRecyclerView;
    @BindView(R.id.llgallery)
    LinearLayout llgallery;

    private EventRepository eventRepository;
    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;
    private int COLUMNS = 3;

    public static GalleryFragment newInstance() {
        GalleryFragment fragment = new GalleryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invokeActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        eventRepository = new EventRepository(getContext());
        authRepository = new AuthRepository(getContext(), getActivity());
        progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
        progressBuilder.show();

        eventRepository.getGallery(authRepository.getAuth(), AppPreferences.getInstance(getContext()).getLongData(Constants.ID_EVENT), new GalleryCallBack() {
            @Override
            public void onSuccess(@NonNull ArrayList<Object> pictures) {
                progressBuilder.dismiss();
                if (pictures.size() > 0) {
                    llgallery.setVisibility(View.GONE);
                    galleryRecyclerView.setVisibility(View.VISIBLE);

                    galleryRecyclerView.setHasFixedSize(true);
                    galleryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), COLUMNS));

                    //Your RecyclerView.Adapter
                    //SimpleAdapter mAdapter = new SimpleAdapter(this);

                    ArrayList<GallerySections> sections = (ArrayList<GallerySections>) pictures.get(pictures.size() - 1);
                    pictures.remove(pictures.size() - 1);
                    //ArrayList<Pictures> pics = (ArrayList<Pictures>) pictures.get(0);

                    SimpleAdapter mAdapter = new SimpleAdapter(getContext(), pictures);
                    List<SectionedGridRecyclerViewAdapter.Section> sectionsAdapter = new ArrayList<SectionedGridRecyclerViewAdapter.Section>();
                    //Sections
                    for (GallerySections s : sections)
                        sectionsAdapter.add(new SectionedGridRecyclerViewAdapter.Section(s.getElements(), s.getDate()));

                    //Add your adapter to the sectionAdapter
                    SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
                    SectionedGridRecyclerViewAdapter mSectionedAdapter =
                            new SectionedGridRecyclerViewAdapter(getContext(),
                                    R.layout.gallery_date_item, R.id.gallery_date, galleryRecyclerView, mAdapter);
                    mSectionedAdapter.setSections(sectionsAdapter.toArray(dummy));

                    //Apply this adapter to the RecyclerView
                    galleryRecyclerView.setAdapter(mSectionedAdapter);


                    //GalleryAdapter adapter = new GalleryAdapter(pictures, GalleryFragment.this);
                    //galleryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), COLUMNS));
                    //galleryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    //galleryRecyclerView.setAdapter(adapter);
                } else {
                    llgallery.setVisibility(View.VISIBLE);
                    galleryRecyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                progressBuilder.dismiss();
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        if (isAdded()) {
                            AlertBuilderView alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                            alertBuilderView.show();
                            alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    authRepository.logOut();
                                    Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                                }
                            });
                        }
                    } else {
                        if (isAdded())
                            new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    if (isAdded())
                        new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });


        return view;

    }

    private void invokeActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_return_action_bar_fragment, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.NO_GRAVITY
                )
        );
        AppCompatImageView title = activity.findViewById(R.id.action_bar_return_string);
        //title.setText(getActivity().getText(R.string.back));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(MenuEventFragment.newInstance());
            }
        });
    }

    protected void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

}
