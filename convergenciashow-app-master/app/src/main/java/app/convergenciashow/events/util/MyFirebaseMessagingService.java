package app.convergenciashow.events.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ConversationActivity;
import app.convergenciashow.events.data.model.response.MessagesResponse;

import static app.convergenciashow.events.activity.ConversationActivity.activity;
import static app.convergenciashow.events.activity.ConversationActivity.chatActive;
import static app.convergenciashow.events.util.Constants.IMAGE_PROFILE_PATH;
import static app.convergenciashow.events.util.Constants.JPG;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String TAG = "TAG";
    private static String RECEIVING_MESSAGE = "RECEIVING_MESSAGE";
    private static String IDUSER = "idUser";
    private static String IDMESSAGE = "idMessage";
    private static String NAME = "name";
    private static String TYPE = "type";
    private static String NOTREAD = "notRead";

    public MyFirebaseMessagingService() {
    }

    private void sendNotification(Intent intent, String title, String body) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "M_CH_ID");

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_notification);
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_notification);
        }

        /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_stat_name);
            notificationBuilder.setColor(getResources().getColor(R.color.orange));
        } else {
            notificationBuilder.setSmallIcon(R.drawable.ic_stat_name);
        }*/
        notificationBuilder
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            if (remoteMessage.getData().get(TYPE).trim().equals(RECEIVING_MESSAGE)) {
                if (!chatActive) {
                    Intent intent = new Intent(this, ConversationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Constants.IDUSERTOCONVERSE, Long.parseLong(remoteMessage.getData().get(IDUSER)));
                    intent.putExtra(Constants.NAMEUSERTOCONVERSE, remoteMessage.getData().get(NAME));
                    intent.putExtra(Constants.IMAGEUSERTOCONVERSE, getProfileImage(remoteMessage.getData().get(IDUSER)));
                    sendNotification(intent, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
                } else {
                    if (activity != null) {
                        MessagesResponse message = new MessagesResponse(
                                Long.parseLong(remoteMessage.getData().get(IDMESSAGE)),
                                Long.parseLong(remoteMessage.getData().get(IDUSER)),
                                remoteMessage.getNotification().getBody(),
                                Utils.formatDate(Constants.MESSAGES_DATE_FORMAT, new Date()));
                        ((ConversationActivity) activity).updateMessages(message);
                    }

                }

            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private String getProfileImage(String userId) {
        return IMAGE_PROFILE_PATH + userId + JPG;
    }
}
