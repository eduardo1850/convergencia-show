package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.GetDetailsOfSpeaker;

public interface DetailsOfSpeakerCallBack {

    void onSuccess(@NonNull GetDetailsOfSpeaker response);

    void onError(@NonNull BaseResponse baseResponse);

}
