package app.convergenciashow.events.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import app.convergenciashow.events.BuildConfig;
import app.convergenciashow.events.R;
import app.convergenciashow.events.adapter.FullScreenImageAdapter;
import app.convergenciashow.events.domain.Pictures;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.WSCallbacks.GalleryAdapterCallback;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static app.convergenciashow.events.util.Constants.FRAGMENT_GALLERY;

public class FullScreenViewActivity extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.btnClose)
    AppCompatImageView close;
    @BindView(R.id.imagedes_iv_share)
    AppCompatImageView imagedes_iv_share;
    @BindView(R.id.textDisplay)
    TextView textDisplay;

    private Unbinder unbinder;
    private ImageView imageView_;
    private FullScreenImageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);
        unbinder = ButterKnife.bind(this);
        if (getIntent() != null) {
            String data = getIntent().getStringExtra("pictures");
            ArrayList<Pictures> pictures = new Gson().fromJson(data, new TypeToken<ArrayList<Pictures>>() {
            }.getType());
            adapter = new FullScreenImageAdapter(pictures, this, new GalleryAdapterCallback() {
                @Override
                public void updateNumber(String text, ImageView imageView) {
                    imageView_ = imageView;
                    textDisplay.setText(text);
                }
            });
            viewPager.setAdapter(adapter);
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppPreferences.getInstance(FullScreenViewActivity.this).saveIntData(Constants.STACK_TRACE, FRAGMENT_GALLERY);
                unbinder.unbind();
                finish();
            }
        });

        imagedes_iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareImage();
            }
        });

    }
    
    public void shareImage() {
        // Get access to bitmap image from view
        //ImageView ivImage = (ImageView) findViewById(R.id.ivResult);
        // Get access to the URI for the bitmap
        Uri bmpUri = getLocalBitmapUri(imageView_);
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // Launch sharing dialog for image
            startActivity(Intent.createChooser(shareIntent, "Share Image"));
        } else {
            Toast.makeText(this, "", Toast.LENGTH_LONG).show();
        }
    }
    
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                bmpUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider",file);
            } else {
                bmpUri = Uri.fromFile(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent() != null) {
            int pos = getIntent().getIntExtra("position", 0);
            viewPager.setCurrentItem(pos);
        }
    }

}
