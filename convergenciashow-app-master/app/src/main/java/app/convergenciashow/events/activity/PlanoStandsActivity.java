package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ReturnBaseActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.PlanoRepository;
import app.convergenciashow.events.domain.Plano;
import app.convergenciashow.events.fragment.PlanoFragment;
import app.convergenciashow.events.fragment.StandsFragment;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.PlanoCallBack;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class PlanoStandsActivity extends ReturnBaseActivity {

    private BottomNavigationView navigationView;
    private PlanoRepository repository;
    private AuthRepository authRepository;
    private Plano plano;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_stands);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setComponent() {

        navigationView = findViewById(R.id.navigationView);
        repository = new PlanoRepository(this);
        authRepository = new AuthRepository(getApplicationContext(), this);
        repository.getPlano(authRepository.getAuth(), AppPreferences.getInstance(PlanoStandsActivity.this).getLongData(Constants.ID_EVENT), new PlanoCallBack() {
            @Override
            public void onSuccess(@NonNull Plano value) {
                plano = value;
                navigationView.setSelectedItemId(R.id.action_stands);
                //openFragment(PlanoFragment.newInstance(plano));
                openFragment(StandsFragment.newInstance(plano));
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        AlertBuilderView alertBuilderView = new AlertBuilderView(PlanoStandsActivity.this, Operations.AlertBuilder_error, getString(R.string.session_error));
                        alertBuilderView.show();
                        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                authRepository.logOut();
                                Utils.invokeActivity(PlanoStandsActivity.this, LoginActivity.class, true);
                            }
                        });
                    } else {
                        new AlertBuilderView(PlanoStandsActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    new AlertBuilderView(PlanoStandsActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }
        });

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment selectedFragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_mapa:
                        selectedFragment = PlanoFragment.newInstance(plano);
                        openFragment(selectedFragment);
                        break;
                    case R.id.action_stands:
                        selectedFragment = StandsFragment.newInstance(plano);
                        openFragment(selectedFragment);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
    }
}
