package app.convergenciashow.events.data.model.request;

public class SendAMessageRequest {

    private Long idUser;
    private String message;

    public SendAMessageRequest(Long idUser, String message) {
        this.idUser = idUser;
        this.message = message;
    }
}
