package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class ScheduleResponse extends BaseResponse {
    private ArrayList<ExposResponse> expos;

    public ArrayList<ExposResponse> getExpos() {
        return expos;
    }

    public void setExpos(ArrayList<ExposResponse> expos) {
        this.expos = expos;
    }
}
