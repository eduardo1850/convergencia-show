package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class NewsResponse extends BaseResponse {
    private ArrayList<NewResponse> news;

    public ArrayList<NewResponse> getNews() {
        return news;
    }

    public void setNews(ArrayList<NewResponse> news) {
        this.news = news;
    }
}
