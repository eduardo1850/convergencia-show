package app.convergenciashow.events.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.database.SQLiteHelper;
import app.convergenciashow.events.data.model.request.CanBeReachedByMessageRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.PutUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.logOutBtn)
    Button logOutBtn;
    @BindView(R.id.saveBtn)
    Button saveBtn;
    @BindView(R.id.profileDescription)
    EditText profileUserDescription;
    @BindView(R.id.profileLastName)
    EditText profileUserLastName;
    @BindView(R.id.profileName)
    EditText profileUserName;
    @BindView(R.id.profileRole)
    EditText profileUserRole;
    @BindView(R.id.profileImage)
    ImageView profileImage;
    @BindView(R.id.switchButton)
    Switch switchButton;
    @BindView(R.id.txtEmail)
    TextView txtEmail;

    private static final String TITLE_BAR = "Yo";
    private Bitmap bitmap;
    private boolean isImageTaken = false;
    private Context ctx;
    private AuthRepository repository;
    private AlertBuilderView progressBuilder, alertBuilderView;
    private Unbinder unbinder;
    private SQLiteHelper dbhelper;

    public ProfileFragment() {
        this.ctx = this.getContext();
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    public static ProfileFragment newInstanceArg(Bitmap bitmap) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable("BITMAP", bitmap);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTitleBar(TITLE_BAR);
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setComponent() {
        Activity activity = getActivity();
        repository = new AuthRepository(getContext(), activity);
        dbhelper = new SQLiteHelper(getActivity());
        logOutBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);

        if (repository.getAuth().isCanUserBeReached())
            switchButton.setChecked(true);
        else
            switchButton.setChecked(false);

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    callcanBeReachedByMessage(true);
                } else {
                    callcanBeReachedByMessage(false);
                }
            }
        });

        final PickSetup setup = new PickSetup()
                .setTitle(getString(R.string.choose_text))
                .setCancelText(getString(R.string.cancel_text))
                .setFlip(true)
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera_text))
                .setGalleryButtonText(getString(R.string.gallery_text))
                .setIconGravity(Gravity.LEFT)
                .setButtonOrientation(LinearLayoutCompat.VERTICAL)
                .setSystemDialog(false);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickImageDialog.build(setup).show((FragmentActivity) getContext());
            }
        });

        AuthUser authUser = repository.getAuth();

        if (authUser != null) {
            profileUserDescription.setText(authUser.getUserDescription());
            profileUserLastName.setText(authUser.getUserLastName());
            profileUserRole.setText(authUser.getRole());
            profileUserName.setText(authUser.getUserName());
            txtEmail.setText(authUser.getEmail());
            if (getArguments() != null) {
                bitmap = getArguments().getParcelable("BITMAP");
                profileImage.setImageBitmap(bitmap);
                isImageTaken = true;
            } else {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.default_user);
                requestOptions.error(R.drawable.default_user);
                Glide.with(activity)
                        .setDefaultRequestOptions(requestOptions)
                        .load(getProfileImageUrl(authUser.getIdUser(), authUser.getImageVersion()))
                        .into(profileImage);
                isImageTaken = false;
            }
        }

    }

    private String getProfileImageUrl(Long profileId, int version) {
        return Constants.IMAGE_PROFILE_PATH + profileId + Constants.JPG + Constants.VER + version;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logOutBtn:
                alertBuilderView = new AlertBuilderView(getActivity(), this, Operations.AlertBuilder_error_double, getString(R.string.alert_closesession));
                alertBuilderView.show();
                break;
            case R.id.saveBtn:
                PutUser user = new PutUser();
                user.setName(profileUserName.getText().toString());
                user.setLastName(profileUserLastName.getText().toString());
                user.setRole(profileUserRole.getText().toString());
                user.setUserDescription(profileUserDescription.getText().toString());
                //Log.d("osmi", bitmap.getByteCount() + "");
                repository.update(repository.getAuth(), user, bitmap, new GenericCallBack() {
                    @Override
                    public void onSuccess(@NonNull BaseResponse value) {
                        isImageTaken = false;
                        if (isAdded())
                            Toast.makeText(getActivity(), getString(R.string.update_success), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        redirectError(baseResponse);
                    }
                });
                break;
            case R.id.alert_btn_ok:
                alertBuilderView.dismiss();
                progressBuilder = new AlertBuilderView(getActivity(), Operations.AlertBuilder_progress, "");
                progressBuilder.show();
                AuthUser authUser = repository.getAuth();
                repository.logOut(authUser, new GenericCallBack() {
                    @Override
                    public void onSuccess(@NonNull BaseResponse value) {
                        progressBuilder.dismiss();
                        AppPreferences.getInstance(getActivity()).remove(Constants.ID_EVENT);
                        Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                    }

                    @Override
                    public void onError(@NonNull BaseResponse baseResponse) {
                        progressBuilder.dismiss();
                        if (baseResponse != null)
                            if (isAdded())
                                new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            else if (isAdded())
                                new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                    }
                });
                break;
            case R.id.alert_btn_nok:
                alertBuilderView.dismiss();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null)
            unbinder.unbind();
    }

    private void callcanBeReachedByMessage(final boolean ischecked) {
        CanBeReachedByMessageRequest request = new CanBeReachedByMessageRequest(ischecked);
        repository.canBeReachedByMessage(repository.getAuth(), request, new GenericCallBack() {
            @Override
            public void onSuccess(@NonNull BaseResponse value) {
                if (ischecked)
                    dbhelper.updateCanBeReachedField(1);
                else
                    dbhelper.updateCanBeReachedField(0);
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                redirectError(baseResponse);
            }
        });

    }

    private void redirectError(BaseResponse baseResponse) {
        AlertBuilderView alertBuilderView;
        if (baseResponse != null) {
            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                if (isAdded()) {
                    alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            repository.logOut();
                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                        }
                    });
                }
            } else {
                if (isAdded())
                    new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
            }
        } else {
            if (isAdded()) {
                alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error));
                alertBuilderView.show();
                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        repository.logOut();
                        Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                    }
                });
            }
        }
    }


}
