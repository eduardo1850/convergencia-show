package app.convergenciashow.events.activity;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.zxing.Result;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.base.ReturnBaseActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class SimpleScannerActivity extends ReturnBaseActivity implements ZXingScannerView.ResultHandler {

    private WebView webView;
    private ZXingScannerView mScannerView;
    private Unbinder unbinder;
    private AlertBuilderView progressBuilder;
    private ViewGroup contentFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_scanner);
        unbinder = ButterKnife.bind(this);

        contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        webView = (WebView) findViewById(R.id.content_wv); ;
        contentFrame.setVisibility(View.VISIBLE);
        webView.setVisibility(View.GONE);
        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
        if (mScannerView != null)
            mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        //Log.v("QR", rawResult.getText()); // Prints scan results
        //Log.v("QR", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        String URL = rawResult.getText() + "/" + Utils.getUserIdBase64(SimpleScannerActivity.this);
        final String URLreplace = URL.replaceAll("https", "http");
        mScannerView.resumeCameraPreview(this);
        mScannerView.stopCamera();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    contentFrame.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                    progressBuilder = new AlertBuilderView(SimpleScannerActivity.this, Operations.AlertBuilder_progress, "");
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView.setWebViewClient(new Callback());
                    webView.loadUrl(URLreplace);

                    if (Build.VERSION.SDK_INT >= 19)
                        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                    else
                        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageStarted(WebView view, String url, Bitmap favicon) {
                            super.onPageStarted(view, url, favicon);
                            progressBuilder.show();
                        }

                        @Override
                        public void onPageFinished(WebView view, String url) {
                            super.onPageFinished(view, url);
                            progressBuilder.dismiss();
                        }
                    });
                }catch (Exception e){
                    Log.e(getClass().getSimpleName().toString(), e.getMessage());
                }
            }
        });


    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return (false);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }


}
