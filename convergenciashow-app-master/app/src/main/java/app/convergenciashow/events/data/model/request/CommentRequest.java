package app.convergenciashow.events.data.model.request;

public class CommentRequest {
    private Long idPost;
    private String comment;

    public CommentRequest(Long idPost, String comment) {
        this.idPost = idPost;
        this.comment = comment;
    }
}
