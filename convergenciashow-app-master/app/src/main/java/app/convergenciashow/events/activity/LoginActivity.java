package app.convergenciashow.events.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.AuthRequest;
import app.convergenciashow.events.data.model.request.AuthRequestLinkedIn;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.AuthCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static app.convergenciashow.events.util.Utils.buildScope;
import static app.convergenciashow.events.util.Utils.invokeActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.login_til_email)
    TextInputLayout til_email;
    @BindView(R.id.login_et_email)
    EditText et_email;
    @BindView(R.id.login_til_password)
    TextInputLayout tilpassword;
    @BindView(R.id.login_et_password)
    EditText et_password;
    @BindView(R.id.login_btn_login)
    Button btn_login;
    @BindView(R.id.login_btn_linkedin)
    Button btn_linkedin;
    @BindView(R.id.login_tv_register)
    TextView tv_register;
    @BindView(R.id.login_tv_resetpass)
    TextView tv_resetpass;

    private AuthRepository authRepository;
    private AlertBuilderView progressBuilder;
    private String linkedinToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        authRepository = new AuthRepository(getApplicationContext(), this);
        progressBuilder = new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_progress, "");

        btn_login.setOnClickListener(this);
        btn_linkedin.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        tv_resetpass.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn_login:
                if (validate()) {
                    progressBuilder.show();
                    authRepository.authenticate(getLoginRequest(), new AuthCallBack() {
                        @Override
                        public void onSuccess(@NonNull AuthUser value) {
                            progressBuilder.dismiss();
                            if (value.isVerified()) {
                                AppPreferences.getInstance(LoginActivity.this).saveLongData(Constants.ID_EVENT, value.getDefaultEvent());
                                Utils.validateEvent(LoginActivity.this);
                            } else {
                                Utils.invokeActivity(LoginActivity.this, UnverifiedAccount.class, true);
                            }
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            progressBuilder.dismiss();
                            if (baseResponse != null)
                                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            else
                                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                        }
                    });
                }
                break;
            case R.id.login_btn_linkedin:
                // Validate LinkedIn package instalation
                if (Utils.isPackageExists(this, Constants.LinkedIn_Package)) {
                    progressBuilder.show();
                    // Validate LinkedIn session
                    if (!LISessionManager.getInstance(this).getSession().isValid()) {
                        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
                            @Override
                            public void onAuthSuccess() {
                                //fetch basic profile data for user
                                fetchLinkedInProfileData();
                            }

                            @Override
                            public void onAuthError(LIAuthError error) {
                                progressBuilder.dismiss();
                                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, getString(R.string.login_linkedin_error)).show();
                            }
                        }, false);
                    } else {
                        //if user is already authenticated fetch basic profile data for user
                        if (LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken() != null)
                            linkedinToken = LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().getValue();
                        fetchLinkedInProfileData();
                    }
                } else {
                    new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_linkedin, getString(R.string.login_linkedin_appvalidation)).show();
                }
                break;
            case R.id.login_tv_register:
                invokeActivity(LoginActivity.this, NewRegisterActivity.class, false);
                break;
            case R.id.login_tv_resetpass:
                invokeActivity(LoginActivity.this, RecoverPassword.class, false);
                break;


        }
    }

    // onActivityResult LinkedIn
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
        if (LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken() != null)
            linkedinToken = LISessionManager.getInstance(getApplicationContext()).getSession().getAccessToken().getValue();
    }

    // Fetch linkedin profile information and login using linkedIn credentials
    private void fetchLinkedInProfileData() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, Constants.FetchProfileDataURI, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                try {
                    authRepository.authenticateLinkedIn(getLoginLinkedInRequest(jsonObject), new AuthCallBack() {
                        @Override
                        public void onSuccess(@NonNull AuthUser value) {
                            progressBuilder.dismiss();
                            if (value.isVerified()) {
                                AppPreferences.getInstance(LoginActivity.this).saveLongData(Constants.ID_EVENT, value.getDefaultEvent());
                                Utils.validateEvent(LoginActivity.this);
                            } else {
                                Utils.invokeActivity(LoginActivity.this, UnverifiedAccount.class, true);
                            }
                        }

                        @Override
                        public void onError(@NonNull BaseResponse baseResponse) {
                            progressBuilder.dismiss();
                            if (baseResponse != null)
                                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                            else
                                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                        }
                    });
                } catch (JSONException e) {
                    new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error)).show();
                }
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                progressBuilder.dismiss();
                new AlertBuilderView(LoginActivity.this, Operations.AlertBuilder_error, getString(R.string.login_linkedin_fetchprofileinfoerror)).show();
            }
        });
    }

    // Get Login Request
    private AuthRequest getLoginRequest() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AuthRequest request = new AuthRequest(et_email.getText().toString(), et_password.getText().toString(), Constants.ANDROID_APP, refreshedToken);
        return request;
    }

    // Get Login LinkedIn Request
    private AuthRequestLinkedIn getLoginLinkedInRequest(JSONObject jsonObject) throws
            JSONException {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AuthRequestLinkedIn request = new AuthRequestLinkedIn(
                jsonObject.getString("id"),
                linkedinToken,
                jsonObject.getString("emailAddress"),
                jsonObject.getString("firstName"),
                jsonObject.getString("lastName"),
                Constants.ANDROID_APP,
                refreshedToken
        );
        return request;
    }

    // Validate Email and Pssword
    private boolean validate() {
        til_email.setError(null);
        tilpassword.setError(null);

        if (Utils.isEmpty(et_email)) {
            til_email.setError(getString(R.string.email_required));
            return false;
        } else if (!Utils.isEmailValid(et_email.getText().toString())) {
            til_email.setError(getString(R.string.email_invalid));
            return false;
        }

        if (Utils.isEmpty(et_password)) {
            tilpassword.setError(getString(R.string.password_required));
            return false;
        } else if (!Utils.isPasswordValid(et_password.getText().toString())) {
            tilpassword.setError(getString(R.string.password_invalid));
            return false;
        }

        return true;
    }

}
