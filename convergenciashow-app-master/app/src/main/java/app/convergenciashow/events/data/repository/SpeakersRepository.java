package app.convergenciashow.events.data.repository;

import android.content.Context;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.SpeakerResponse;
import app.convergenciashow.events.data.model.response.SpeakersResponse;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Speaker;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.SpeakersCallBack;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static app.convergenciashow.events.data.service.ServiceManger.getExpositoresService;

public class SpeakersRepository {

    private Context context;

    public SpeakersRepository(Context context){
        this.context = context;
    }

    public void getSpeakers(AuthUser user, Long idEvent, final SpeakersCallBack cb) {
        String userHeader = user.getIdUser() + " " + user.getIdSession();
        Call<SpeakersResponse> callResponse = getExpositoresService().list(userHeader, idEvent + "");
        callResponse.enqueue(new Callback<SpeakersResponse>() {
            @Override
            public void onResponse(Call<SpeakersResponse> call, Response<SpeakersResponse> response) {
                if (response.code() == HTTP_OK) {
                    cb.onSuccess(transform(response.body()));
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    cb.onError(baseResponse);
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), context);
                    cb.onError(baseResponse);
                }
            }

            @Override
            public void onFailure(Call<SpeakersResponse> call, Throwable t) {
                cb.onError(null);
            }
        });
    }

    private ArrayList<Speaker> transform(SpeakersResponse response) {
        ArrayList<SpeakerResponse> speakersResponses = response.getSpeakers();
        ArrayList<Speaker> speakers = new ArrayList<>();
        for (SpeakerResponse s : speakersResponses) {
            Speaker speaker = new Speaker();
            speaker.setId(s.getId());
            speaker.setName(s.getName());
            speaker.setLastName(s.getLastName());
            speaker.setCompanyName(s.getCompanyName());
            speaker.setRole(s.getRole());
            speaker.setImageVersion(s.getImageVersion());
            speaker.setExpos(s.getExpos());
            speakers.add(speaker);
        }
        return speakers;
    }

}
