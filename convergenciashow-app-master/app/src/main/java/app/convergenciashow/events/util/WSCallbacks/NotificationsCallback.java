package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import app.convergenciashow.events.domain.Notification;

public interface NotificationsCallback {
    void onSuccess(@NonNull ArrayList<Notification> value);

    void onError(@NonNull Throwable throwable);
}
