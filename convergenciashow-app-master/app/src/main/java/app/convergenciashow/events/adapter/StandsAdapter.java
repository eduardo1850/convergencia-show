package app.convergenciashow.events.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import app.convergenciashow.events.R;
import app.convergenciashow.events.domain.Stand;

public class StandsAdapter extends RecyclerView.Adapter<StandsAdapter.ViewHolder> implements Filterable {

    private ArrayList<Stand> items;
    private ArrayList<Stand> itemsFiltered;
    private Context context;

    public StandsAdapter(ArrayList<Stand> items, Context context) {
        this.items = items;
        this.itemsFiltered = items;
        this.context = context;
    }

    public void update(ArrayList<Stand> expoItems) {
        items.addAll(expoItems);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StandsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v;
        ViewHolder vh;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stand_item, viewGroup, false);
        vh = new ViewHolder(v, viewType);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull StandsAdapter.ViewHolder viewHolder, int i) {
        //Stand s = items.get(i);
        final Stand s = itemsFiltered.get(i);

        viewHolder.descriptionStand.setText(s.getDescription());
        viewHolder.claveStand.setText(s.getKey());
        viewHolder.nombreStand.setText(s.getExhibitor());
    }


    @Override
    public int getItemCount() {
        return itemsFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    itemsFiltered = items;
                } else {
                    ArrayList<Stand> filteredList = new ArrayList<>();
                    for (Stand row : items) {
                        if (row.getExhibitor().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    itemsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemsFiltered = (ArrayList<Stand>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView claveStand;
        TextView nombreStand;
        TextView descriptionStand;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            claveStand = itemView.findViewById(R.id.claveStand);
            nombreStand = itemView.findViewById(R.id.nombreStand);
            descriptionStand = itemView.findViewById(R.id.descriptionStand);
        }
    }


}
