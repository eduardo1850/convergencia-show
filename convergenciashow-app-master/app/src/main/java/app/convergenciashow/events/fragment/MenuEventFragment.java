package app.convergenciashow.events.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.ConvergenciaActivity;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.activity.PlanoStandsActivity;
import app.convergenciashow.events.activity.SchedulesActivity;
import app.convergenciashow.events.activity.SelectEventActivity;
import app.convergenciashow.events.activity.SimpleScannerActivity;
import app.convergenciashow.events.activity.StreamingActivity;
import app.convergenciashow.events.adapter.MenuEventAdapter;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.request.GotoEventRequest;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.domain.EventDetail;
import app.convergenciashow.events.domain.MenuEventItem;
import app.convergenciashow.events.util.AppPreferences;
import app.convergenciashow.events.util.Constants;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import app.convergenciashow.events.util.WSCallbacks.EventDetailCallBack;
import app.convergenciashow.events.util.WSCallbacks.GenericCallBack;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class MenuEventFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.CIeventImage)
    CircleImageView eventImage;
    @BindView(R.id.eventName)
    TextView eventTitle;
    @BindView(R.id.menuRv)
    RecyclerView menuRecyclerView;
    @BindView(R.id.switchButton)
    Switch switchButton;

    private EventRepository repository;
    private AuthRepository authRepository;
    private int COLUMNS = 3;
    private AlertBuilderView progressBuilder;
    private Button changeButton;

    public MenuEventFragment() {

    }

    public static MenuEventFragment newInstance() {
        MenuEventFragment fragment = new MenuEventFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //setTitleBar(TITLE_BAR);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_event, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        invokeActionBar(getString(R.string.evento));

        final Activity activity = getActivity();
        changeButton = (Button) getActivity().findViewById(R.id.btn_change_event);
        changeButton.setOnClickListener(this);
        progressBuilder = new AlertBuilderView(activity, Operations.AlertBuilder_progress, "");
        repository = new EventRepository(getContext());
        authRepository = new AuthRepository(getContext(), activity);
        menuRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), COLUMNS, GridLayoutManager.VERTICAL, false));
        menuRecyclerView.setAdapter(new MenuEventAdapter(getEventItems(), MenuEventFragment.this));
        //progressBuilder.show();

        repository.getDetail(authRepository.getAuth(), AppPreferences.getInstance(getActivity()).getLongData(Constants.ID_EVENT) + "", new EventDetailCallBack() {
            @Override
            public void onSuccess(@NonNull EventDetail value) {
                AppPreferences.getInstance(activity).saveLongData(Constants.ID_EVENT, value.getId());
                AppPreferences.getInstance(activity).saveIntData(Constants.EVENT_MAP_VERSION, value.getGlobalMapVersion());
                AppPreferences.getInstance(activity).saveBooleanData(Constants.EVENT_MAP_AVAILABLE, value.isGlobalMapAvailable());
                if (value.isWillUserBeInEvent())
                    switchButton.setChecked(true);
                else
                    switchButton.setChecked(false);

                if (isAdded())
                    Glide.with(activity)
                            .load(Utils.getEventImageUrl(value.getId(), value.getImageVersion()))
                            .into(eventImage);
                eventTitle.setText(value.getName());
                setSwtich(value);
                //progressBuilder.dismiss();
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                //progressBuilder.dismiss();
                redirectError(baseResponse);
            }
        });

    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);

    }

    /*@Override
    protected void setComponent() {
    }*/

    private ArrayList<MenuEventItem> getEventItems() {

        ArrayList<MenuEventItem> items = new ArrayList<>();

        // Agenda
        MenuEventItem item1 = new MenuEventItem();
        item1.setName(getString(R.string.app_schedule));
        item1.setImage(R.drawable.agenda);
        //item1.setFragment(CalendarFragment. ());
        item1.setActivity(SchedulesActivity.class);

        // Asistentes
        MenuEventItem item2 = new MenuEventItem();
        item2.setName(getString(R.string.app_assistants));
        item2.setImage(R.drawable.asistentes);
        item2.setFragment(AsistentesFragment.newInstance());

        // Sinergia OSC
        /*MenuEventItem item3 = new MenuEventItem();
        item3.setName(getString(R.string.app_forum));
        item3.setImage(R.drawable.foro);
        item3.setFragment(ForoFragment.newInstance());*/

        // La Casa
        /*MenuEventItem item4 = new MenuEventItem();
        item4.setName(getString(R.string.app_home));
        item4.setImage(R.drawable.lacasa);
        item4.setFragment(LaCasaFragment.newInstance());*/

        MenuEventItem item4 = new MenuEventItem();
        item4.setName(getString(R.string.app_home));
        item4.setImage(R.drawable.lacasa);
        item4.setActivity(ConvergenciaActivity.class);

        // Encuestas
        MenuEventItem item5 = new MenuEventItem();
        item5.setName(getString(R.string.app_polls));
        item5.setImage(R.drawable.polls);
        item5.setFragment(PollsFragment.newInstance());

        // Conferencistas
        MenuEventItem item6 = new MenuEventItem();
        item6.setName(getString(R.string.app_exponents));
        item6.setImage(R.drawable.expositores);
        item6.setFragment(SpeakersFragment.newInstance());

        // Streaming
        MenuEventItem item7 = new MenuEventItem();
        item7.setName(getString(R.string.app_streaming));
        item7.setImage(R.drawable.streaming);
        item7.setActivity(StreamingActivity.class);

        // Presentaciones
        /*MenuEventItem item8 = new MenuEventItem();
        item8.setName(getString(R.string.app_content));
        item8.setImage(R.drawable.contenido);
        item8.setFragment(ContentFragment.newInstance());*/

        // Patrocinadores
        MenuEventItem item9 = new MenuEventItem();
        item9.setName(getString(R.string.app_sponsors));
        item9.setImage(R.drawable.sponsors);
        item9.setFragment(SponsorsFragment.newInstance());

        // Galeria
        MenuEventItem item10 = new MenuEventItem();
        item10.setName(getString(R.string.app_gallery));
        item10.setImage(R.drawable.galeria);
        item10.setFragment(GalleryFragment.newInstance());

        // Twitter
        MenuEventItem item11 = new MenuEventItem();
        item11.setName(getString(R.string.app_twitter));
        item11.setImage(R.drawable.twitter);
        item11.setFragment(TwitterFragment.newInstance());

        // Mapa
        /*MenuEventItem item12 = new MenuEventItem();
        item12.setName(getString(R.string.app_map));
        item12.setImage(R.drawable.ubicacion);
        item12.setFragment(MapFragment.newInstance());*/
        //item10.setActivity(PlanoStandsActivity.class);

        // Feria de Organizaciones
        MenuEventItem item13 = new MenuEventItem();
        item13.setName(getString(R.string.app_plan));
        item13.setImage(R.drawable.plano);
        item13.setActivity(PlanoStandsActivity.class);

        // Patrocinadores
        MenuEventItem item14 = new MenuEventItem();
        item14.setName(getString(R.string.app_mediapartners));
        item14.setImage(R.drawable.partners);
        item14.setFragment(MediaPartnersFragment.newInstance());

        MenuEventItem item15 = new MenuEventItem();
        item7.setName(getString(R.string.app_tarjetero));
        item7.setImage(R.drawable.tarjetero);
        item7.setActivity(SimpleScannerActivity.class);

        items.add(item6);  // Conferencistas
        items.add(item13); // Expositores
        items.add(item2);  // Visitantes
        items.add(item9);  // Sponsors
        items.add(item1);  // Agenda
        // Tarjetero
        items.add(item7);  // Streaming
        items.add(item5);  // Encuestas
        items.add(item4);  // Convergencia
        items.add(item10); // Galeria
        items.add(item11); // Twitter

        // Hospedaje
        //items.add(item3);  // Sinergia ESR
        //items.add(item8);  // Presentaciones
        //items.add(item12); // Mapa

        //items.add(item14);  // MediaPartners

        return items;
    }

    private void setSwtich(final EventDetail value) {
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {
                    callmarkInEvent(value.getId(), true);
                } else {
                    callmarkInEvent(value.getId(), false);
                }
            }
        });
    }

    private void callmarkInEvent(Long eventID, final boolean ischecked) {
        GotoEventRequest request = new GotoEventRequest(eventID, ischecked);
        authRepository.markInEvent(authRepository.getAuth(), request, new GenericCallBack() {
            @Override
            public void onSuccess(@NonNull BaseResponse value) {

            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                redirectError(baseResponse);
            }
        });
    }

    private void redirectError(BaseResponse baseResponse) {
        AlertBuilderView alertBuilderView;
        if (baseResponse != null) {
            if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                if (isAdded()) {
                    alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.session_error));
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            authRepository.logOut();
                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                        }
                    });
                }
            } else {
                if (isAdded()) {
                    alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, baseResponse.getCodeDescription());
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            authRepository.logOut();
                            Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                        }
                    });
                }
            }
        } else {
            if (isAdded()) {
                alertBuilderView = new AlertBuilderView(getActivity(), Operations.AlertBuilder_error, getString(R.string.generic_error));
                alertBuilderView.show();
                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        authRepository.logOut();
                        Utils.invokeActivity(getActivity(), LoginActivity.class, true);
                    }
                });
            }
        }
    }

    private void invokeActionBar(String titleBar) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_action_bar, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                )
        );
        TextView title = activity.findViewById(R.id.action_bar_title);
        Button switchEvent = activity.findViewById(R.id.btn_eventswitch);

        title.setVisibility(View.GONE);
        switchEvent.setVisibility(View.GONE);

        /*switchEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded())
                    Utils.invokeActivity(getActivity(), SelectEventActivity.class, false);
            }
        });*/
    }

    public void openFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, selectedFragment);
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        if (isAdded())
            Utils.invokeActivity(getActivity(), SelectEventActivity.class, false);
    }
}
