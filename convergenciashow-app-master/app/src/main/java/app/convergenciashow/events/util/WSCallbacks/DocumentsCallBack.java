package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import java.util.List;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.domain.Document;

public interface DocumentsCallBack {

    void onSuccess(@NonNull List<Document> documents);

    void onError(@NonNull BaseResponse baseResponse);
}
