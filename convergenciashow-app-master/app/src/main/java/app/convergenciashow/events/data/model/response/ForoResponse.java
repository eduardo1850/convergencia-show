package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class ForoResponse extends BaseResponse{
    private ArrayList<PostResponse> posts;

    public ArrayList<PostResponse> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<PostResponse> posts) {
        this.posts = posts;
    }
}
