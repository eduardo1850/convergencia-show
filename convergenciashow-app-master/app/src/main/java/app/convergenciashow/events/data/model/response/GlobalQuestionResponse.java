package app.convergenciashow.events.data.model.response;

import java.io.Serializable;
import java.util.ArrayList;

public class GlobalQuestionResponse implements Serializable {
    private Long idQuestion;
    private String title;
    private Integer order;
    private boolean isPublic;
    private ArrayList<Long> optionsSelected;
    private boolean multiSelection;
    private Integer answers;
    private ArrayList<OptionsResponse> options;

    public Long getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Long> getOptionsSelected() {
        return optionsSelected;
    }

    public void setOptionsSelected(ArrayList<Long> optionsSelected) {
        this.optionsSelected = optionsSelected;
    }

    public boolean isMultiSelection() {
        return multiSelection;
    }

    public void setMultiSelection(boolean multiSelection) {
        this.multiSelection = multiSelection;
    }

    public Integer getAnswers() {
        return answers;
    }

    public void setAnswers(Integer answers) {
        this.answers = answers;
    }

    public ArrayList<OptionsResponse> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionsResponse> options) {
        this.options = options;
    }

    public void setOrder(Integer order){this.order= order;}
    public Integer getOrder(){return order;}

    public void setIsPublic(boolean isPublic){this.isPublic = isPublic;}
    public boolean getIsPublic(){return isPublic;}

}
