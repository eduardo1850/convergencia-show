package app.convergenciashow.events.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.linkedin.platform.utils.Scope;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.convergenciashow.events.R;
import app.convergenciashow.events.activity.GetSpeakerDetailActivity;
import app.convergenciashow.events.activity.GetUserDetailActivity;
import app.convergenciashow.events.activity.LoginActivity;
import app.convergenciashow.events.activity.MenuContainerActivity;
import app.convergenciashow.events.activity.QuestionActivity;
import app.convergenciashow.events.activity.SelectEventActivity;
import app.convergenciashow.events.activity.GlobalQuestionActivity;
import app.convergenciashow.events.activity.SimpleScannerActivity;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.database.SQLiteHelper;
import app.convergenciashow.events.data.model.response.ActiveQuestionResponse;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.GlobalQuestionResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.data.repository.EventRepository;
import app.convergenciashow.events.domain.AuthUser;
import app.convergenciashow.events.domain.Expo;
import app.convergenciashow.events.domain.Speaker;
import app.convergenciashow.events.domain.User;
import app.convergenciashow.events.util.WSCallbacks.ActiveQuestionCallback;
import app.convergenciashow.events.util.WSCallbacks.GlobalQuestionCallback;
import okhttp3.ResponseBody;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Eduardo Aceves Adán
 */
public class Utils {

    public static String TAG = Utils.class.getSimpleName().toString();

    // EditText is empty validation.
    public static boolean isEmpty(EditText editText) {
        boolean result = true;
        if (editText != null)
            if (!editText.getText().toString().isEmpty())
                result = false;
        return result;
    }

    // Email validation method
    public static boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // Check password with minimum requirement here
    public static boolean isPasswordValid(String password) {
        return password.length() > 1;
    }

    /**
     * Parsea los codigos de error ligados a los servicios web
     *
     * @param response Objeto para codigos genericos
     * @return BaseResponse
     */
    public static BaseResponse parse409(ResponseBody response, Context context) {
        BaseResponse baseResponse = null;
        try {
            Gson gson = new Gson();
            baseResponse = gson.fromJson(response.string(), BaseResponse.class);
            baseResponse.setCodeDescription(getStringResourceByName(context, "error_" + baseResponse.getCodeNumber()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseResponse;
    }

    // invoke new activity
    public static void invokeActivity(Activity activity, Class clazz, boolean shouldFinish) {
        Intent intent = new Intent(activity, clazz);
        activity.startActivity(intent);
        if (shouldFinish) {
            activity.finish();
        }
    }

    // get hash from linkedin
    public static void getPackageHash(Activity activity) {
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = activity.getPackageManager().getPackageInfo("app.convergenciashow.events", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d(TAG, "Hash  : " + Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d(TAG, e.getMessage(), e);
        }

    }

    // LinkedIn Api scope
    public static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE, Scope.R_EMAILADDRESS);
    }

    // Validate if a certain package is already installed in device
    public static boolean isPackageExists(Activity activity, String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = activity.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }

    // Encode Bitmap to base64
    public static String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    public static String parseDate(String rawdate) {
        StringBuffer sb = new StringBuffer();
        try {
            if (rawdate != null) {
                String[] split = rawdate.split(" ");
                String[] date = split[0].split("-");
                sb.append(date[2] + "-");
                sb.append(Utils.getMonth(date[1]) + "-");
                sb.append(date[0]);
                return sb.toString();
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static String parseTime(String rawdate) {
        StringBuffer sb = new StringBuffer();
        try {
            if (rawdate != null) {
                String[] split = rawdate.split(" ");
                if (split[1].contains("."))
                    sb.append(" " + split[1].substring(0, split[1].length() - 2));
                return sb.toString();
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }


    public static String getMonth(String month) {
        switch (month) {
            case "01":
                return "Ene";
            case "02":
                return "Feb";
            case "03":
                return "Mar";
            case "04":
                return "Abr";
            case "05":
                return "May";
            case "06":
                return "Jun";
            case "07":
                return "Jul";
            case "08":
                return "Ago";
            case "09":
                return "Sep";
            case "10":
                return "Oct";
            case "11":
                return "Nov";
            case "12":
                return "Dic";
            default:
                return "";
        }
    }

    public static String getCompleteMonth(String month) {
        switch (month) {
            case "1":
            case "01":
                return "Enero";
            case "2":
            case "02":
                return "Febrero";
            case "3":
            case "03":
                return "Marzo";
            case "4":
            case "04":
                return "Abril";
            case "5":
            case "05":
                return "Mayo";
            case "6":
            case "06":
                return "Junio";
            case "7":
            case "07":
                return "Julio";
            case "8":
            case "08":
                return "Agosto";
            case "9":
            case "09":
                return "Septiembre";
            case "10":
                return "Octubre";
            case "11":
                return "Noviembre";
            case "12":
                return "Diciembre";
            default:
                return "";
        }
    }

    @SuppressLint("StringFormatMatches")
    public static String timeIsRunningOut(String Date, Context context) {
        long minutesAgo = 0;
        String text = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat(Constants.MESSAGES_DATE_FORMAT);
            String date = format.format(Calendar.getInstance().getTime());
            Date date1 = format.parse(date);
            Date date2 = format.parse(Date);
            long diffMs = date1.getTime() - date2.getTime();
            long diffSec = diffMs / 1000;
            minutesAgo = diffSec / 60;
            //long sec = diffSec % 60;
            if (minutesAgo > 10080) {
                String[] splitDate = Date.split(" ");
                text = splitDate[0];
            } else if (minutesAgo > 1400) {
                //El pasado %@ a las %@
                String[] splitDate = Date.split(" ");
                if (splitDate[1].contains("."))
                    text = String.format(context.getString(R.string.chat_a_days_ago), getDayOfWeek(format.parse(Date), context), splitDate[1].substring(0, splitDate[0].length() - 2));
                else
                    text = String.format(context.getString(R.string.chat_a_days_ago), getDayOfWeek(format.parse(Date), context), splitDate[1]);
            } else if (minutesAgo >= 120) {
                //Hace %d horas
                text = String.format(context.getString(R.string.chat_a_x_hours_ago), (minutesAgo / 60));
            } else if (minutesAgo >= 62) {
                //Hace 1 hora y %d minutos
                text = String.format(context.getString(R.string.chat_a_x_hour_and_minutesago), (minutesAgo - 60));
            } else if (minutesAgo > 3) {

                text = String.format(context.getString(R.string.chat_a_x_minutes_ago), minutesAgo);
            } else {
                text = context.getString(R.string.chat_a_moment_ago);

            }
        } catch (Exception exception) {
            return text;
        }
        return text;
    }

    public static String setDateSchedule(String Date, Context context) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.SCHEDULES_DATE_FORMAT);
        try {
            String[] tmp = Date.substring(0, 11).split("-");
            return getDayOfWeek(format.parse(Date), context) + ", " + tmp[2] + " de " + getCompleteMonth(tmp[1]);
        } catch (ParseException e) {
            return Date.substring(0, 11);
        }
    }

    public static String getDayOfWeek(Date date, Context context) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            switch (dayOfWeek) {
                case 1:
                    return context.getString(R.string.dayofweek_1);
                case 2:
                    return context.getString(R.string.dayofweek_2);
                case 3:
                    return context.getString(R.string.dayofweek_3);
                case 4:
                    return context.getString(R.string.dayofweek_4);
                case 5:
                    return context.getString(R.string.dayofweek_5);
                case 6:
                    return context.getString(R.string.dayofweek_6);
                case 7:
                    return context.getString(R.string.dayofweek_7);
                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }

    public static String formatDate(final String format, final Date date) {
        final DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static void gotoUserDetail(final Long IdAsistente, Activity activity) {
        Long idEvent = AppPreferences.getInstance(activity).getLongData(Constants.ID_EVENT);
        Intent intent = new Intent(activity, GetUserDetailActivity.class);
        intent.putExtra(Constants.IDUSERTOCONVERSE, IdAsistente);
        intent.putExtra(Constants.IDEVENT, idEvent);
        activity.startActivity(intent);

    }

    public static void gotoSpeakerDetail(final Long IdAsistente, Activity activity) {
        Long idEvent = AppPreferences.getInstance(activity).getLongData(Constants.ID_EVENT);
        Intent intent = new Intent(activity, GetSpeakerDetailActivity.class);
        intent.putExtra(Constants.IDUSERTOCONVERSE, IdAsistente);
        activity.startActivity(intent);
    }

    public static String arrayToString(String[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        for (int a = 0; a < array.length; a++) {
            sb.append(array[a] + ", ");
        }
        String s = sb.toString();
        if (!s.equals("")) {
            s = s.substring(0, s.lastIndexOf(", "));
        }
        return s;
    }

    public static void changeLikeStatus(boolean isVoted, final ImageView iv_likes) {
        if (isVoted) {
            iv_likes.setImageResource(0);
            iv_likes.setBackgroundResource(R.drawable.ic_like_on);
        } else {
            iv_likes.setImageResource(0);
            iv_likes.setBackgroundResource(R.drawable.ic_like);
        }
    }

    public static String getParseForumDate(String Date) {
        try {
            StringBuffer sb = new StringBuffer();
            String[] pdate = Date.split(" ");
            String[] tmp = pdate[0].split("-");
            sb.append(getCompleteMonth(tmp[1].trim()) + " ");
            sb.append(tmp[2].trim() + ", ");
            sb.append(tmp[0].trim());
            return sb.toString();
        } catch (Exception e) {
            return Date;
        }
    }

    /**
     * Obtiene el valor del archivo strings.xml a traves del nombre.
     *
     * @param context - Activity.
     * @param aString - Valor a buscar.
     * @return String - Valor de campo string.
     */
    public static String getStringResourceByName(Context context, String aString) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }

    public static String getForumImageUrl(Activity activity, Long profileId, int version) {
        return Constants.FORUM_URL_PATH + AppPreferences.getInstance(activity).getLongData(Constants.ID_EVENT) + "/" + profileId + Constants.JPG + Constants.VER + version;
    }

    public static String getGalleryImageUrl(boolean isThumbnail, Long imageId, int version) {
        if (isThumbnail)
            return Constants.GALLERY_URL_PATH + imageId + "-thumb" + Constants.JPG + Constants.VER + version;
        else
            return Constants.GALLERY_URL_PATH + imageId + Constants.JPG + Constants.VER + version;
    }

    public static String getDocumentUrl(Long documentId, int version) {
        return Constants.DOCUMENTS_URL_PATH + documentId + Constants.PDF + Constants.VER + version;
    }

    public static String getSponsorsUrl(Long imageId) {
        return Constants.SPONSORS_URL_PATH + imageId + Constants.JPG;
    }

    public static String getEventImageUrl(Long eventId, int version) {
        return Constants.EVENT_URL_PATH + eventId + Constants.JPG + Constants.VER + version;
    }

    public static String getPdfUrl(Long eventID, int version) {
        return Constants.PDF_URL_PATH + eventID + Constants.PDF + Constants.VER + version;
    }

    public static String getMapUrl(Long eventID, int version) {
        return Constants.MAP_URL_PATH + eventID + Constants.PDF + Constants.VER + version;
    }

    public static String getSpeakersImageUrl(Long profileId, int version) {
        return Constants.IMAGE_SPEAKERS_PATH + profileId + Constants.JPG + Constants.VER + version;
    }

    public static String getProfileImageUrl(Long profileId, int version) {
        return Constants.IMAGE_PROFILE_PATH + profileId + Constants.JPG + Constants.VER + version;
    }

    public static String getAuthorImageUrl(Long authorId, int version) {
        return Constants.AUTHOR_URL_PATH + authorId + Constants.JPG + Constants.VER + version;
    }

    public static String getNewsImageUrl(Long newId, int version) {
        return Constants.NEWS_URL_PATH + newId + Constants.JPG + Constants.VER + version;
    }

    public static boolean requestPermissions(Activity activity, ArrayList<String> arrPerm) {
        ArrayList<String> permissions = new ArrayList<>();
        for (int i = 0; i < arrPerm.size(); i++) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), arrPerm.get(i)) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(arrPerm.get(i));
            }
        }
        if (!permissions.isEmpty()) {
            String[] permissionsString = new String[arrPerm.size()];
            permissionsString = arrPerm.toArray(permissionsString);
            ActivityCompat.requestPermissions(activity, permissionsString, Constants.PERMISSIONS_CODE);
            return true;
        } else {
            return false;
        }
    }

    public static Uri getLocalBitmapUri(ImageView imageView) {

        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }

        Uri bmpUri = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void setFragmentToShow(Activity activity, int fragmentToShow) {
        SharedPreferences.Editor editor = activity.getPreferences(Context.MODE_PRIVATE).edit();
        editor.putInt(Constants.FRAGMENT_TO_SHOW, fragmentToShow);
        editor.commit();
    }

    public static void validateEvent(Activity activity) {
        Long idEvent = AppPreferences.getInstance(activity).getLongData(Constants.ID_EVENT);
        if (idEvent == 0) {
            Utils.invokeActivity(activity, SelectEventActivity.class, true);
        } else {
            Utils.invokeActivity(activity, MenuContainerActivity.class, true);
        }
    }

    public static boolean validateInternet(Activity activity) {
        if (isConnectionNetwork(activity)) {
            return true;
        } else {
            new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.alert_message_Internet)).show();
            return false;
        }
    }

    public static boolean isConnectionNetwork(Activity activity) {
        NetworkInfo netInfo = null;
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            netInfo = cm.getActiveNetworkInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return netInfo != null && netInfo.isAvailable();
    }

    /**
     * Hides the current open keyboard
     *
     * @param activity the activity
     */
    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.e(className, e.getMessage());
        }
    }

    public static void getActiveQuestion(final Long idPoll, final EventRepository repository, final AuthRepository authRepository, final Activity activity, final boolean reload) {

        repository.getActiveQuestion(authRepository.getAuth(), idPoll, new ActiveQuestionCallback() {
            @Override
            public void onSuccess(@NonNull ActiveQuestionResponse activeQuestionResponse) {
                /*if (activeQuestionResponse.getOptionsSelected() != null) {
                    Intent intent = new Intent(activity, QuestionActivity.class);
                    intent.putExtra(Constants.QUESTION_ACTIVITY, activeQuestionResponse);
                    intent.putExtra(Constants.ID_POLL, idPoll);
                    activity.startActivity(intent);
                } else {*/
                if (reload) {
                    ((QuestionActivity) activity).loadInformation(idPoll, activeQuestionResponse);
                } else {
                    Intent intent = new Intent(activity, QuestionActivity.class);
                    intent.putExtra(Constants.QUESTION_ACTIVITY, activeQuestionResponse);
                    intent.putExtra(Constants.ID_POLL, idPoll);
                    activity.startActivity(intent);
                }
                //}
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(Constants.POLLS_ERROR_CODE)) {
                        if (reload) {
                            ((QuestionActivity) activity).loadInformation(idPoll, null);
                        } else {
                            final Intent intent = new Intent(activity, QuestionActivity.class);
                            intent.putExtra(Constants.ID_POLL, idPoll);
                            activity.startActivity(intent);
                        }
                    } else if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        AlertBuilderView alertBuilderView = new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.session_error));
                        alertBuilderView.show();
                        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                authRepository.logOut();
                                Utils.invokeActivity(activity, LoginActivity.class, true);
                            }
                        });
                    } else {
                        new AlertBuilderView(activity, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.generic_error)).show();
                }

            }
        });
    }

    public static void getNextQuestion(final Long idPoll, final EventRepository repository, final AuthRepository authRepository, final Activity activity, final boolean reload) {
        repository.getNextQuestion(authRepository.getAuth(), idPoll, new GlobalQuestionCallback() {
            @Override
            public void onSuccess(@NonNull GlobalQuestionResponse activeQuestionResponse) {
                /*if (activeQuestionResponse.getOptionsSelected() != null) {
                    Intent intent = new Intent(activity, QuestionActivity.class);
                    intent.putExtra(Constants.QUESTION_ACTIVITY, activeQuestionResponse);
                    intent.putExtra(Constants.ID_POLL, idPoll);
                    activity.startActivity(intent);
                } else {*/
                if (reload) {
                    ((GlobalQuestionActivity) activity).loadInformation(idPoll, activeQuestionResponse);
                } else {
                    Intent intent = new Intent(activity, GlobalQuestionActivity.class);
                    intent.putExtra(Constants.GLOBAL_QUESTION_ACTIVITY, activeQuestionResponse);
                    intent.putExtra(Constants.ID_POLL, idPoll);
                    activity.startActivity(intent);
                }
                //}
            }

            @Override
            public void onError(@NonNull BaseResponse baseResponse) {
                if (baseResponse != null) {
                    if (baseResponse.getCodeNumber().equals(Constants.POLLS_NO_MORE_QUESTIONS)) {
                        if (reload) {
                            ((GlobalQuestionActivity) activity).loadInformation(idPoll, null);
                        } else {
                            final Intent intent = new Intent(activity, GlobalQuestionActivity.class);
                            intent.putExtra(Constants.ID_POLL, idPoll);
                            activity.startActivity(intent);
                        }
                    } else if (baseResponse.getCodeNumber().equals(String.valueOf(HTTP_UNAUTHORIZED))) {
                        AlertBuilderView alertBuilderView = new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.session_error));
                        alertBuilderView.show();
                        alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                authRepository.logOut();
                                Utils.invokeActivity(activity, LoginActivity.class, true);
                            }
                        });
                    } else {
                        new AlertBuilderView(activity, Operations.AlertBuilder_error, baseResponse.getCodeDescription()).show();
                    }
                } else {
                    new AlertBuilderView(activity, Operations.AlertBuilder_error, activity.getString(R.string.generic_error)).show();
                }

            }
        });
    }

    public static String getGalleryDate(String dateString, Context context) {
        Calendar c = Calendar.getInstance();
        StringBuffer galleryDate = new StringBuffer();
        try {
            String date[] = dateString.split("-");
            c.set(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2]));
            galleryDate.append(getDayOfWeek(c.getTime(), context) + ", ");
            galleryDate.append(Integer.parseInt(date[2]) + " de " + getCompleteMonth(date[1]));
        } catch (Exception e) {
            galleryDate.append(getDayOfWeek(c.getTime(), context) + ", ");
            galleryDate.append(c.get(Calendar.DAY_OF_MONTH) + " de " + getCompleteMonth((c.get(Calendar.MONTH) + 1) + ""));
        }
        return galleryDate.toString();
    }

    public static String getDescription(Object object) {
        String Company = "", Role = "", sub = "";
        try {
            if (object instanceof User) {
                User item = ((User) object);
                Company = item.getCompanyName();
                Role = item.getRole();
            } else if (object instanceof Speaker) {
                Speaker item = ((Speaker) object);
                Company = item.getCompanyName();
                Role = item.getRole();
            } else if(object instanceof Expo){
                Expo item = ((Expo) object);
                Company = item.getDate().substring(11, 16);
                Role = item.getRoomTitle();
            }

            if (Company.trim().isEmpty()) {
                if (Role.isEmpty()) {
                    sub = "";
                } else {
                    sub = Role;
                }
            } else {
                if (Role.trim().isEmpty()) {
                    sub = Company;
                } else {
                    sub = Company + " - " + Role;
                }
            }
            return sub;
        } catch (Exception e) {
            return sub;
        }
    }

    public static String getUserIdBase64(Activity activity) {
        try {
            SQLiteHelper sqLiteHelper = new SQLiteHelper(activity);
            AuthUser authUser = sqLiteHelper.getAuth();
            String idUser = Long.toString(authUser.getIdUser());
            byte[] data = idUser.getBytes("UTF-8");
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            return "";
        }
    }


}
