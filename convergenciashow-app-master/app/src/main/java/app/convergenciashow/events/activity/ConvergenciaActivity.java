package app.convergenciashow.events.activity;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.custom.AlertBuilderView;
import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.ConvergenciaResponse;
import app.convergenciashow.events.data.repository.AuthRepository;
import app.convergenciashow.events.util.Operations;
import app.convergenciashow.events.util.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.convergenciashow.events.data.service.ServiceManger.getStreamingService;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class ConvergenciaActivity extends AppCompatActivity {

    private String urlStream;
    private VideoView streaming;
    private AlertBuilderView alertBuilderView;

    @Override
    protected void onStart() {
        super.onStart();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        streaming = findViewById(R.id.streaming);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MediaController mc = new MediaController(this);
        streaming.setMediaController(mc);
        getStreaming();
    }


    public void getStreaming() {
        Call<ConvergenciaResponse> callResponse = getStreamingService().getConvergenciaInfo("VIDEO");
        callResponse.enqueue(new Callback<ConvergenciaResponse>() {
            @Override
            public void onResponse(Call<ConvergenciaResponse> call, final Response<ConvergenciaResponse> response) {
                if (response.code() == HTTP_OK) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            urlStream = response.body().getDescription();
                            streaming.setVideoURI(Uri.parse(urlStream));
                            streaming.start();
                        }
                    });
                } else if (response.code() == HTTP_UNAUTHORIZED) {
                    BaseResponse baseResponse = new BaseResponse();
                    baseResponse.setCodeNumber(String.valueOf(HTTP_UNAUTHORIZED));
                    alertBuilderView = new AlertBuilderView(ConvergenciaActivity.this, Operations.AlertBuilder_error, getString(R.string.session_error));
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            AuthRepository authRepository = new AuthRepository(getApplicationContext(), ConvergenciaActivity.this);
                            authRepository.logOut();
                            Utils.invokeActivity(ConvergenciaActivity.this, LoginActivity.class, true);
                        }
                    });
                } else {
                    BaseResponse baseResponse = Utils.parse409(response.errorBody(), getApplicationContext());
                    alertBuilderView = new AlertBuilderView(ConvergenciaActivity.this, Operations.AlertBuilder_error, baseResponse.getCodeDescription());
                    alertBuilderView.show();
                    alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            onBackPressed();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ConvergenciaResponse> call, Throwable t) {
                alertBuilderView = new AlertBuilderView(ConvergenciaActivity.this, Operations.AlertBuilder_error, getString(R.string.generic_error));
                alertBuilderView.show();
                alertBuilderView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        onBackPressed();
                    }
                });
            }
        });

    }
}
