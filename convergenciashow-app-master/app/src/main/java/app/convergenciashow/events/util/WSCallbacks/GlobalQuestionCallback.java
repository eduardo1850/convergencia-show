package app.convergenciashow.events.util.WSCallbacks;

import android.support.annotation.NonNull;

import app.convergenciashow.events.data.model.response.BaseResponse;
import app.convergenciashow.events.data.model.response.GlobalQuestionResponse;

public interface GlobalQuestionCallback {

    void onSuccess(@NonNull GlobalQuestionResponse activeQuestionResponse);
    void onError(@NonNull BaseResponse baseResponse);
}
