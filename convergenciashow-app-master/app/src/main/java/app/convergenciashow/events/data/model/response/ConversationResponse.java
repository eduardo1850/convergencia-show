package app.convergenciashow.events.data.model.response;

public class ConversationResponse {

    private Long idUser;
    private String name;
    private Boolean userHasImage;
    private Integer imageVersion;
    private String date;
    private Integer notRead;

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getUserHasImage() {
        return userHasImage;
    }

    public void setUserHasImage(Boolean userHasImage) {
        this.userHasImage = userHasImage;
    }

    public Integer getImageVersion() {
        return imageVersion;
    }

    public void setImageVersion(Integer imageVersion) {
        this.imageVersion = imageVersion;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getNotRead() {
        return notRead;
    }

    public void setNotRead(Integer notRead) {
        this.notRead = notRead;
    }




}
