package app.convergenciashow.events.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Plano implements Parcelable{

    public Plano(){

    }

    public Plano (Parcel in){
        readFromParcel(in);
    }

    private Boolean mapAvailable;

    private Integer mapVersion;

    private ArrayList<Stand> stands;

    public Boolean getMapAvailable() {
        return mapAvailable;
    }

    public void setMapAvailable(Boolean mapAvailable) {
        this.mapAvailable = mapAvailable;
    }

    public Integer getMapVersion() {
        return mapVersion;
    }

    public void setMapVersion(Integer mapVersion) {
        this.mapVersion = mapVersion;
    }

    public ArrayList<Stand> getStands() {
        return stands;
    }

    public void setStands(ArrayList<Stand> stands) {
        this.stands = stands;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mapAvailable);
        dest.writeInt(mapVersion);
        dest.writeValue(stands);
    }

    private void readFromParcel(Parcel in) {
        mapAvailable = (Boolean) in.readValue(null);
        mapVersion = in.readInt();
        stands = (ArrayList<Stand>) in.readValue(null);
    }

    public static final Parcelable.Creator<Plano> CREATOR
            = new Parcelable.Creator<Plano>() {
        public Plano createFromParcel(Parcel in) {
            return new Plano(in);
        }

        public Plano[] newArray(int size) {
            return new Plano[size];
        }
    };

}
