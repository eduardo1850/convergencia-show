package app.convergenciashow.events.data.model.response;

public class StreamingResponse extends BaseResponse{
    private String content;

    private String image;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
