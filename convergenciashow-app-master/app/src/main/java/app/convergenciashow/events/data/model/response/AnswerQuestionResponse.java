package app.convergenciashow.events.data.model.response;

import java.util.ArrayList;

public class AnswerQuestionResponse extends BaseResponse {

    private Integer answers;
    private ArrayList<OptionsResponse> options;

    public Integer getAnswers() {
        return answers;
    }

    public void setAnswers(Integer answers) {
        this.answers = answers;
    }

    public ArrayList<OptionsResponse> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionsResponse> options) {
        this.options = options;
    }
}
