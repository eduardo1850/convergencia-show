package app.convergenciashow.events.data.service;

import app.convergenciashow.events.data.model.response.ConvergenciaResponse;
import app.convergenciashow.events.data.model.response.StreamingResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StreamService {

    @GET("general/get-live-streaming-url")
    Call<StreamingResponse> getStreaming();

    @GET("general/get-general-info")
    Call<ConvergenciaResponse> getConvergenciaInfo(
            @Query("key") String description
    );

}
