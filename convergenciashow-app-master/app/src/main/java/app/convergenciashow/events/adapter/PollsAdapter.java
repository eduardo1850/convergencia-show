package app.convergenciashow.events.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import app.convergenciashow.events.R;
import app.convergenciashow.events.data.model.response.Poll;
import app.convergenciashow.events.fragment.PollsFragment;
import app.convergenciashow.events.util.WSCallbacks.PollsFragmentCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PollsAdapter extends RecyclerView.Adapter<PollsAdapter.ViewHolder> implements Filterable {

    private ArrayList<Poll> polls;
    private ArrayList<Poll> pollsFiltered;
    private PollsFragment fragment;
    private PollsFragmentCallback cb;
    private static final String JPG = ".jpg";
    private static final String VER = "?v=";
    private Unbinder unbinder;
    private Long idUser;
    private Activity activity;

    public PollsAdapter(ArrayList<Poll> polls, PollsFragment fragment, PollsFragmentCallback cb) {
        this.idUser = idUser;
        this.polls = polls;
        this.pollsFiltered = polls;
        this.fragment = fragment;
        this.cb = cb;
    }

    public void update(ArrayList<Poll> pollsList) {
        polls.addAll(pollsList);
        notifyDataSetChanged();
    }
    public void clear() {
        polls.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_poll_item, viewGroup, false);
        PollsAdapter.ViewHolder vh = new PollsAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PollsAdapter.ViewHolder viewHolder, int i) {
        final Poll poll = pollsFiltered.get(i);
        activity = fragment.getActivity();
        viewHolder.poll_name.setText(poll.getTitle());
        viewHolder.poll_author.setText(poll.getSubtitle());
        viewHolder.poll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cb.getActiveQuestionCB(poll.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (pollsFiltered != null)
            return pollsFiltered.size();
        else
            return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    pollsFiltered = polls;
                } else {
                    ArrayList<Poll> filteredList = new ArrayList<>();
                    for (Poll row : polls) {
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    pollsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = pollsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pollsFiltered = (ArrayList<Poll>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.poll_item)
        View poll_item;
        @BindView(R.id.pollitem_name)
        TextView poll_name;
        @BindView(R.id.pollitem_author)
        TextView poll_author;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


}
